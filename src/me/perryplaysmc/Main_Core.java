package me.perryplaysmc;

import me.perryplaysmc.chat.ChatHandler;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.core.configuration.ConfigManager;
import me.perryplaysmc.core.configuration.Configuration;
import me.perryplaysmc.enchantment.eventHandler.EnchantListener;
import me.perryplaysmc.event.user.UserInteractEvent;
import me.perryplaysmc.event.user.UserMoveEvent;
import me.perryplaysmc.event.user.block.Action;
import me.perryplaysmc.listener.ShopAddItemsEvents;
import me.perryplaysmc.permissions.group.GroupManager;
import me.perryplaysmc.prison.cells.PrisonCell;
import me.perryplaysmc.prison.cells.PrisonCellManager;
import me.perryplaysmc.prison.cells.events.SignClick;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.GuiHandler;
import me.perryplaysmc.utils.inventory.shops.GUIShopHandler;
import me.perryplaysmc.utils.inventory.shops.events.ShopInteract;
import me.perryplaysmc.world.Creator;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main_Core extends Core implements Listener {

    static Main_Core inst;
    public Scoreboard scoreboard;

    @Override
    public void onEnable() {
        init();
        initialize("Core");
        inst = this;
        scoreboard = Bukkit.getServer().getScoreboardManager().getMainScoreboard();
        if(scoreboard.getTeam("shulker")!=null)
            scoreboard.getTeam("shulker").unregister();
        //You need to create a different team for every color
        Team t = scoreboard.registerNewTeam("shulker");
        t.setPrefix(ChatColor.GREEN + ""); //Here you can set the color using ChatColor or using  minecraft color codes with §
        t.setColor(ChatColor.GREEN);
        t.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
        registerListeners(this, new EnchantListener(),
                new ShopAddItemsEvents(),
                new ShopInteract(),
                new GuiHandler(), new ChatHandler(), new SignClick());
        if(!ConfigManager.copyFolderFiles(Config.defaultDirectory.getPath() + "/userData", Config.defaultDirectory.getPath() + "/userDataBackup")){
            CoreAPI.error("Error has occurred");
        }
        GUIShopHandler.reload();
        ShopInteract.load();
        if(getWorlds().isSet("Worlds")) {
            for(String s : getWorlds().getSection("Worlds").getKeys(false)) {
                try {
                    if(getWorlds()==null) {
                        System.out.println("Worlds config is null");
                        break;
                    }
                    if(getWorlds().isSet("Worlds." + s + ".generator")) {
                        if(getWorlds().getString("Worlds." + s + ".generator").equalsIgnoreCase("bukkit")) {
                            setupWorld(new Creator(s));
                            continue;
                        }
                        String gen = getWorlds().getString("Worlds." + s + ".generator");
                        if(!gen.contains(".")) {
                            gen = "me.perryplaysmc.world.generators."+gen;
                        }
                        ChunkGenerator g = (ChunkGenerator) Class.forName(gen).newInstance();
                        setupWorld(new Creator(s, g));
                    }
                }catch (ClassNotFoundException | InstantiationException | IllegalAccessException | ClassCastException e) {
                    CoreAPI.error("Invalid Generator class " + getWorlds().getString("Worlds." + s+".generator"));
                }
            }
        }
        GroupManager.loadPermissions();
        PrisonCellManager.loadCells();
        if(getSettings().getBoolean("Features.AntiCheat.isEnabled")) {
            registerListeners("me.perryplaysmc.anticheat.listeners");
        }
    }

    @Override
    public void onDisable() {
        for(User u : getOnline()) {
            u.getPermissionData().removeAttachment();
        }
        for(PrisonCell cell : PrisonCellManager.getCells()) {
            cell.getRegion().clear();
        }
    }

    public static Main_Core getInstance() {
        return inst;
    }

    private void setupWorld(WorldCreator creator) {
        debug("Loading world " + creator.name());
        if(CoreAPI.getWorlds().isSet("Worlds."+creator.name()+".data")) {
            Configuration a = CoreAPI.getWorlds().getSection("Worlds." + creator.name() + ".data");
            if(Bukkit.getWorld(creator.name()) == null) {
                if(a.isSet("Environment"))
                    creator.environment(World.Environment.valueOf(a.getString("Environment").toUpperCase()));
                else
                    CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.Environment", creator.environment().name());
                if(a.isSet("WorldType"))
                    creator.type(WorldType.valueOf(a.getString("WorldType").toUpperCase()));
                else
                    CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.WorldType", creator.environment().name());
                if(a.isSet("GenerateStructures"))
                    creator.generateStructures(a.getBoolean("GenerateStructures"));
                else
                    CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.GenerateStructures", creator.generateStructures());
            }
            World w = Bukkit.getWorld(creator.name()) == null ? creator.createWorld() : Bukkit.getWorld(creator.name());
            CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.MaxHeight", w.getMaxHeight());

            if(a.isSet("Difficulty"))
                w.setDifficulty(Difficulty.valueOf(a.getString("Difficulty").toUpperCase()));
            else
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.Difficulty", w.getDifficulty().name());
            if(a.isSet("GameRules"))
                for (String b : a.getSection("GameRules").getKeys(false)) {
                    String c = a.getString("GameRules."+b);
                    w.setGameRuleValue(b, c);
                }
            else
                for(String gameRule : w.getGameRules()) {
                    CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.GameRules." + gameRule, w.getGameRuleValue(gameRule));
                }
            if(a.isSet("MonsterSpawnLimit"))
                w.setMonsterSpawnLimit(a.getInt("MonsterSpawnLimit"));
            else
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.MonsterSpawnLimit", w.getMonsterSpawnLimit());

            if(a.isSet("AmbientSpawnLimit"))
                w.setAmbientSpawnLimit(a.getInt("AmbientSpawnLimit"));
            else
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.AmbientSpawnLimit", w.getAmbientSpawnLimit());

            if(a.isSet("AnimalSpawnLimit"))
                w.setAmbientSpawnLimit(a.getInt("AnimalSpawnLimit"));
            else
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.AnimalSpawnLimit", w.getAnimalSpawnLimit());

            if(a.isSet("WaterAnimalSpawnLimit"))
                w.setWaterAnimalSpawnLimit(a.getInt("WaterAnimalSpawnLimit"));
            else
                CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.WaterAnimalSpawnLimit", w.getWaterAnimalSpawnLimit());
            Bukkit.getWorlds().add(w);
        }else {
            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.Environment", creator.environment().name());
            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.WorldType", creator.type().name());
            CoreAPI.getWorlds().set("Worlds." + creator.name() + ".data.GenerateStructures", creator.generateStructures());
            World w = Bukkit.createWorld(creator);
            setWorld(w);
            Bukkit.getWorlds().add(w);
        }
    }

    public void setWorld(World w) {
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.MaxHeight", w.getMaxHeight());
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.Seed", w.getSeed());
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.SeaLevel", w.getSeaLevel());
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.Difficulty", w.getDifficulty().name());
        for(String g : w.getGameRules()) {
            CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.GameRules." + g, w.getGameRuleValue(g));
        }
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.Spawn", w.getSpawnLocation());
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.WaterAnimalSpawnLimit", w.getWaterAnimalSpawnLimit());
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.MonsterSpawnLimit", w.getMonsterSpawnLimit());
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.AmbientSpawnLimit", w.getAmbientSpawnLimit());
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.AnimalSpawnLimit", w.getAnimalSpawnLimit());
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.AllowAnimals", w.getAllowAnimals());
        CoreAPI.getWorlds().set("Worlds." + w.getName() + ".data.AllowMonsters", w.getAllowMonsters());
    }

    List<String> called = new ArrayList<>();

    @EventHandler(priority = EventPriority.HIGHEST)
    void userAction(PlayerInteractEvent e) {
        Player player = e.getPlayer(), p = e.getPlayer();
        if(called.contains(p.getName()))return;
        else called.add(p.getName());
        new BukkitRunnable(){public void run(){called.remove(p.getName());}}.runTaskLater(this, 1);
        User u = CoreAPI.getUser(player);
        Action action = Action.NULL;
        Action actiontype = Action.NULL;
        if(e.getAction() ==  org.bukkit.event.block.Action.LEFT_CLICK_AIR) {
            action = Action.LEFT_CLICK_AIR;
            actiontype = Action.INTERACT;
        } else if(e.getAction() == org.bukkit.event.block.Action.RIGHT_CLICK_AIR) {
            action = Action.RIGHT_CLICK_AIR;
            actiontype = Action.INTERACT;
        } else if(e.getAction() ==  org.bukkit.event.block.Action.LEFT_CLICK_BLOCK) {
            action = Action.LEFT_CLICK_BLOCK;
            actiontype = Action.INTERACT;
        } else if(e.getAction() ==  org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK) {
            action = Action.RIGHT_CLICK_BLOCK;
            actiontype = Action.INTERACT;
        }
        UserInteractEvent event = new UserInteractEvent(u, action, actiontype, e.getClickedBlock());
        Bukkit.getPluginManager().callEvent(event);
        e.setCancelled(event.isCancelled());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void userMove(PlayerMoveEvent e) {
        Player player = e.getPlayer(), p = e.getPlayer();
        if(called.contains(p.getName()))return;
        else called.add(p.getName());
        new BukkitRunnable(){public void run(){called.remove(p.getName());}}.runTaskLater(this, 1);
        User u = CoreAPI.getUser(player);
        double toX = e.getTo().getX(), fromX = e.getFrom().getX();
        double toZ = e.getTo().getZ(), fromZ = e.getFrom().getZ();
        int toYaw = (int)e.getTo().getYaw(), fromYaw = (int)e.getFrom().getYaw();
        int toPitch = (int)e.getTo().getPitch(), fromPitch = (int)e.getFrom().getPitch();
        Block to = e.getTo().getBlock(), from = e.getFrom().getBlock(), toUnder = to.getRelative(0,-1,0);

        me.perryplaysmc.event.user.move.Action action = me.perryplaysmc.event.user.move.Action.NULL;
        me.perryplaysmc.event.user.move.Action actiontype = me.perryplaysmc.event.user.move.Action.NULL;

        if((toZ > fromZ || toZ < fromZ) && (toX > fromX || toX < fromX)) {
            action = me.perryplaysmc.event.user.move.Action.MOVE_X_Z;
            actiontype = me.perryplaysmc.event.user.move.Action.MOVE;
        } else if(toX > fromX || toX < fromX) {
            action = me.perryplaysmc.event.user.move.Action.MOVE_X;
            actiontype = me.perryplaysmc.event.user.move.Action.MOVE;
        } else if(toZ > fromZ || toZ < fromZ) {
            action = me.perryplaysmc.event.user.move.Action.MOVE_Z;
            actiontype = me.perryplaysmc.event.user.move.Action.MOVE;
        } else if(e.getTo().getY() < e.getFrom().getY()) {
            action = me.perryplaysmc.event.user.move.Action.MOVE_Y;
            actiontype = me.perryplaysmc.event.user.move.Action.FALL;
        } else if(to.getY() > from.getY() && (!toUnder.getType().isSolid() && toUnder.getType().name().contains("AIR"))) {
            action = me.perryplaysmc.event.user.move.Action.MOVE_Y;
            actiontype = me.perryplaysmc.event.user.move.Action.JUMP;
        } else if((toX > fromX || toX < fromX) && (to.getX() > from.getY() && (!toUnder.getType().isSolid() && toUnder.getType().name().contains("AIR")))) {
            action = me.perryplaysmc.event.user.move.Action.MOVE_X_Y;
            actiontype = me.perryplaysmc.event.user.move.Action.MOVE;
        } else if((toZ > fromZ || toZ < fromZ) && (to.getX() > from.getY() && (!toUnder.getType().isSolid() && toUnder.getType().name().contains("AIR")))) {
            action = me.perryplaysmc.event.user.move.Action.MOVE_Z_Y;
            actiontype = me.perryplaysmc.event.user.move.Action.MOVE;
        } else if((toYaw > fromYaw) && (toPitch > fromPitch)) {
            action = me.perryplaysmc.event.user.move.Action.PITCH_DOWN_YAW_RIGHT;
            actiontype = me.perryplaysmc.event.user.move.Action.PITCH_YAW;
        } else if((toYaw < fromYaw) && (toPitch < fromPitch)) {
            action = me.perryplaysmc.event.user.move.Action.PITCH_UP_YAW_LEFT;
            actiontype = me.perryplaysmc.event.user.move.Action.PITCH_YAW;
        } else if((toYaw > fromYaw) && (toPitch < fromPitch)) {
            action = me.perryplaysmc.event.user.move.Action.PITCH_UP_YAW_RIGHT;
            actiontype = me.perryplaysmc.event.user.move.Action.PITCH_YAW;
        } else if((toYaw < fromYaw) && (toPitch > fromPitch)) {
            action = me.perryplaysmc.event.user.move.Action.PITCH_DOWN_YAW_LEFT;
            actiontype = me.perryplaysmc.event.user.move.Action.PITCH_YAW;
        } else if((toPitch > fromPitch)) {
            action = me.perryplaysmc.event.user.move.Action.PITCH_DOWN;
            actiontype = me.perryplaysmc.event.user.move.Action.PITCH;
        } else if((toPitch < fromPitch)) {
            action = me.perryplaysmc.event.user.move.Action.PITCH_UP;
            actiontype = me.perryplaysmc.event.user.move.Action.PITCH;
        } else if((toYaw < fromYaw)) {
            action = me.perryplaysmc.event.user.move.Action.YAW_LEFT;
            actiontype = me.perryplaysmc.event.user.move.Action.YAW;
        } else if((toYaw > fromYaw)) {
            action = me.perryplaysmc.event.user.move.Action.YAW_RIGHT;
            actiontype = me.perryplaysmc.event.user.move.Action.YAW;
        }
        UserMoveEvent event = new UserMoveEvent(u, action, actiontype,
                to.getLocation(), from.getLocation());
        Bukkit.getPluginManager().callEvent(event);
        e.setCancelled(event.isCancelled());
    }

}
