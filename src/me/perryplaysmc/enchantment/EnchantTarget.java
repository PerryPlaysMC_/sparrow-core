package me.perryplaysmc.enchantment;

import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum EnchantTarget {


    //ARMOR

    ARMOR_HELMETS_ALL() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("ARMOR_HELMET") && t.includes(mat)) return true;
            }
            return false;
        }
    },
    ARMOR_CHESTPLATES_ALL() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("ARMOR_HELMET") && t.includes(mat)) return true;
            }
            return false;
        }
    },ARMOR_LEGGINGS_ALL() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("ARMOR_HELMET") && t.includes(mat)) return true;
            }
            return false;
        }
    },ARMOR_BOOTS_ALL() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("ARMOR_HELMET") && t.includes(mat)) return true;
            }
            return false;
        }
    },
    ARMOR_ALL() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("ARMOR") && t.includes(mat)) return true;
            }
            return false;
        }
    }



    //DIAMOND
    ,DIAMOND_ARMOR_HELMET() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.DIAMOND_HELMET;
        }
    },DIAMOND_ARMOR_CHESTPLATE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.DIAMOND_CHESTPLATE;
        }
    },DIAMOND_ARMOR_LEGGINGS() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.DIAMOND_LEGGINGS;
        }
    },DIAMOND_ARMOR_BOOTS() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.DIAMOND_BOOTS;
        }
    },DIAMOND_ARMOR_ALL() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("DIAMOND_ARMOR") && t.includes(mat)) return true;
            }
            return false;
        }
    },

    //IRON
    IRON_ARMOR_HELMET() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.IRON_HELMET;
        }
    },IRON_ARMOR_CHESTPLATE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.IRON_CHESTPLATE;
        }
    },IRON_ARMOR_LEGGINGS() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.IRON_LEGGINGS;
        }
    },IRON_ARMOR_BOOTS() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.IRON_BOOTS;
        }
    },IRON_ARMOR_ALL() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("IRON_ARMOR") && t.includes(mat)) return true;
            }
            return false;
        }
    },
    //GOLD
    GOLDEN_ARMOR_HELMET() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.GOLDEN_HELMET;
        }
    },GOLDEN_ARMOR_CHESTPLATE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.GOLDEN_CHESTPLATE;
        }
    },GOLDEN_ARMOR_LEGGINGS() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.GOLDEN_LEGGINGS;
        }
    },GOLDEN_ARMOR_BOOTS() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.GOLDEN_BOOTS;
        }
    },GOLDEN_ARMOR_ALL() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("GOLDEN_ARMOR") && t.includes(mat)) return true;
            }
            return false;
        }
    },

    //CHAINMAIL
    CHAINMAIL_ARMOR_HELMET() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.CHAINMAIL_HELMET;
        }
    },CHAINMAIL_ARMOR_CHESTPLATE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.CHAINMAIL_CHESTPLATE;
        }
    },CHAINMAIL_ARMOR_LEGGINGS() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.CHAINMAIL_LEGGINGS;
        }
    },CHAINMAIL_ARMOR_BOOTS() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.CHAINMAIL_BOOTS;
        }
    },CHAINMAIL_ARMOR_ALL() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("CHAINMAIL_ARMOR") && t.includes(mat)) return true;
            }
            return false;
        }
    },LEATHER_ARMOR_HELMET() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.LEATHER_HELMET;
        }
    },LEATHER_ARMOR_CHESTPLATE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.LEATHER_CHESTPLATE;
        }
    },LEATHER_ARMOR_LEGGINGS() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.LEATHER_LEGGINGS;
        }
    },LEATHER_ARMOR_BOOTS() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.LEATHER_BOOTS;
        }
    },LEATHER_ARMOR_ALL() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("LEATHER_ARMOR") && t.includes(mat)) return true;
            }
            return false;
        }
    }


    //TOOLS
    ,DIAMOND_SWORD() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.DIAMOND_SWORD;
        }
    },DIAMOND_AXE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.DIAMOND_AXE;
        }
    },DIAMOND_SHOVEL() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.DIAMOND_SHOVEL;
        }
    },DIAMOND_HOE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.DIAMOND_HOE;
        }
    },DIAMOND_PICKAXE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.DIAMOND_PICKAXE;
        }
    },IRON_SWORD() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.IRON_SWORD;
        }
    },IRON_AXE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.IRON_AXE;
        }
    },IRON_SHOVEL() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.IRON_SHOVEL;
        }
    },IRON_HOE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.IRON_HOE;
        }
    },IRON_PICKAXE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.IRON_PICKAXE;
        }
    },GOLDEN_SWORD() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.GOLDEN_SWORD;
        }
    },GOLDEN_AXE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.GOLDEN_AXE;
        }
    },GOLDEN_SHOVEL() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.GOLDEN_SHOVEL;
        }
    },GOLDEN_HOE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.GOLDEN_HOE;
        }
    },GOLDEN_PICKAXE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.GOLDEN_PICKAXE;
        }
    },STONE_SWORD() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.STONE_SWORD;
        }
    },STONE_AXE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.STONE_AXE;
        }
    },STONE_SHOVEL() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.STONE_SHOVEL;
        }
    },STONE_HOE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.STONE_HOE;
        }
    },STONE_PICKAXE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.STONE_PICKAXE;
        }
    },WOODEN_SWORD() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.WOODEN_SWORD;
        }
    },WOODEN_AXE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.WOODEN_AXE;
        }
    },WOODEN_SHOVEL() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.WOODEN_SHOVEL;
        }
    },WOODEN_HOE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.WOODEN_HOE;
        }
    },WOODEN_PICKAXE() {
        @Override
        public boolean includes(Material mat) {
            return mat == Material.WOODEN_PICKAXE;
        }
    },
    DIAMOND() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("DIAMOND")&&t.includes(mat)) return true;
            }
            return false;
        }
    },
    GOLD() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("GOLD")&&t.includes(mat)) return true;
            }
            return false;
        }
    },
    IRON() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("IRON")&&t.includes(mat)) return true;
            }
            return false;
        }
    },
    STONE() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("STONE")&&t.includes(mat)) return true;
            }
            return false;
        }
    },
    WOOD() {
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.name().contains("WOOD")&&t.includes(mat)) return true;
            }
            return false;
        }
    },

    //WEAPONS
    BOW {
        public boolean includes(Material item) {
            return item.equals(Material.BOW);
        }
    },
    FISHING_ROD {
        public boolean includes(Material item) {
            return item.equals(Material.FISHING_ROD);
        }
    },
    BREAKABLE {
        public boolean includes(Material item) {
            return item.getMaxDurability() > 0 && item.getMaxStackSize() == 1;
        }
    },
    WEARABLE {
        public boolean includes(Material item) {
            return ARMOR_ALL.includes(item)
                    ||
                    item.equals(Material.ELYTRA)
                    || item.equals(Material.PUMPKIN)
                    || item.equals(Material.CARVED_PUMPKIN)
                    || item.equals(Material.JACK_O_LANTERN)
                    || item.equals(Material.SKELETON_SKULL)
                    || item.equals(Material.WITHER_SKELETON_SKULL)
                    || item.equals(Material.ZOMBIE_HEAD)
                    || item.equals(Material.PLAYER_HEAD)
                    || item.equals(Material.CREEPER_HEAD)
                    || item.equals(Material.DRAGON_HEAD);
        }
    },
    TRIDENT {
        public boolean includes(Material item) {
            return item.equals(Material.TRIDENT);
        }
    },
    WEAPON {
        public boolean includes(Material item) {
            return item.equals(Material.WOODEN_SWORD) || item.equals(Material.STONE_SWORD) || item.equals(Material.IRON_SWORD) || item.equals(Material.DIAMOND_SWORD) || item.equals(Material.GOLDEN_SWORD);
        }
    },
    ELYTRA {
        public boolean includes(Material item) {
            return item == Material.ELYTRA;
        }
    },






    ALL(){
        @Override
        public boolean includes(Material mat) {
            for(EnchantTarget t : values()) {
                if(t.includes(mat) &&t!=ALL) return true;
            }
            return false;
        }
    };

    EnchantTarget() {

    }
    
    public String getName() {
        return StringUtils.getNameFromEnum(this).replace(" ", "_");
    }
    
    public boolean includes(ItemStack item) {
        return this.includes(item.getType());
    }

    public abstract boolean includes(Material mat);

}
