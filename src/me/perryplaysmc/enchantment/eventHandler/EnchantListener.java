package me.perryplaysmc.enchantment.eventHandler;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.enchantment.CustomEnchant;
import me.perryplaysmc.enchantment.EnchantHandler;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@SuppressWarnings("all")
public class EnchantListener implements Listener {
    
    
    HashMap<String, Integer> stuff = new HashMap<>();
    @EventHandler
    void onClick(InventoryClickEvent e) {
        if(Version.isVersionHigherThan(true, Versions.v1_14))
            if(e.getClickedInventory()!=null&&e.getClickedInventory().getType() == InventoryType.GRINDSTONE) {
                (new BukkitRunnable() {
                    @Override
                    public void run() {
                        ItemStack stack = e.getClickedInventory().getItem(2);
                        ItemBuilder item = ItemBuilder.createItem(stack);
                        if(stack!=null&&!item.getEnchants().isEmpty()) {
                            stack = item.removeAllEnchants().buildItem();
                            e.getClickedInventory().setItem(2, stack);
                            e.getInventory().setItem(2, stack);
                            ((Player) e.getView().getPlayer()).updateInventory();
                        }
                    }
                }).runTaskLater(Core.getAPI(), 10);
            }
    }
    
    List<String> pl = new ArrayList<>();
    
    // @EventHandler
    void onPrep(PrepareAnvilEvent e){
        if(pl.contains(e.getView().getPlayer().getName())) return;
        else pl.add(e.getView().getPlayer().getName());
        (new BukkitRunnable() {
            @Override
            public void run() { pl.remove(e.getView().getPlayer().getName());
            }
        }).runTaskLater(Core.getAPI(), 5);
        AnvilInventory inv = e.getInventory();
        ItemStack s1 = inv.getItem(0), s2 = inv.getItem(1);
        if(s1==null||s2==null||!s1.hasItemMeta()||!s2.hasItemMeta())return;
        ItemStack result = e.getResult().clone();
        ItemStack r = new ItemStack(result.getType(), result.getAmount());
        ItemMeta im = r.getItemMeta();
        ItemBuilder item = ItemBuilder.createItem(r);
        for(Enchantment c : result.getEnchantments().keySet())
            item.addEnchant(c, result.getEnchantmentLevel(c));
        inv.setItem(2, item.buildItem());
        //e.setResult(result);
    }
    
    

    
    @EventHandler
    void onAnvilPrepare(PrepareAnvilEvent e) {
        if(pl.contains(e.getView().getPlayer().getName())) return;
        else pl.add(e.getView().getPlayer().getName());
        (new BukkitRunnable() {
            @Override
            public void run() {
                pl.remove(e.getView().getPlayer().getName());
            }
        }).runTaskLater(Core.getAPI(), 5);
        AnvilInventory inv = e.getInventory();
        if(inv.getItem(0) == null || inv.getItem(1) == null) return;
        ItemStack is1 = inv.getItem(0), is2 = inv.getItem(1);
        if(!is1.hasItemMeta() || !is2.hasItemMeta()) return;
        ItemStack ir = e.getResult();
        ItemBuilder r = ItemBuilder.createItem(ir);
        ItemBuilder i1 = ItemBuilder.createItem(is1);
        ItemBuilder i2 = ItemBuilder.createItem(is2);
        if(r == null ||ir.getType().name().contains("AIR")) {
            if(is1.getType() == is2.getType()) ir.setType(is1.getType());
            ir.setItemMeta(is1.getItemMeta());
            if(!stuff.containsKey(((Player) e.getView().getPlayer()).getName())) {
                stuff.put(((Player) e.getView().getPlayer()).getName(), new Random().nextInt(15) + 1);
            }
            e.getInventory().setRepairCost(stuff.get(((Player) e.getView().getPlayer()).getName()));
            ((Player)e.getView().getPlayer()).updateInventory();
            r = ItemBuilder.createItem(ir);
        }
        List<Enchantment> enchantments = new ArrayList<>();
        for(Enchantment ench : EnchantHandler.getAllEnchantments()) {
            if(!enchantments.contains(ench) && i1.hasEnchant(ench) && !i2.hasEnchant(ench)) {
                A:for(Enchantment en : i2.getEnchants().keySet()) {
                    if(ench.conflictsWith(en)) {
                        r.removeEnchant(en);
                    }else {
                        r.addEnchant(en, i1.getEnchant(en));
                        enchantments.add(ench);
                    }
                }
            }
            if(!enchantments.contains(ench) && i2.hasEnchant(ench) && !i1.hasEnchant(ench)) {
                boolean willAdd = true;
                A:for(Enchantment en : i1.getEnchants().keySet()) {
                    if(ench.conflictsWith(en)) {
                        willAdd = false;
                        continue A;
                    }
                    willAdd = true;
                }
                if(willAdd) {
                    r.addEnchant(ench, i2.getEnchant(ench));
                    enchantments.add(ench);
                    willAdd = false;
                }
            }
            if(!enchantments.contains(ench) &&(((i1.hasEnchant(ench) && i2.hasEnchant(ench))))) {
                int lvl1 = i1.getEnchant(ench), lvl2 = i2.getEnchant(ench), lvl = 0;
                if(lvl1 == lvl2) {
                    if((lvl1+1) > ench.getMaxLevel()) continue;
                    lvl = lvl1+1;
                }
                else if(lvl2 > lvl1) {
                    lvl=lvl2;
                }
                else if(lvl1 > lvl2) {
                    lvl=lvl1;
                }
                r.addEnchant(ench, lvl);
            }
        }
        ItemStack finalR = r.buildItem();
        (new BukkitRunnable(){
            @Override
            public void run() {
                e.setResult(finalR);
                e.getInventory().setItem(2, finalR);
                ((Player) e.getView().getPlayer()).updateInventory();
            }
        }).runTaskLater(Core.getAPI(), 2);
    }
    
    int getLevel(CustomEnchant en) {
        int exp = 1;
        switch(en.getLevels()) {
            case LEVELS_25_30:
                exp = new Random().nextInt(6) +25;
                break;
            case LEVELS_20_25:
                exp = new Random().nextInt(6) +20;
                break;
            case LEVELS_15_20:
                exp = new Random().nextInt(6) +15;
                break;
            case LEVELS_10_15:
                exp = new Random().nextInt(6) +10;
                break;
            case LEVELS_6_10:
                exp = new Random().nextInt(5) +6;
                break;
            case LEVELS_0_6:
                exp = new Random().nextInt(7);
                break;
        }
        return exp;
    }
    
    @EventHandler
    public void onEnchantEvent(EnchantItemEvent e) {
        Player player = e.getEnchanter();
        ItemStack items = e.getItem();
        ItemMeta im = items.getItemMeta();
        A:for(Enchantment enc : e.getEnchantsToAdd().keySet()) {
            for(Enchantment et : im.getEnchants().keySet()) {
                if(enc.conflictsWith(et))
                    continue A;
            }
            if(!im.hasEnchant(enc)) {
                im.addEnchant(enc, e.getEnchantsToAdd().get(enc), true);
                items.setItemMeta(im);
            }
        }
        List<CustomEnchant> enchants2 = new ArrayList<>();
        List<CustomEnchant> hold = new ArrayList<>();
        hold.addAll(EnchantHandler.getEnchantments());
        List<CustomEnchant> random = new ArrayList<>();
        while(hold.size() > 0) {
            int r = new Random().nextInt(hold.size());
            random.add(hold.get(r));
            hold.remove(r);
        }
        ItemBuilder item = ItemBuilder.createItem(items);
        A:for(CustomEnchant ench : random) {
            boolean t = false;
            C:switch(ench.getLevels()) {
                case LEVELS_25_30:
                    if(e.getExpLevelCost() >= 25) t = true;
                    break C;
                case LEVELS_20_25:
                    if(e.getExpLevelCost() >= 20 && e.getExpLevelCost() < 25) t = true;
                    break C;
                case LEVELS_15_20:
                    if(e.getExpLevelCost() >= 15 && e.getExpLevelCost() < 20) t = true;
                    break C;
                case LEVELS_10_15:
                    if(e.getExpLevelCost() >= 10 && e.getExpLevelCost() < 15) t = true;
                    break C;
                case LEVELS_6_10:
                    if(e.getExpLevelCost() >= 6 && e.getExpLevelCost() < 10) t = true;
                    break C;
                case LEVELS_0_6:
                    if(e.getExpLevelCost() >= 0 && e.getExpLevelCost() < 6) t = true;
                    break C;
            }
            if(!t) continue A;
            for(Enchantment enc : im.getEnchants().keySet()) {
                if(ench.conflictsWith(enc)) continue A;
            }
            for(Enchantment enc : enchants2) {
                if(ench.conflictsWith(enc)) continue A;
            }
            for(Enchantment enc : e.getEnchantsToAdd().keySet()) {
                if(ench.conflictsWith(enc)) continue A;
            }
            if(ench.canEnchantItem(item.buildItem()) && !item.hasEnchant(ench) && !item.getItemMeta().hasEnchant(ench)) {
                for(Enchantment enc : im.getEnchants().keySet()) if(ench.conflictsWith(enc)) continue A;
                for(Enchantment enc : enchants2) if(ench.conflictsWith(enc)) continue A;
                for(Enchantment enc : e.getEnchantsToAdd().keySet()) if(ench.conflictsWith(enc)) continue A;
                item.addEnchant(ench, e.getExpLevelCost());
                if(item.getEnchant(ench) > -1) {
                    enchants2.add(ench);
                }
            }
        }
        item.reload();
        e.getInventory().setItem(0, item.buildItem());
    }
    
    
}
