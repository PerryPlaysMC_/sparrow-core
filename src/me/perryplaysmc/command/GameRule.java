package me.perryplaysmc.command;

import com.google.common.base.Preconditions;
import com.sun.istack.internal.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/25/19-2023
 * Package: me.perryplaysmc.command
 * Path: me.perryplaysmc.command.GameRule
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class GameRule<T> {
    private static Map<String, GameRule<?>> gameRules = new HashMap();
    public static final GameRule<Boolean> ANNOUNCE_ADVANCEMENTS = new GameRule("announceAdvancements", true, Boolean.class);
    public static final GameRule<Boolean> COMMAND_BLOCK_OUTPUT = new GameRule("commandBlockOutput", true, Boolean.class);
    public static final GameRule<Boolean> DISABLE_ELYTRA_MOVEMENT_CHECK = new GameRule("disableElytraMovementCheck", true, Boolean.class);
    public static final GameRule<Boolean> DO_DAYLIGHT_CYCLE = new GameRule("doDaylightCycle", false, Boolean.class);
    public static final GameRule<Boolean> DO_ENTITY_DROPS = new GameRule("doEntityDrops", true, Boolean.class);
    public static final GameRule<Boolean> DO_FIRE_TICK = new GameRule("doFireTick", true, Boolean.class);
    public static final GameRule<Boolean> DO_LIMITED_CRAFTING = new GameRule("doLimitedCrafting", false, Boolean.class);
    public static final GameRule<Boolean> DO_MOB_LOOT = new GameRule("doMobLoot", true, Boolean.class);
    public static final GameRule<Boolean> DO_MOB_SPAWNING = new GameRule("doMobSpawning", true, Boolean.class);
    public static final GameRule<Boolean> DO_TILE_DROPS = new GameRule("doTileDrops", true, Boolean.class);
    public static final GameRule<Boolean> DO_WEATHER_CYCLE = new GameRule("doWeatherCycle", true, Boolean.class);
    public static final GameRule<Boolean> KEEP_INVENTORY = new GameRule("keepInventory", false, Boolean.class);
    public static final GameRule<Boolean> LOG_ADMIN_COMMANDS = new GameRule("logAdminCommands", true, Boolean.class);
    public static final GameRule<Boolean> MOB_GRIEFING = new GameRule("mobGriefing", true, Boolean.class);
    public static final GameRule<Boolean> NATURAL_REGENERATION = new GameRule("naturalRegeneration", true, Boolean.class);
    public static final GameRule<Boolean> REDUCED_DEBUG_INFO = new GameRule("reducedDebugInfo", false, Boolean.class);
    public static final GameRule<Boolean> SEND_COMMAND_FEEDBACK = new GameRule("sendCommandFeedback", true, Boolean.class);
    public static final GameRule<Boolean> SHOW_DEATH_MESSAGES = new GameRule("showDeathMessages", true, Boolean.class);
    public static final GameRule<Boolean> SPECTATORS_GENERATE_CHUNKS = new GameRule("spectatorsGenerateChunks", false, Boolean.class);
    public static final GameRule<Boolean> DISABLE_RAIDS = new GameRule("disableRaids", false, Boolean.class);
    public static final GameRule<Integer> RANDOM_TICK_SPEED = new GameRule("randomTickSpeed", 3, Integer.class);
    public static final GameRule<Integer> SPAWN_RADIUS = new GameRule("spawnRadius", 10, Integer.class);
    public static final GameRule<Integer> MAX_ENTITY_CRAMMING = new GameRule("maxEntityCramming", 24, Integer.class);
    public static final GameRule<Integer> MAX_COMMAND_CHAIN_LENGTH = new GameRule("maxCommandChainLength", 65536, Integer.class);
    private final String name;
    private final Class<T> type;
    private final Object defaultVal;
    
    private GameRule(String name, Object defaultVal,  Class<T> clazz) {
        Preconditions.checkNotNull(name, "GameRule name cannot be null");
        Preconditions.checkNotNull(clazz, "GameRule type cannot be null");
        Preconditions.checkArgument(clazz == Boolean.class || clazz == Integer.class, "Must be of type Boolean or Integer. Found %s ", clazz.getName());
        this.name = name;
        this.type = clazz;
        this.defaultVal = defaultVal;
        gameRules.put(name, this);
    }
    
    
    public String getName() {
        return this.name;
    }
    
    public Object getDefaultValue() {
        return this.defaultVal;
    }
    
    
    public Class<T> getType() {
        return this.type;
    }
    
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (!(obj instanceof GameRule)) {
            return false;
        } else {
            GameRule<?> other = (GameRule)obj;
            return this.getName().equals(other.getName()) && this.getType() == other.getType();
        }
    }
    
    public String toString() {
        return "GameRule{key=" + this.name + ", type=" + this.type + '}';
    }
    
    public static GameRule<?> getByName(String rule) {
        Preconditions.checkNotNull(rule, "Rule cannot be null");
        return (GameRule)gameRules.get(rule);
    }
    
    
    public static GameRule<?>[] values() {
        return (GameRule[])gameRules.values().toArray(new GameRule[gameRules.size()]);
    }
}
