package me.perryplaysmc.command;

import com.google.common.collect.ImmutableList;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.exceptions.CommandException;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class CommandHandler extends org.bukkit.command.Command {

    private static List<Command> commands;
    private static CommandHandler h;
    public static boolean isCommandDoubleSlash = CoreAPI.getSettings().getBoolean("settings.isCommandDoubleSlash");

    public CommandHandler(String name) {
        super(name);
        h=this;
    }

    public static List<Command> getCommands() {
        if(commands == null) {
            commands = new ArrayList<>();
        }
        return commands;
    }

    public static void addCommand(Command cmd) {
        if(!getCommands().contains(cmd)) {
            getCommands().add(cmd);
            cmd.handler = h;
        }
    }

    public static Command getCommand(String label) {
        for(Command cmd : getCommands()) {
            if(label.equalsIgnoreCase(cmd.getName()) ||
                    label.equalsIgnoreCase(CoreAPI.getSettings().getString("settings.main-name").toLowerCase() + ":" + cmd.getName())) {
                return cmd;
            }
            if(cmd.getAliases() != null && cmd.getAliases().length > 0) {
                int index = 0;
                A:for (String s : cmd.getAliases()) {
                    index++;
                    if(StringUtils.isEmpty(s) && index < (cmd.getAliases().length + -1)) {
                        CoreAPI.error( "Alias must contain at least 1 character");
                        continue A;
                    }
                    if(label.equalsIgnoreCase(s) ||
                            label.equalsIgnoreCase(CoreAPI.getSettings().getString("settings.main-name").toLowerCase() + ":" + s)) {
                        return cmd;
                    }
                }
            }
        }
        return null;
    }


    @Override
    public boolean execute(CommandSender sender, String cl, String[] ar) {
        Command cmd = getCommand(cl);
        if(cmd == null && isCommandDoubleSlash) {
            cmd = getCommand(cl.substring(1));
            if(cmd == null && isCommandDoubleSlash) {
                cmd = getCommand(cl.substring(2));
            }
        }

        if(cmd != null) {
            CommandSource s = CoreAPI.getCommandSource(sender.getName());
            cmd.s = s;
            cmd.handler = this;
            try {
                Argument[] args = new Argument[ar.length];
                for(int i = 0; i < ar.length; i++) {
                    String a = ar[i];
                    args[i] = new Argument(a);
                }
                cmd.args = args;
                try {
                    cmd.run(s, new CommandLabel(cl), args);
                }catch (Exception e) {
                    if(e instanceof CommandException) {
                        if(!StringUtils.isEmpty(e.getMessage())&&s!=null)
                            s.sendMessage(e.getMessage());
                        return true;
                    }
                    e.printStackTrace();
                    if(args.length == 0) {
                        cmd.argsLengthError(false);
                    }else {
                        cmd.argsLengthError(true);
                    }
                }
            } catch (CommandException e) {
                if(!StringUtils.isEmpty(e.getMessage())&&s!=null)
                    s.sendMessage(e.getMessage());
            }
        }
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String cl, String[] ar) throws IllegalArgumentException {
        Command cmd = getCommand(cl);
        if(cmd == null && isCommandDoubleSlash) {
            cmd = getCommand(cl.substring(1));
            if(cmd == null && isCommandDoubleSlash) {
                cmd = getCommand(cl.substring(2));
            }
        }

        if(cmd != null) {
            CommandSource s = CoreAPI.getCommandSource(sender.getName());
            cmd.s = s;
            cmd.handler = this;
            Argument[] args = new Argument[ar.length];
            for(int i = 0; i < ar.length; i++) {
                String a = ar[i];
                args[i] = new Argument(a);
            }
            cmd.args = args;
            cmd.tabArgs = args;
            try {
                try {
                    return cmd.tabComplete(s, new CommandLabel(cl), args);
                } catch (CommandException e) {
                    if(s != null && s.isPlayer())
                        s.getUser().sendAction(e.getMessage());
                }
            }catch(Exception e) {
            }
        }
        return ImmutableList.of();
    }
}
