package me.perryplaysmc.command;

public class CommandLabel {

    private String name;


    public CommandLabel(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public boolean equalsIgnoreCase(String... others) {
        for(String s : others) {
            if(getName().equalsIgnoreCase(s))return true;
        }
        return false;
    }
    
    public boolean contains(String... contains) {
        for(String s : contains) {
            if(getName().toLowerCase().contains(s.toLowerCase()))return true;
        }
        return false;
    }
    

    public boolean equals(Object... objects) {
        for(Object obj : objects) {
            if(name == obj.toString()) return true;
        }
        return false;
    }

}
