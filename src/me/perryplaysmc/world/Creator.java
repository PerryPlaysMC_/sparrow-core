package me.perryplaysmc.world;

import org.bukkit.WorldCreator;
import org.bukkit.generator.ChunkGenerator;

public class Creator extends WorldCreator {
    public Creator(String name, ChunkGenerator gen) {
        super(name);
        generator(gen);
    }
    public Creator(String name) {
        super(name);
    }
}
