package me.perryplaysmc.world;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.logging.LogType;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;
import org.bukkit.generator.ChunkGenerator;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public class WorldManager {

    public static boolean duplicateWorld(String worldName, String dest) {
        if(new File("." + File.separator + dest).exists()) { //Check if the world already exists
            CoreAPI.getConsole().sendMessage(Level.INFO.getLocalizedName() + " " + dest + " already exists!");
            return false;
        }
        try {
            FileUtils.copyDirectory(new File("." + File.separator + worldName), new File("." + File.separator + dest)); //Copy the files
            FileUtils.forceDelete(new File("."+File.separator+dest+File.separator+"uid.dat")); //delete the uid.dat. Doesn't work if you don't do this.
            CoreAPI.getConsole().sendMessage(Level.INFO.getLocalizedName() + " World " + worldName + " has been duplicated!");
            return true;
        } catch(IOException e) {
            CoreAPI.getConsole().sendMessage(Level.SEVERE.getLocalizedName() + " Error duplicating world!");
            return false;
        }
    }

    public static World loadWorld(String worldName) {
        World world = new WorldCreator(worldName).createWorld();
        return world;
    }
    public static World loadWorld(String worldName, ChunkGenerator gen) {
        World world = new Creator(worldName, gen).createWorld();
        return world;
    }

    public static boolean removeWorld(World world) {
        if(Bukkit.getServer().getWorlds().contains(world))
            Bukkit.getServer().unloadWorld(world,false); //Unload without saving.
        try {
            File f = new File(Bukkit.getWorldContainer(),world.getName());
            if(f.isDirectory() && f.listFiles() != null)
                for(File file : f.listFiles()) {
                    if(!delete(file))
                        FileUtils.forceDelete(file);
                }
            FileUtils.forceDelete(f);
            FileUtils.deleteDirectory(f);
            f.delete();
            FileUtils.deleteQuietly(f);
            return true;
        } catch(IOException e) {
            CoreAPI.getConsole().sendMessage(Level.SEVERE.getLocalizedName() + " Unable to delete world!");
            return false;
        }
    }

    static boolean delete(File f) {
        if(f.isDirectory() && f.listFiles() != null) {
            for(File file : f.listFiles()) {
                if(!delete(file)) {
                    try {
                        FileUtils.forceDelete(file);
                    } catch (IOException e) {
                        CoreAPI.getConsole().sendMessage(Level.SEVERE.getLocalizedName() + " Unable to delete file " + file.getName() + "!");
                    }
                }
            }
            return true;
        }
        return false;
    }

    public static boolean removeWorld(String world) {
        if(Bukkit.getWorld(world)!=null)
            Bukkit.getServer().unloadWorld(Bukkit.getWorld(world),false); //Unload without saving.
        try {
            FileUtils.deleteDirectory(new File(Bukkit.getWorldContainer(), world));
            CoreAPI.info( "Deleted world " + world);
            return true;
        } catch(IOException e) {
            CoreAPI.getConsole().sendMessage(Level.SEVERE.getLocalizedName() + " Unable to delete world!");
            return false;
        }
    }

}