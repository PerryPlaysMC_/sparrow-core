package me.perryplaysmc.world.generators;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeneratorFlatLand extends ChunkGenerator {
    
    
    @Override
    public boolean canSpawn(World world, int x, int z) {
        return true;
    }
    
    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        List<BlockPopulator> populators = new ArrayList<>();
        return populators;
    }
    
    @Override
    public ChunkData generateChunkData(World world, Random random, int cX, int cZ, BiomeGrid biome) {
        ChunkData chunk = createChunkData(world);
        for(int x = 0; x < 16; x++) {
            for(int z = 0; z < 16; z++) {
                Random r = new Random();
                int n = r.nextInt(100);
                chunk.setBlock(x, 4, z, n < 35 ? Material.GRASS_BLOCK : n < 50 ? Material.GREEN_TERRACOTTA : Material.GREEN_CONCRETE);
                chunk.setBlock(x, 3, z, Material.DIRT);
                chunk.setBlock(x, 2, z, Material.DIRT);
                chunk.setBlock(x, 1, z, Material.DIRT);
                chunk.setBlock(x, 0, z, Material.BEDROCK);
            }
        }
        return chunk;
    }
}
