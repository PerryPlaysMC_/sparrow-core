package me.perryplaysmc.world.generators.biome.noiseGenerators;

import org.bukkit.World;

public class RiverNoiseGenerator extends BiomeNoiseGenerator {

	@Override
	public void setWorld(World world) {
		super.setWorld(world);
		this.generator.setScale(1.0/-3);
		this.magnitude = 1.0;
	}
	
	@Override
	public double get3dNoise(double x, double y, double z) {
		return 0;
	}

}