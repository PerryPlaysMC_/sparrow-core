package me.perryplaysmc.world.generators;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.util.noise.PerlinOctaveGenerator;
import org.bukkit.util.noise.SimplexOctaveGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeneratorDefault extends ChunkGenerator {
    private static final int SEA_LEVEL = 63;
    private static final int MAGNITUDE = 64;
    private static final int MIDDLE = 70; // the "middle" of the road
    private static final double SCALE = 32.0; //how far apart the tops of the hills are
    private static final double THRESHOLD = 10.0; // the cutoff point for terrain
    private static int CHUNKS = 0;
    private static int CHANGES = 0;
    private static int LAST_MAXHEIGHT = 0;
    
    @Override
    public ChunkData generateChunkData(World world, Random random, int cX, int cZ, BiomeGrid biomegrid) {
        ChunkData chunk = createChunkData(world);
        PerlinOctaveGenerator elevation = new PerlinOctaveGenerator(world.getSeed(), 4);
        elevation.setScale(1/200.0);
        //1 Divided by the Amount of blocks in between peaks
        PerlinOctaveGenerator detail = new PerlinOctaveGenerator(world.getSeed(), 4);
        detail.setScale(1/30.0);
        
        PerlinOctaveGenerator rough = new PerlinOctaveGenerator(world.getSeed(), 1);
        rough.setScale(1/150.0);
        
        SimplexOctaveGenerator overhang = new SimplexOctaveGenerator(world.getSeed(), 3);
        overhang.setScale(1/90.0);
        
        
        for(int x = 0; x < 16; x++) {
            for(int z = 0; z < 16; z++) {
                int rx = cX*16+x, rz = cZ*16+z;
                double elevationNoise = elevation.noise(rx, rz, 0.5, 0.5, true);
                double detailNoise = detail.noise(rx, rz, 0.5, 0.5, true);
                double roughNoise = rough.noise(rx, rz, 0.5, 0.5, true);
                int maxHeight = (int) ((elevationNoise + detailNoise * roughNoise) * MAGNITUDE + SEA_LEVEL);
                int overHang = (int) ((overhang.noise(rx, rz, 0.9, 0.8)+1)
                                      *8)+SEA_LEVEL+-5;
                if(maxHeight > world.getMaxHeight())
                    maxHeight = world.getMaxHeight();
                if(overHang > world.getMaxHeight())
                    overHang = world.getMaxHeight();
                Location realLoc = new Location(world, x, maxHeight, z);
                Biome b = biomegrid.getBiome(x,z);
                if(maxHeight <= SEA_LEVEL && b.name().contains("OCEAN")) {
                    biomegrid.setBiome(x,z,Biome.LUKEWARM_OCEAN);
                    for(int y = maxHeight; y < SEA_LEVEL; y++)
                        setBlock(chunk, x, y, z, Material.WATER);
                    setBlock(chunk, x, maxHeight - 4, z, Material.STONE);
                    setBlock(chunk, x, maxHeight - 0, z, Material.SAND);
                    setBlock(chunk, x, maxHeight - 1, z, Material.SAND);
                    setBlock(chunk, x, maxHeight - 2, z, Material.SAND);
                    setBlock(chunk, x, maxHeight - 3, z, Material.SAND);
                    
                }else {
                    if(b.name().contains("MOUNTAIN")) {
                        int maxHeight2 = (int) ((elevationNoise + detailNoise) * MAGNITUDE + SEA_LEVEL);
                        setBlock(chunk, x, maxHeight2, z, Material.STONE);
                    }else if(!b.name().contains("DESERT")) {
                        setBlock(chunk, x, maxHeight, z, Material.GRASS_BLOCK);
                        setBlock(chunk, x, overHang, z, Material.GRASS_BLOCK);
//                        setBlock(chunk, x, overHang - 1, z, Material.DIRT);
//                        setBlock(chunk, x, overHang - 2, z, Material.DIRT);
//                        setBlock(chunk, x, overHang - 3, z, Material.STONE);
//                        setBlock(chunk, x, overHang - 4, z, Material.STONE);
//                        setBlock(chunk, x, overHang - 5, z, Material.STONE);
                    }else {
                        setBlock(chunk, x, maxHeight-5, z, Material.STONE);
                        setBlock(chunk, x, maxHeight-4, z, Material.SANDSTONE);
                        setBlock(chunk, x, maxHeight-3, z, Material.SANDSTONE);
                        setBlock(chunk, x, maxHeight-2, z, Material.SAND);
                        setBlock(chunk, x, maxHeight-1, z, Material.SAND);
                        setBlock(chunk, x, maxHeight-0, z, Material.SAND);
                    }
                    
                }
                
            }
        }
        return chunk;
    }
    
    void setBlock(ChunkData c, int x, int y, int z, Material mat) {
        c.setBlock(x, y, z, mat);
    }
    
    @Override
    public boolean canSpawn(World world, int x, int z) {
        return true;
    }
    
    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        List<BlockPopulator> populators = new ArrayList<>();
        return populators;
    }
    
    /*

    
    @Override
    //Generates block sections. Each block section is 16*16*16 blocks, stacked above each other. //There are world height / 16 sections. section number is world height / 16 (>>4)
    //returns a byte[world height / 16][], formatted [section id][Blocks]. If there are no blocks in a section, it need not be allocated.
    public byte[][] generateBlockSections(World world, Random rand,
                                          int ChunkX, int ChunkZ, BiomeGrid biomes) {
        Random random = new Random(world.getSeed());
        SimplexOctaveGenerator gen = new SimplexOctaveGenerator(random, 8);
        PerlinOctaveGenerator gen2 = new PerlinOctaveGenerator(world.getSeed(), 8);
        
        byte[][] chunk = new byte[world.getMaxHeight() / 16][];
        
        gen2.setScale(1/scale);
        gen.setScale(1/scale); //The distance between peaks of the terrain. Scroll down more to see what happens when you play with this
        double threshold = this.threshold; //scroll down to see what happens when you play with this.
        
        for (int x=0;x<16;x++) {
            for (int z=0;z<16;z++) {
                int real_x = x+ChunkX * 16;
                int real_z = z+ChunkZ*16;
                
                double height = middle+gen2.noise(real_x, real_z, 0.5, 0.5)*middle/3; // generate some smoother terrain
                
                for (int y=1; y<height && y<256; y++) {
                    if(y > middle-middle/3) {
                        double noise = gen.noise(real_x, y, real_z, 0.5, 0.5);
                        if(noise > threshold) //explained above
                            setBlock(x,y,z,chunk,Material.STONE); //set the block solid
                    } else {
                        setBlock(x,y,z,chunk,Material.STONE);
                    }
                }
            }
        }
        return chunk;
    }
}
     */
}
