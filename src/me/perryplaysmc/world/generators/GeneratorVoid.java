package me.perryplaysmc.world.generators;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeneratorVoid extends ChunkGenerator {
    
    public GeneratorVoid() {
    
    }
    
    @Override
    public boolean canSpawn(World world, int x, int z) {
        return true;
    }
    
    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        List<BlockPopulator> populators = new ArrayList<>();
        return populators;
    }
    
    @Override
    public ChunkData generateChunkData(World world, Random random, int cX, int cZ, BiomeGrid biome) {
        ChunkData chunk = createChunkData(world);
        for(int x = 0; x < 16; x++) {
            for(int z = 0; z < 16; z++) {
                for(int y = 0; y < world.getMaxHeight(); y++)
                    chunk.setBlock(x, y, z, Material.AIR);
            }
        }
        return chunk;
    }
    
    
}
