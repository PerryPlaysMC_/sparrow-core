package me.perryplaysmc.chat.json.command;

import me.perryplaysmc.utils.string.ColorUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.HashMap;

public class JsonHandler implements Listener {

    private static HashMap<String, Run> commands = new HashMap<>();

    public static Run getCommand(String x) {
        return commands.get(x);
    }

    public static void addCommand(String x, Run r) {
        commands.put(x, r);
    }

    public static HashMap<String, Run> getCommands() {
        return commands;
    }

    @EventHandler
    void onCommand(AsyncPlayerChatEvent e) {
        String msg = ColorUtil.removeColor(e.getMessage());
        if(getCommand(msg.split(" ")[0]) != null) {
            e.setCancelled(true);
            getCommand(msg.split(" ")[0]).run(e.getPlayer(),
                    e.getMessage().substring(msg.split(" ")[0].length()).split(" "));
            e.setMessage("");
        }
    }
}
