package me.perryplaysmc.chat.json;

import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Represents an object that can be serialized to a JSON writer instance.
 */
public interface JsonRepresentedObject {

	void writeJson(JsonWriter writer) throws IOException;
	
}
