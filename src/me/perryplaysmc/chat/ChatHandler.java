package me.perryplaysmc.chat;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.event.user.UserChatEvent;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.utils.string.PlaceHolderUtil;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("all")
public class ChatHandler implements org.bukkit.event.Listener {
    
    
    private HashMap<CommandSource, MessageData> users = new HashMap<>();
    String original = "§r";
    ArrayList<User> sent = new ArrayList<>();
    
    @EventHandler
    void onCommand(PlayerCommandPreprocessEvent e) {
        User u = CoreAPI.getUser(e.getPlayer());
        if(u==null)return;
        if(u.isCaptured())e.setCancelled(true);
        for (User us : CoreAPI.getOnlineUsers()) {
            if(!us.getUniqueId().equals(u.getUniqueId()) && us.isCommandSpyEnabled() && !sent.contains(us)) {
                String f = CoreAPI.format(CoreAPI.getChat(),"Events.CommandSpy.message", u.getRealName(), e.getMessage());
                Message msg = new Message(f);
                msg.suggest(ColorUtil.removeColor(CoreAPI.format(CoreAPI.getChat(),"Events.CommandSpy.suggest", u.getRealName(), e.getMessage(),
                        u.getLocation().getBlockX()
                        , u.getLocation().getBlockY()
                        , u.getLocation().getBlockZ())))
                        .tooltip(CoreAPI.format(CoreAPI.getChat(),"Events.CommandSpy.hover", u.getRealName(), e.getMessage()
                                , u.getLocation().getBlockX()
                                , u.getLocation().getBlockY()
                                , u.getLocation().getBlockZ()));
                msg.send(us);
                sent.add(us);
            }
        }
    }
    
    private class MessageData {
        boolean isInvalid;
        Message msg;
        MessageData(boolean isInvalid, Message msg) {
            this.isInvalid = isInvalid;
            this.msg = msg;
        }
    }
    
    String getMessagePart1(User u, String group) {
        String msg = CoreAPI.getChat().getString("Chat.JSON.formats."+group+".part1.message");
        return PlaceHolderUtil.translate(u, msg.contains("%message%") ? msg.split("%message%")[0] : msg.split("\\{message}")[0]);
    }
    
    String getMessagePart2(User u, String group) {
        String msg = CoreAPI.getChat().getString("Chat.JSON.formats."+group+".part2.message");
        return PlaceHolderUtil.translate(u, msg.contains("%message%") ? msg.split("%message%")[0] : msg.split("\\{message}")[0]);
    }
    
    String getMessagePart3(User u, String group) {
        String msg = CoreAPI.getChat().getString("Chat.JSON.formats."+group+".part3.message");
        return PlaceHolderUtil.translate(u, msg.contains("%message%") ? msg.split("%message%")[0] : msg.split("\\{message}")[0]);
    }
    
    String getHoverPart1(User u, String group) {
        return PlaceHolderUtil.translate(u, CoreAPI.getChat().getString("Chat.JSON.formats."+group+".part1.hover", ""));
    }
    
    String getHoverPart2(User u, String group) {
        return PlaceHolderUtil.translate(u, CoreAPI.getChat().getString("Chat.JSON.formats."+group+".part2.hover", ""));
    }
    
    String getHoverPart3(User u, String group) {
        return PlaceHolderUtil.translate(u, CoreAPI.getChat().getString("Chat.JSON.formats."+group+".part3.hover", ""));
    }
    
    String getSuggestPart1(User u, String group) {
        return PlaceHolderUtil.translate(u, CoreAPI.getChat().getString("Chat.JSON.formats."+group+".part1.click", ""));
    }
    
    String getSuggestPart2(User u, String group) {
        return PlaceHolderUtil.translate(u, CoreAPI.getChat().getString("Chat.JSON.formats."+group+".part2.click", ""));
    }
    
    String getSuggestPart3(User u, String group) {
        return PlaceHolderUtil.translate(u, CoreAPI.getChat().getString("Chat.JSON.formats."+group+".part3.click", ""));
    }
    
    
    Message generateMessage(User u, String group) {
        String
                part1 = StringUtils.translate(getMessagePart1(u, group)),
                part2 = StringUtils.translate(getMessagePart2(u, group)),
                part3 = StringUtils.translate(getMessagePart3(u, group)),
                hover1 = StringUtils.translate(getHoverPart1(u, group)),
                hover2 = StringUtils.translate(getHoverPart2(u, group)),
                hover3 = StringUtils.translate(getHoverPart3(u, group)),
                suggest1 = StringUtils.translate(getSuggestPart1(u, group)),
                suggest2 = StringUtils.translate(getSuggestPart2(u, group)),
                suggest3 = StringUtils.translate(getSuggestPart3(u, group));
        
        Message msg = new Message();
        if(!StringUtils.isEmpty(part1)) {
            msg.then(ColorUtil.removeColor(part1).endsWith(" ") ? part1.substring(0, part1.lastIndexOf(" ")) : part1);
            if(!StringUtils.isEmpty(suggest1))
                msg.suggest(suggest1);
            if(!StringUtils.isEmpty(hover1))
                msg.hover(hover3);
        }
        if(!StringUtils.isEmpty(part2)) {
            if(doSpace(part1, ""))
                msg.then(" ");
            msg.then(ColorUtil.removeColor(part2).endsWith(" ") ? part2.substring(0, part2.lastIndexOf(" ")) : part2);
            if(!StringUtils.isEmpty(suggest2))
                msg.suggest(suggest2);
            if(!StringUtils.isEmpty(hover2))
                msg.hover(hover2);
        }
        if(!StringUtils.isEmpty(part3)) {
            if(doSpace(part1, part2))
                msg.then(" ");
            msg.then(ColorUtil.removeColor(part3).endsWith(" ") ? part3.substring(0, part3.lastIndexOf(" ")) : part3);
            if(!StringUtils.isEmpty(suggest3))
                msg.suggest(suggest3);
            if(!StringUtils.isEmpty(hover3))
                msg.hover(hover3);
        }
        if(!StringUtils.isEmpty(ColorUtil.removeColor(part3))) {
            if(ColorUtil.removeColor(part3).endsWith(" "))
                msg.then(" ");
            original = part3;
        } else if(!StringUtils.isEmpty(ColorUtil.removeColor(part2))) {
            if(ColorUtil.removeColor(part2).endsWith(" "))
                msg.then(" ");
            original = part2;
        } else if(!StringUtils.isEmpty(ColorUtil.removeColor(part1))) {
            if(ColorUtil.removeColor(part1).endsWith(" "))
                msg.then(" ");
            original = part1;
        }
        original = ColorUtil.getLastUsedColors(original).replace("&", "§").replace(" ", "");
        msg.original = original.replace("&", "§");
        return msg;
    }
    
    
    @EventHandler(priority = EventPriority.HIGHEST)
    void onChat(AsyncPlayerChatEvent e) {
        User u = CoreAPI.getUser(e.getPlayer());
        if(u==null)return;
        boolean isMuted = CoreAPI.getChat().getBoolean("settings.muted");
        boolean isSlow = CoreAPI.getChat().getBoolean("settings.Slow.enabled");
        if(!u.getChatSettings().isEnabled()) {
            u.sendMessageFormat(CoreAPI.getChat(), "settings.disabled-Chat-Message");
            e.setCancelled(true);
            return;
        }
        if(isMuted) {
            if(e.isCancelled()) return;
            u.sendMessageFormat(CoreAPI.getChat(), "settings.mute-Message");
            e.setCancelled(true);
            return;
        }
        if(isSlow) {
            if(e.isCancelled()) return;
            if(u.isCooldown("slowChat")) {
                u.sendMessageFormat(CoreAPI.getChat(), "settings.Slow.slow-Message", new DecimalFormat("#.###").format(u.getCooldownRemains("slowChat")));
                e.setCancelled(true);
                return;
            }
            u.startCooldown("slowChat", CoreAPI.getChat().getDouble("settings.Slow.time"));
        }
        if(!CoreAPI.getChat().getBoolean("Chat.isClickable")) {
            original = ColorUtil.getLastUsedColors(CoreAPI.getChat().getString("Chat.format").split("%message%")[0]);
            String f = StringUtils.translateForUser(u, original,
                    CoreAPI.getChat().getString("Chat.format").replace("%player%", u.getRealName()).replace("%message%", e.getMessage()));
            e.setFormat(f);
            return;
        }
        if(e.isCancelled()) return;
        Message msg = generateMessage(u, u.getChatSettings().getChatFormat());
        e.setMessage(original.replace("&", "§") + ColorUtil.changeChatColorTo('&', e.getMessage()));
        e.setCancelled(true);
        String messages = e.getMessage();
        boolean invalidItem = false;
        MessageData data = generateMessage(u, null, generateMessage(u, u.getChatSettings().getChatFormat()), e.getMessage(), invalidItem);
        
        Message finalMsg = msg;
        Bukkit.getScheduler().runTaskLater(Core.getAPI(), () -> {
            List<User> users = new ArrayList<>();
            users.addAll(CoreAPI.getOnlineUsers());
            
            UserChatEvent e2 = new UserChatEvent(u, e.getMessage().substring(2), data.isInvalid, users);
            Bukkit.getPluginManager().callEvent(e2);
            if(e2.isCancelled() || data.isInvalid) return;
            for (User us : e2.getRecipients()) {
                if(us.getChatSettings().isEnabled()) {
                    if(!us.getChatSettings().allowLinks()) {
                        data.msg.clone().removeLinkData().send(us);
                        continue;
                    }
                    data.msg.send(us);
                }
            }
            data.msg.send(CoreAPI.getConsole());
        }, 1);
    }
    MessageData generateMessage(User u, CommandSource toSend, Message ms, String message, boolean invalidItem) {
        Message msg = toSend == null ? ms : ms.clone();
        String[] messageSplit = message.split(" ");
        A:for(int i = 0; i < messageSplit.length; i++) {
            String m = ColorUtil.changeChatColorTo('&', messageSplit[i]).replace(original.replace("§", "&"), original.replace("&", "§"));
            String og = ColorUtil.removeColor(m);
            String x1 = og.startsWith("https://") | og.startsWith("http://") ? og : "https://"+og;
            if(x1.startsWith("https://") || x1.startsWith("http://")) {
                og = x1;
            }else if(!x1.startsWith("https://") && !x1.startsWith("http://")) {
                og = "https://" + x1;
            }
            m = StringUtils.translateForUser(u, original, m);
            if(StringUtils.checkForDomain(og)) {
                msg.then(u, m, TextOptions.KEEP_COLORS).hover("[c]Click to go to: ", "[pc]" + og).link(og);
                if(i < messageSplit.length+-1)msg.then(" ");
                continue;
            }
            og = og.replace("https://", "");
            B:for(int j = 1; j < 10; j++) {
                if(og.equalsIgnoreCase("["+j+"]")) {
                    ItemStack x = u.getBase().getInventory().getItem(j+-1);
                    if(x == null || x.getType() == Material.AIR) {
                        u.sendMessage("[c]Invalid inventory at HotBar Slot '[pc]" + j + "[c]'");
                        msg.then("[pc]Air"+msg.last);
                        if(i < messageSplit.length+-1)msg.then(" ");
                        continue A;
                    }
                    
                    String name =  "[pc]"+(x.hasItemMeta() & (x.getItemMeta().hasDisplayName()
                                                              | x.getItemMeta().hasLocalizedName()
                    ) ? (x.getItemMeta().hasLocalizedName() ? x.getItemMeta().getLocalizedName()
                            : x.getItemMeta().getDisplayName()) : StringUtils.getNameFromEnum(x.getType()).replace(" ", " [pc]"));
                    msg.then("[pc]" + name + msg.original + " [c]x" + x.getAmount() + msg.last).tooltip(x);
                    if(i < messageSplit.length+-1)msg.then(" ");
                    continue A;
                }
            }
            if(og.equalsIgnoreCase("[item]") || og.equalsIgnoreCase("[i]")) {
                ItemStack x = u.getBase().getInventory().getItemInMainHand();
                if(x.getType() == Material.AIR) {
                    invalidItem = true;
                    u.sendMessage("[c]Invalid item.");
                    break;
                }
                
                String name = "[pc]"+(x.hasItemMeta() & (x.getItemMeta().hasDisplayName()
                                                         | x.getItemMeta().hasLocalizedName()
                ) ? (x.getItemMeta().hasLocalizedName() ? x.getItemMeta().getLocalizedName()
                        : x.getItemMeta().getDisplayName()) : StringUtils.getNameFromEnum(
                        u.getBase().getInventory().getItemInMainHand().getType()).replace(" ", " [pc]"));
                msg.then("[pc]" + name + " x" + x.getAmount() + msg.last).tooltip(x);
                if(i < messageSplit.length+-1)msg.then(" ");
                continue;
            }
            String equation = ColorUtil.removeColor(m.contains("=")? m.split("=")[0] : m);
            if(NumberUtil.containsEquation(equation))
                try {
                    msg.then(u, m, TextOptions.KEEP_COLORS).hover("[pc]" + equation + "\n[c]=\n[pc]"+NumberUtil.calc(equation));
                    if(i < messageSplit.length+-1)msg.then(" ");
                    continue;
                }catch (Exception e2){
                
                }
            msg.then(u, m, TextOptions.KEEP_COLORS);
            if(i < messageSplit.length+-1)msg.then(" ");
        }
        if(toSend!=null)
            users.put(toSend, new MessageData(invalidItem, msg));
        return new MessageData(invalidItem, msg);
    }
    
    private boolean doSpace(String part1, String part2) {
        if(StringUtils.isEmpty(part1))
            if(!StringUtils.isEmpty(part2)&&part2.endsWith(" "))
                return true;
            else{}
        else{
            if(!StringUtils.isEmpty(part1)&&part1.endsWith(" "))
                return true;
        }
        return false;
    }
    
}