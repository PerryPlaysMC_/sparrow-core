package me.perryplaysmc.exceptions;

import me.perryplaysmc.utils.string.StringUtils;

public class CommandException extends Exception {

    private String message;
    private boolean preset = true;

    public CommandException(String message) {
        super(message);
        this.message=message;
    }

    @Override
    public String getMessage() {
        String pre = "";//preset ? "[c]Error while performing command: \n[pc]" : "";
        return StringUtils.translate(pre + message);
    }
}
