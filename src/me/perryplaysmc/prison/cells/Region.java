package me.perryplaysmc.prison.cells;

import me.perryplaysmc.Main_Core;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.utils.string.TimeUtil;
import me.perryplaysmc.world.LocationUtil;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.time.Month;
import java.util.*;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/6/19-2023
 * Package: me.perryplaysmc.prison.cells
 * Path: me.perryplaysmc.prison.cells.Region
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Region {
    
    private List<Block> outSide, box;
    private Location min, max;
    private List<Entity> entities = new ArrayList<>();
    private int index = 0;
    private int i = 0;
    private int amtInterval = 5;
    private List<Location> spawnedEntities = new ArrayList<>();
    
    public Region(Location min, Location max) {
        this.min = min;
        this.max = max;
        load();
    }
    public Location getMin() {
        return min;
    }
    
    public void setMin(Location min) {
        this.min = min;
    }
    
    public Location getMax() {
        return max;
    }
    
    public void setMax(Location max) {
        this.max = max;
    }
    
    public interface Loop {
        void loop(Block b);
    }
    
    public void loop(Loop loop) {
        for (int y = min.getBlockY(); y < max.getBlockY() + 1; y++) {
            for (int x = min.getBlockX(); x < max.getBlockX() + 1; x++) {
                for (int z = min.getBlockZ(); z < max.getBlockZ() + 1; z++) {
                    Location loc = new Location(min.getWorld(), x, y, z);
                    loop.loop(loc.getBlock());
                }
            }
        }
    }
    
    public void load() {
        outSide = new ArrayList<>();
        box = new ArrayList<>();
        int rXMin = (int)Math.min(min.getBlockX(), max.getBlockX());
        int rXMax = (int)Math.max(min.getBlockX(), max.getBlockX());
        int rYMin = (int)Math.min(min.getBlockY(), max.getBlockY());
        int rYMax = (int)Math.max(min.getBlockY(), max.getBlockY());
        int rZMin = (int)Math.min(min.getBlockZ(), max.getBlockZ());
        int rZMax = (int)Math.max(min.getBlockZ(), max.getBlockZ());
        max = new Location(max.getWorld(), rXMax, rYMax, rZMax);
        min = new Location(min.getWorld(), rXMin, rYMin, rZMin);
        for(int y = min.getBlockY(); y < max.getBlockY()+1; y++) {
            for(int x = min.getBlockX(); x < max.getBlockX()+1; x++) {
                for(int z = min.getBlockZ(); z < max.getBlockZ()+1; z++) {
                    Location loc = new Location(min.getWorld(), x, y, z);
                    if(isOutline(loc) || isCorner(loc))
                        if(!box.contains(loc.getBlock()))
                            box.add(loc.getBlock());
                }
            }
        }
    }
    
    public boolean isSimilar(Region r) {
        if(r==null||getMax()==null||r.getMax()==null||getMin()==null||r.getMin()==null)return false;
        if(LocationUtil.isSimilar(getMax(), r.getMax()) && LocationUtil.isSimilar(getMin(), r.getMin()))
            return true;
        return false;
    }
    
    public boolean isWithinBoarder(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        return ((x >= xmin && x <= xmax) && (y>=ymin&&y<=ymax) && (z >= zmin && z <= zmax));
    }
    
    public boolean isWallOrCeiling(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        return ((x == xmin || x == xmax) && (y>=ymin&&y<=ymax) && (z == zmin || z == zmax));
    }
    
    public boolean isCorner(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        if(x==xmin&&z==zmin) return true;
        if(x==xmax&&z==zmax) return true;
        if(x==xmin&&z==zmax) return true;
        if(x==xmax&&z==zmin) return true;
        return false;
    }
    
    public boolean isTop(Location l) {
        int ymax = (max.getBlockY());
        int y = l.getBlockY();
        return y==ymax;
    }
    
    public boolean isBottom(Location l) {
        int ymin = (min.getBlockY());
        int y = l.getBlockY();
        return y==ymin;
    }
    
    public List<Block> getOutSide() {
        return outSide;
    }
    
    public List<Block> getBox() {
        return box;
    }
    
    
    public CornerType getCorner(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        if(!(y==ymin||y==ymax)&&((x==xmin&&z==zmin)||(x==xmax&&z==zmax)||(x==xmin&&z==zmax)||(x==xmax&&z==zmin))) {
            return CornerType.Y;
        }
        if(x==xmin&&z==zmin) return CornerType.X_MIN_Z_MIN;
        if(x==xmax&&z==zmax) return CornerType.X_MAX_Z_MAX;
        if(x==xmin&&z==zmax) return CornerType.X_MIN_Z_MAX;
        if(x==xmax&&z==zmin) return CornerType.X_MAX_Z_MIN;
        return CornerType.NONE;
    }
    
    public boolean isOutline(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        if(isCorner(l)) return true;
        if(!(y==ymin||y==ymax))return false;
        return (x == xmin || x == xmax) || (z == zmin || z == zmax);
    }
    
    public Direction getDirectionExact(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        //if(isCorner(l)) return Direction.NONE;
        if(x == xmin) return Direction.NX;
        if(x == xmax) return Direction.PX;
        if(z == zmin) return Direction.NX;
        if(z == zmax) return Direction.PX;
        return Direction.NONE;
    }
    
    public Direction getDirection(Location l) {
        int xmax = (max.getBlockX());
        int xmin = (min.getBlockX());
        int ymax = (max.getBlockY());
        int ymin = (min.getBlockY());
        int zmax = (max.getBlockZ());
        int zmin = (min.getBlockZ());
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        if(isCorner(l)) return Direction.NONE;
        if(!(y==ymin||y==ymax))return Direction.NONE;
        if(x==xmin||x==xmax)return Direction.X;
        if(z==zmin||z==zmax)return Direction.Z;
        return Direction.NONE;
    }
    
    
    public enum CornerType {
        Y,
        X_MIN_Z_MIN,
        X_MAX_Z_MAX,
        X_MIN_Z_MAX,
        X_MAX_Z_MIN,
        NONE
    }
    public enum Direction {
        X,Z,
        PX,
        PY,
        PZ,
        NX,
        NY,
        NZ,
        NONE
    }

    void doEntities() {
        for(Entity e : entities) {
            Main_Core.getInstance().scoreboard.getTeam("shulker").removeEntry(e.getUniqueId().toString());
            e.remove();
        }
        entities.clear();
    }

    void loadEntity(Block b) {
        Location l = b.getLocation();
        World world = b.getWorld();
        Location l2 = l.clone().add(0.5, 0.5, 0.5);
        if(NumberUtil.isEven(l.getBlockX())&&NumberUtil.isEven(l.getBlockZ()))
            spawnEntity(world, l2);
    }

    void spawnEntity(World w, Location l) {
        (new BukkitRunnable() {
            @Override
            public void run() {
                if(!spawnedEntities.contains(l)) {
                    Slime s = (Slime) w.spawnEntity(l, EntityType.SLIME);
                    entities.add(s);
                    s.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 9999, 1, true, false));
                    s.setGravity(false);
                    s.setAI(false);
                    s.setInvulnerable(true);
                    s.setGlowing(true);
                    s.setSize(1);
                    s.setCollidable(false);
                    Main_Core.getInstance().scoreboard.getTeam("shulker").addEntry(s.getUniqueId().toString());
                    spawnedEntities.add(l);
                }
            }
        }).runTaskLater(Core.getAPI(), i);
        i++;
    }

    public void clear() {
        for(Entity e : entities)  {
            e.remove();
            Main_Core.getInstance().scoreboard.getTeam("shulker").removeEntry(e.getUniqueId().toString());
        }
        entities.clear();
        spawnedEntities.clear();
    }

    public void outLine(boolean entities, Player p) {
        if(entities) {
            for (int i = 0; i < amtInterval; i++) {
                if(index >= getBox().size())return;
                Block b = getBox().get(index);
                loadEntity(b);
                index++;
            }
            return;
        }
        for(Block b : getBox()) {
            Location l = b.getLocation();
            if(l.getBlock().getType().isOccluding())continue;
            if(!p.isOnline()) {
                return;
            }
            if(p.isOnline() && l.distance(p.getLocation()) > 65) continue;
            World world = b.getWorld();
            Location l2 = l.clone().add(0.5, 0.5, 0.5);
            if(isCorner(l)) {
                Region.CornerType t = getCorner(l);
                if(t == Region.CornerType.Y) {
                    for (double a = -0.5; a < 0.5; a += 0.2) {
                        sendParticle(world, l2.clone().add(0, a, 0));
                    }
                }
                if(t == Region.CornerType.X_MAX_Z_MAX) {
                    for (double a = 0; a < 0.5; a += 0.2) {
                        sendParticle(world, l2.clone().add(-a, 0, 0));
                        sendParticle(world, l2.clone().add(0, 0, -a));
                    }
                }
                if(t == Region.CornerType.X_MAX_Z_MIN) {
                    for (double a = 0; a < 0.5; a += 0.2) {
                        sendParticle(world, l2.clone().add(-a, 0, 0));
                        sendParticle(world, l2.clone().add(0, 0, a));
                    }
                }
                if(t == Region.CornerType.X_MIN_Z_MAX) {
                    for (double a = 0; a < 0.5; a += 0.2) {
                        sendParticle(world, l2.clone().add(a, 0, 0));
                        sendParticle(world, l2.clone().add(0, 0, -a));
                    }
                }
                if(t == Region.CornerType.X_MIN_Z_MIN) {
                    for (double a = 0; a < 0.5; a += 0.2) {
                        sendParticle(world, l2.clone().add(a, 0, 0));
                        sendParticle(world, l2.clone().add(0, 0, a));
                    }
                }
                if(isTop(l)) {
                    for (double a = -0.5; a < 0; a += 0.2) {
                        sendParticle(world, l2.clone().add(0, a, 0));
                    }
                }
                if(isBottom(l)) {
                    for (double a = 0.5; a > 0; a -= 0.2) {
                        sendParticle(world, l2.clone().add(0, a, 0));
                    }
                }
                continue;
            }

            if(getDirection(l2) == Region.Direction.Z) {
                for (double a = -0.5; a < 1; a += 0.2) {
                    sendParticle(world, l2.clone().add(a, 0, 0));
                }
            }
            if(getDirection(l2) == Region.Direction.X) {
                for (double a = -0.5; a < 1; a += 0.2) {
                    sendParticle(world, l2.clone().add(0, 0, a));
                }
            }
        }
    }
    int i1 = 0;
    void sendParticle(World w, Location l) {
        try{
            float red = 0;
            float green = 255;
            float blue = 0;
            w.spawnParticle(Particle.REDSTONE, l.getX(), l.getY(), l.getZ(), 0, 0.001, 10, 0, 1);
        }catch (Exception e) {
            try{
                Date d = Calendar.getInstance().getTime();
                int monthI = d.getMonth()+1;
                Particle.DustOptions options = new Particle.DustOptions(Color.fromRGB(0, 255, 0), 1);
                if(NumberUtil.isEven(i1) && Month.of(monthI) == Month.DECEMBER) {
                    options = new Particle.DustOptions(Color.fromRGB(255, 0, 0), 1);
                }
                if(NumberUtil.isEven(i1) && Month.of(monthI) == Month.NOVEMBER) {
                    options = new Particle.DustOptions(Color.fromRGB(218, 116, 0), 1);
                }
                if(!NumberUtil.isEven(i1) && Month.of(monthI) == Month.NOVEMBER) {
                    options = new Particle.DustOptions(Color.fromRGB(255, 242, 112), 1);
                }
                w.spawnParticle(Particle.REDSTONE, l, 2, 0, 0, 0, 1, options);
                i1++;
            }catch (Exception e1) {

            }
        }
    }
    
    private int findOffSet(int i, int j) {
        int ret = 0;
        if(i > j) {
            int x = 0;
            for(int a = j; a < i; a++) {
                x++;
            }
            return x;
        }
        if(j > i) {
            int x = 0;
            for(int a = i; a < j; a++) {
                x++;
            }
            return x;
        }
        return 0;
    }
}
