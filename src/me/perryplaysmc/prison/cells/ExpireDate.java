package me.perryplaysmc.prison.cells;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/26/19-2023
 * Package: me.perryplaysmc.prison.cells
 * Path: me.perryplaysmc.prison.cells.CellDate
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class ExpireDate {
    
    private int days, hours, minutes;
    private Long time;
    private Date date;

    public ExpireDate(int[] time) {
        this.days = time[0];
        this.hours = time[1];
        this.minutes = time[2];
    }
    public ExpireDate(int days) {
        this.days = days;
    }
    public ExpireDate(int days, int hours) {
        this.days = days;
        this.hours = hours;
    }
    public ExpireDate(int days, int hours, int minutes) {
        this.days = days;
        this.hours = hours;
        this.minutes = minutes;
    }
    
    public long getTime(){
        int days = getDays();
        int hours = (days+getHours())*24;
        int minutes = (hours+getMinutes())*60;
        int seconds = (minutes)*60;
        return seconds*1000;
    }
    
    
    public int getDays() {
        return days;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }


    public String toString() {
        return getDays()+"d";
    }
    
    
}
