package me.perryplaysmc.prison.cells;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.core.configuration.Configuration;
import me.perryplaysmc.exceptions.InvalidLocationException;
import me.perryplaysmc.prison.ah.AuctionHouseManager;
import me.perryplaysmc.user.User;
import me.perryplaysmc.user.data.PlayerData;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.utils.string.TimeUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.*;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/26/19-2023
 * Package: me.perryplaysmc.prison.cells
 * Path: me.perryplaysmc.prison.cells.PrisonCell
 * <p>
 * Any attempts to use these program(sign) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class PrisonCell {


    private Location max, min;
    private Region region;
    private UUID owner;
    private Date rent;
    private ExpireDate maxExpire;
    private double price, renewPrice;
    private String group;
    private int id;
    private BukkitTask task;
    private Sign sign;
    private Config sec;
    private Configuration cfg;

    public PrisonCell(String group, int id) {
        this.group = group;
        this.id = id;
        sec = PrisonCellManager.getConfig(group);
        setSection();
        this.max = cfg.getLocation("Location.max");
        this.min = cfg.getLocation("Location.min");
        this.region = new Region(min, max);
        this.price = cfg.getDouble("Price");
        this.rent = new Date(cfg.getLong("RentDate"));
        this.maxExpire = new ExpireDate(cfg.getInt("ExpireTime.days"));
        if (cfg.isSet("Location.Sign") && cfg.getLocation("Location.Sign").getBlock().getType().name().contains("SIGN"))
            this.sign = (Sign) cfg.getLocation("Location.Sign").getBlock().getState();
        else cfg.set("Location.Sign", null);
        if (cfg.isSet("owner"))
            this.owner = UUID.fromString(cfg.getString("owner"));
        PrisonCellManager.addCell(this);
    }

    public PrisonCell(String group, Region region, ExpireDate maxExpire, double price) {
        this.region = region;
        this.max = region.getMax();
        this.min = region.getMin();
        this.price = price;
        this.maxExpire = maxExpire;
        this.group = group;
        this.id = PrisonCellManager.getCells(group).size() + 1;
        PrisonCellManager.addCell(this);
        sec = PrisonCellManager.getConfig(group);
        setSection();
        cfg.set("Price", price);
        cfg.set("Location.max", max);
        cfg.set("Location.min", min);
        cfg.set("ExpireTime.days", maxExpire.getDays());
    }

    private void findMinMax() {
        if (max == null || min == null) {
            try {
                throw new InvalidLocationException("Please set both locations.");
            } catch (InvalidLocationException e) {
                e.printStackTrace();
            }
        }
        if (max.getWorld().getName() != min.getWorld().getName()) {
            try {
                throw new InvalidLocationException("Cannot create points between two different worlds.");
            } catch (InvalidLocationException e) {
                e.printStackTrace();
            }
            return;
        }
        int rXMin = (int) Math.min(min.getBlockX(), max.getBlockX());
        int rXMax = (int) Math.max(min.getBlockX(), max.getBlockX());
        int rYMin = (int) Math.min(min.getBlockY(), max.getBlockY());
        int rYMax = (int) Math.max(min.getBlockY(), max.getBlockY());
        int rZMin = (int) Math.min(min.getBlockZ(), max.getBlockZ());
        int rZMax = (int) Math.max(min.getBlockZ(), max.getBlockZ());
        max = new Location(max.getWorld(), rXMax, rYMax, rZMax);
        min = new Location(min.getWorld(), rXMin, rYMin, rZMin);
    }

    public void setRegion(Region region) {
        this.region = region;
        region.load();
    }

    public void updateRegion() {
        region.load();
        this.max = region.getMax();
        this.min = region.getMin();
        cfg.set("Location.max", region.getMax());
        cfg.set("Location.min", region.getMin());
    }

    public void setSign(Sign sign) {
        this.sign = sign;
        cfg.set("Location.Sign", sign.getLocation());
    }

    public Sign getSign() {
        return this.sign;
    }

    public Date getRentDate() {
        return rent;
    }

    public void rent(Player player) {
        User u = CoreAPI.getUser(player);
        if (isRented() &&
                getOwner()!=null&&u.getUniqueId().toString().equalsIgnoreCase(getOwner().toString())
                || player.getName().equalsIgnoreCase(getOwnerName())
        ) {
            setRent(new Date(System.currentTimeMillis()));
            return;
        }
        setRent(new Date(System.currentTimeMillis()));
        setOwner(player.getUniqueId());
    }

    public void setRent(Date rent) {
        if (rent == null)
            cfg.set("RentDate", null);
        else
            cfg.set("RentDate", rent.getTime());
        this.rent = rent;
    }

    public boolean isRented() {
        return owner != null && rent != null;
    }

    public long getTimeLeft() {
        if (this.isRented()) {
            return (getTotalTime()) + -(getElapsedTime());
        }
        return 0L;
    }

    public long getElapsedTime() {
        if (this.isRented()) {
            long start = TimeUtil.convert(rent);
            long now = TimeUtil.convert(new Date());
            return now + -start;
        }
        return 0L;
    }

    public long getTotalTime() {
        return maxExpire.getTime();
    }

    public BlockFace getSignFace() {
        byte data = sign.getData().getData();
        org.bukkit.material.Sign s = new org.bukkit.material.Sign(sign.getType(), sign.getRawData());
        if(Version.isVersionHigherThan(true, Versions.v1_13_R2)) {
            Directional dir = (Directional) sign.getBlockData();
            return dir.getFacing().getOppositeFace();
        }
        return s.getAttachedFace();
    }


    public void recalculatePrice(boolean force) {
        if(sign == null || (getSignFace() != null &&
                sign.getBlock().
                        getRelative(
                                getSignFace()
                                        .getOppositeFace())
                        .getType() ==
                        Material.BARRIER) ||
                (getSignFace() != null &&
                        sign.getBlock().
                                getRelative(
                                        getSignFace())
                                .getType() ==
                                Material.BARRIER)) return;
        long elapsed = getElapsedTime() / 1000;
        long total = getTotalTime() / 1000;
        //gets PricePerSecond >> Price / TotalTime
        double pps = price / total;
        //Calculates renewPrice >> renew = elapsed * PricePerSecond
        renewPrice = (double) Math.round((elapsed * pps) * 100) / 100;
        if(!isRented()) {
            String cell = "&2[Cell]",
                    renter = "",
                    time = "&0&o" + TimeUtil.formatDateDiffSign(getTotalTime()),
                    money = "&5&o$"+NumberUtil.format(NumberUtil.getMoney(price)).replace(",", "");
            setLine(cell, renter, time, money);
            return;
        }
        Date d = new Date(getTimeLeft());
        String cell = "&4[Cell]";
        String hold = "Unknown";
        try {
            hold = PlayerData.getName(owner) !=null ? PlayerData.getName(owner) : "Unknown";
        }catch (Exception e) {
            hold = "Unknown";
        }
        String renter = "&f&l" + (CoreAPI.getAllUser(owner)!=null ? CoreAPI.getAllUser(owner).getName() : hold);
        String time = "&0&o" + TimeUtil.formatDateDiffSign(getTimeLeft());
        String money = "&9$" + NumberUtil.format(NumberUtil.getMoney(renewPrice)).replace(",", "");
        if(elapsed>total || (CoreAPI.getSettings().getBoolean("Features.Cells.unrentWithoutPerm")
                && (owner == null || CoreAPI.getAllUser(owner) == null ? false :
                !CoreAPI.getAllUser(owner).getPermissionData().hasPermission("cells.cellblock." + group)))) {
            unrent();
            return;
        }
        if(((getElapsedTime()/1000)%60)==0||force)
            setLine(cell, renter, time, money);
    }

    public void unrent() {
        String cell = "&2[Cell]",
                renter = "",
                time = "&0&o" + TimeUtil.formatDateDiffSign(getTotalTime()),
                money = "&5&o$"+NumberUtil.format(NumberUtil.getMoney(price)).replace(",", "");
        String prev = CoreAPI.getUser(owner).getName();
        setOwner(null);
        setRent(null);
        if(AuctionHouseManager.getAH(getGroup())!=null) {
            final List<ItemStack>[] items = new List[]{new ArrayList<>()};
            List<List<ItemStack>> itemList = new ArrayList<>();
            region.loop((b) -> {
                if (b.getState() instanceof Container) {
                    Container c = (Container) b.getState();
                    for (ItemStack i1 : c.getInventory()) {
                        if (items[0].size() == InventoryType.CHEST.getDefaultSize()) {
                            itemList.add(items[0]);
                            items[0] = new ArrayList<>();
                        }
                        items[0].add(i1);
                    }
                } else
                    items[0].addAll(b.getDrops());
            });
            itemList.forEach(l -> {
                AuctionHouseManager.getAH(group).addChest(prev, l.toArray(new ItemStack[l.size()]));
            });
        }
        region.loop(b -> {
            b.setType(Material.AIR);
            b.getState().update(true);
        });
        loadDefaultBlocks();
        setLine(cell, renter, time, money);
    }

    public void setLine(String line1, String line2, String line3, String line4) {
        sign.setLine(0, StringUtils.translate(line1));
        sign.setLine(1, StringUtils.translate(line2));
        sign.setLine(2, StringUtils.translate(line3));
        sign.setLine(3, StringUtils.translate(line4));
        sign.update(true);
        sign.update(true);
        if(sign.getBlock().getState() instanceof Sign) {
            sign = (Sign) sign.getBlock().getState();
        }
    }


    public String getOwnerName() {
        String hold = "Unknown";
        if(owner == null) return hold;
        try {
            hold = PlayerData.getName(owner)!=null ? PlayerData.getName(owner) : "Unknown";
        }catch (Exception e) {

        }
        String renter = (CoreAPI.getAllUser(owner)!=null ? CoreAPI.getAllUser(owner).getName() : hold);
        return renter;
    }

    public void setOwner(UUID uuid) {
        if(uuid == null) cfg.set("owner", null);
        else cfg.set("owner", uuid.toString());
        this.owner = uuid;
    }

    public Region getRegion() {
        return region;
    }


    public ExpireDate getExpireDate() {
        return maxExpire;
    }

    public Location getPos1() {
        return max;
    }

    public Location getPos2() {
        return min;
    }

    public UUID getOwner() {
        return owner;
    }

    public double getRenewPrice() {
        return renewPrice;
    }

    public double getBuyCost() {
        return price;
    }

    public String getGroup() {
        return group.toLowerCase();
    }

    public int getId() {
        return id;
    }

    public void addDefaultBlock(Block block) {
        int size = cfg.getSection("DefaultBlocks").getKeys(false).size()+1;
        cfg.set("DefaultBlocks." + size + ".location", block.getLocation());
        cfg.set("DefaultBlocks." + size + ".type", block.getType());
        cfg.set("DefaultBlocks." + size + ".data", block.getState().getData().getData());
    }

    void loadDefaultBlocks() {
        for(String s : cfg.getSection("DefaultBlocks").getKeys(false)) {
            Block b = cfg.getLocation("DefaultBlocks."+s+".location").getBlock();
            BlockState state = b.getState();
            state.setType(ItemBuilder.convertMaterial(cfg.getString("DefaultBlocks."+s+".type")));
            state.setRawData(cfg.getByte("DefaultBlocks."+s+".data"));
            state.update(true);
        }
    }

    private void setSection() {
        if(CoreAPI.getSettings().getBoolean("Features.Cell.perGroupFile"))
            cfg = sec.getSection("Cells." + id);
        else
            cfg = sec.getSection("Cells."+getGroup()+"."+id);
    }

}
