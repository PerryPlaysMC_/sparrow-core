package me.perryplaysmc.prison.cells;

import com.avaje.ebeaninternal.server.el.ElSetValue;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.world.LocationUtil;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.*;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/26/19-2023
 * Package: me.perryplaysmc.prison.cells
 * Path: me.perryplaysmc.prison.cells.PrisonCellManager
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class PrisonCellManager {

    private static PrisonCellManager inst = new PrisonCellManager();
    private static Set<PrisonCell> cells = new HashSet<>();
    private static HashMap<String, Config> configs = new HashMap<>();
    private static List<String> groups = new ArrayList<>();

    public static List<PrisonCell> getCells(String group) {
        List<PrisonCell> groups = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();
        for(PrisonCell cell : cells) {
            if(!ids.contains(cell.getId()) && cell.getGroup().equalsIgnoreCase(group.toLowerCase()))
                ids.add(cell.getId());
        }

        Collections.sort(ids);
        for(int i : ids) {
            if(!groups.contains(getCell(group, i)))
                groups.add(getCell(group, i));
        }

        return groups;
    }
    public static List<String> getCells(OfflineUser u) {
        List<String> groups = new ArrayList<>();
        for(PrisonCell cell : cells) {
            if(((cell.getOwnerName()!=null &&
                    cell.getOwnerName().equalsIgnoreCase(u.getRealName())) ||
                    (cell.getOwner()!=null&&u.getUniqueId().toString().equals(cell.getOwner().toString()))))
                if(!groups.contains(cell.getGroup()))
                    groups.add(cell.getGroup());
        }
        return groups;
    }

    public static List<String> getGroups() {
        for(PrisonCell cell : cells) {
            if(groups.contains(cell.getGroup().toLowerCase()))continue;
            groups.add(cell.getGroup().toLowerCase());
        }
        return groups;
    }

    public static List<String> getCellIds(String group) {
        List<String> cells = new ArrayList<>();
        for(PrisonCell cell : getCells(group)) {
            if(cells.contains(cell.getId()+""))continue;
            cells.add(cell.getId()+"");
        }
        return cells;
    }


    public static PrisonCell getCell(String group, int id) {
        for(PrisonCell cell : cells) {
            if(cell.getGroup().equals(group.toLowerCase())&&cell.getId() == id)return cell;
        }
        return null;
    }


    public static void loadCells() {
        cells = new HashSet<>();
        configs = new HashMap<>();
        groups = new ArrayList<>();
        Config cfg = CoreAPI.getCells();
        if(CoreAPI.getSettings().getBoolean("Features.Cell.perGroupFile")) {
            File dir = new File(Config.defaultDirectory, "prisonCells");
            if(!dir.isDirectory() || dir.listFiles() == null || dir.listFiles().length == 0)return;
            for(File f : dir.listFiles()) {
                cfg = new Config(dir, f.getName());
                cfg.reload();
                String group = cfg.getLocalName().toLowerCase();
                for(String cellId : cfg.getSection("Cells."+group).getKeys(false)) {
                    PrisonCell p = new PrisonCell(group, Integer.parseInt(cellId));
                    if(p.getSign()!=null)p.recalculatePrice(true);
                }
            }
            loadSigns();
            return;
        }
        cfg.reload();
        for(String group : cfg.getSection("Cells").getKeys(false)) {
            for(String cellId : cfg.getSection("Cells."+group).getKeys(false)) {
                PrisonCell p = new PrisonCell(group.toLowerCase(), Integer.parseInt(cellId));
                if(p.getSign()!=null)p.recalculatePrice(true);
            }
        }
        loadSigns();
    }

    public static void refreshSigns() {
        for(PrisonCell cell : cells) {
            if(cell.isRented())
                cell.recalculatePrice(true);
            if(cell.getSign()!=null) {
                String[] lines = cell.getSign().getLines();
                boolean isEmpty = true;
                for(String s : lines) {
                    if(!StringUtils.isEmpty(s))
                        isEmpty = false;
                }
                cell.recalculatePrice(true);
            }
        }
    }

    private static void loadSigns() {
        (new BukkitRunnable() {
            @Override
            public void run() {
                for(PrisonCell cell : cells) {
                    if(cell.isRented())
                        cell.recalculatePrice(false);
                    if(cell.getSign()!=null) {
                        String[] lines = cell.getSign().getLines();
                        boolean isEmpty = true;
                        for(String s : lines) {
                            if(!StringUtils.isEmpty(s))
                                isEmpty = false;
                        }
                        cell.recalculatePrice(false);
                    }
                }
            }
        }).runTaskTimer(Core.getAPI(), 20, 20);
    }

    public static Config getConfig(String group) {
        if(CoreAPI.getSettings().getBoolean("Features.Cell.perGroupFile")) {
            for (PrisonCell cell : cells) {
                if(!configs.containsKey(cell.getGroup()))
                    configs.put(cell.getGroup(), new Config("prisonCells", cell.getGroup().toLowerCase() + ".yml"));
            }
            return configs.get(group);
        }else {
            return CoreAPI.getCells();
        }
    }


    public static List<PrisonCell> getClosestToPlayer(Player p, double maxRadius) {

        double closest = maxRadius;
        List<PrisonCell> cells = new ArrayList<>();
        for(PrisonCell d : getCells()) {
            if(d.getPos1().distance(p.getLocation()) < closest) {
                if(!cells.contains(d))
                    cells.add(d);
            }
            if(d.getPos2().distance(p.getLocation()) < closest) {
                if(!cells.contains(d))
                    cells.add(d);
            }
        }
        return cells;
    }

    public static PrisonCellManager addCell(PrisonCell cell) {
        if(!cells.contains(cell))
            cells.add(cell);
        return inst;
    }

    public static PrisonCellManager removeCell(PrisonCell cell) {
        if(cells.contains(cell))
            cells.remove(cell);
        return inst;
    }

    public static boolean cellExists(Region region) {
        for(PrisonCell cell : cells) {
            if(cell.getRegion().isSimilar(region))return true;
        }
        return false;
    }

    public static PrisonCell getCell(Sign s) {
        for(PrisonCell cell : cells) {
            if(cell!=null&&cell.getSign()!=null&&s!=null)
                if(LocationUtil.isSimilar(cell.getSign().getLocation(), s.getLocation()))
                    return cell;
        }
        return null;
    }

    public static PrisonCell getCell(UUID owner) {
        if(owner==null)return null;
        for(PrisonCell cell : cells) {
            if(cell.getOwner()==null)continue;
            if(cell.getOwner().toString().equals(owner.toString()))
                return cell;
        }
        return null;
    }

    public static PrisonCell getCell(String group, UUID owner) {
        if(owner==null)return null;
        for(PrisonCell cell : cells) {
            if(cell.getOwner()==null)continue;
            if(cell.getOwner().toString().equals(owner.toString())&&cell.getGroup().equalsIgnoreCase(group))
                return cell;
        }
        return null;
    }

    public static PrisonCell getCell(OfflineUser owner) {
        return getCell(owner.getUniqueId());
    }


    public static Set<PrisonCell> getCells() {
        return cells;
    }


}
