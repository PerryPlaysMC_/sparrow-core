package me.perryplaysmc.prison.ranks;

import me.perryplaysmc.user.OfflineUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/8/19-2023
 * Package: me.perryplaysmc.prison.ranks
 * Path: me.perryplaysmc.prison.ranks.RankDataHandler
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class RankDataHandler {
    
    public static List<RankData> ranks = new ArrayList<>();
    
    
    public static void addRank(RankData data) {
        if(!ranks.contains(data))
            ranks.add(data);
    }
    public static RankData getFirstRank() {
        int highest = Integer.MAX_VALUE;
        RankData ret = null;
        for(RankData r : ranks) {
            if(r.getIndex() < highest) {
                ret = null;
                highest = r.getIndex();
            }
        }
        return ret;
    }
    
    public static RankData getRank(String name) {
        for(RankData data : ranks) {
            if(data.getRank().equalsIgnoreCase(name))
                return data;
        }
        return null;
    }
    
    public static void rankUp(OfflineUser u) {
        boolean isPrestiege = false;
        if(u.getRankUtils().getRank().isLastRank()) {
            u.getRankUtils().getRank().setNextRank(getFirstRank().getRank());
            isPrestiege = true;
        }
        if(u.getAccount().hasEnough(u.getRankUtils().getRank().cost())) {
            u.getRankUtils().getRank().runCommands();
            u.getRankUtils().setRank(getRank(u.getRankUtils().getRank().getNextRank()));
            if(isPrestiege) {
                u.getRankUtils().addPrestiege();
            }
        }
    }
    
    
}
