package me.perryplaysmc.prison.ah;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.prison.cells.ExpireDate;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.utils.string.TimeUtil;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@SuppressWarnings("all")
public class ChestSign {

    private Chest chest;
    private Sign sign;
    private AuctionHouse ah;
    private UUID owner;
    private Date buyDate;
    private ExpireDate date;
    private double price;
    private String prevOwner;

    public ChestSign(AuctionHouse ah, String prevOwner, double price, Chest chest, Sign sign) {
        this.ah = ah;
        this.prevOwner = prevOwner;
        this.chest = chest;
        this.sign = sign;
        this.price = price;
        this.date = new ExpireDate(0, 0, 2);
        ah.addChestSign(this);
        updateSign();
    }

    public void buyChest(Player player) {
        this.owner = player.getUniqueId();
        this.buyDate = Calendar.getInstance().getTime();
    }
    public boolean isBought() {
        return owner!=null&&buyDate!=null;
    }

    public long getTimeLeft() {
        if(isBought()) {
            return (getTotalTime()) + -(getElapsedTime());
        }
        return 0L;
    }

    public long getElapsedTime() {
        if(isBought()) {
            long start = TimeUtil.convert(buyDate);
            long now = TimeUtil.convert(TimeUtil.getDate());
            return now +- start;
        }
        return 0L;
    }

    public long getTotalTime() {
        return date.getTime();
    }


    public void updateSign() {
//&e[BuyChest] Name ;; Price
        String cell = "&e[BuyChest]",
                renter = prevOwner,
                time = "",
                money = "$"+ NumberUtil.format(NumberUtil.getMoney(price)).replace(",", "");
        if(isBought()) {
            //&6[Chest] Name Time
            cell = "&6[Chest]";
            renter = CoreAPI.getUser(owner).getName();
            time = TimeUtil.formatDateDiffSign(getTimeLeft());
        }
        setLine(cell, renter, time, money);
    }
    public void setLine(String line1, String line2, String line3, String line4) {
        sign.setLine(0, StringUtils.translate(line1));
        sign.setLine(1, StringUtils.translate(line2));
        sign.setLine(2, StringUtils.translate(line3));
        sign.setLine(3, StringUtils.translate(line4));
        sign.update(true);
        sign.update(true);
        if(sign.getBlock().getState() instanceof Sign) {
            sign = (Sign) sign.getBlock().getState();
        }
    }


}
