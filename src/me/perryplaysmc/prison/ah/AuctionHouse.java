package me.perryplaysmc.prison.ah;

import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.core.configuration.Configuration;
import me.perryplaysmc.prison.cells.PrisonCellManager;
import me.perryplaysmc.prison.cells.Region;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import me.perryplaysmc.utils.string.NumberUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.*;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SuppressWarnings("all")
public class AuctionHouse {

    private Config sec;
    private Configuration cfg;
    private List<ChestSign> chests;
    private String group;
    List<Region> regions;


    public AuctionHouse(String group) {
        this.group = group;
        this.chests = new ArrayList<>();
        this.regions = new ArrayList<>();
        this.sec = PrisonCellManager.getConfig(group);
        if(sec.isSet("AuctionHouse.regions"))
            for(String s : sec.getSection("AuctionHouse.regions").getKeys(false)) {
                Location min = sec.getLocation("AuctionHouse.regions." + s + ".min");
                Location max = sec.getLocation("AuctionHouse.regions." + s + ".max");
                regions.add(new Region(min, max));
            }
    }


    public void addPossibility(Region region) {
        this.regions.add(region);

    }

    public void addChest(String prevOwner, ItemStack[] c) {
        Region r = regions.get(new Random().nextInt(regions.size()));
        int x = NumberUtil.newRandomInt(r.getMin().getBlockX(), r.getMax().getBlockX());
        int y = NumberUtil.newRandomInt(r.getMin().getBlockY(), r.getMax().getBlockY());
        int z = NumberUtil.newRandomInt(r.getMin().getBlockZ(), r.getMax().getBlockZ());
        Block loc = r.getMax().getWorld().getBlockAt(x, y, z);
        while(loc.getType().isSolid()) {
            x = NumberUtil.newRandomInt(r.getMin().getBlockX(), r.getMax().getBlockX());
            y = NumberUtil.newRandomInt(r.getMin().getBlockY(), r.getMax().getBlockY());
            z = NumberUtil.newRandomInt(r.getMin().getBlockZ(), r.getMax().getBlockZ());
            loc = r.getMax().getWorld().getBlockAt(x, y, z);
        }
        Material type = Material.CHEST;
        for(BlockFace f : new BlockFace[] {
                BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH, BlockFace.NORTH
        }) {
            if(loc.getRelative(f).getType()==Material.CHEST) {
                type =Material.TRAPPED_CHEST;
            }
            if(loc.getRelative(f).getType()==Material.TRAPPED_CHEST) {
                type = Material.CHEST;
            }
        }
        loc.setType(type);
        Chest chest = (Chest) loc.getState();
        for(ItemStack i : c) {
            int slot = new Random().nextInt(chest.getSnapshotInventory().getSize());
            while(chest.getSnapshotInventory().getItem(slot)!=null||chest.getSnapshotInventory().getItem(slot).getType()!=Material.AIR) {
                slot = new Random().nextInt(chest.getSnapshotInventory().getSize());
            }
            chest.getSnapshotInventory().setItem(slot, i);
        }
        chest.update(true);
        Sign sign = null;
        for(BlockFace f : new BlockFace[] {
                BlockFace.EAST, BlockFace.WEST, BlockFace.SOUTH, BlockFace.NORTH
        }) {
            Block b = chest.getBlock().getRelative(f);
            if(b.getType()==Material.AIR) {
                for(Region rg : regions) {
                    if(!rg.isWithinBoarder(b.getLocation())) {
                        BlockState state = b.getState();
                        state.setType(ItemBuilder.convertMaterial("WALL_SIGN"));
                        state.update(true);
                        org.bukkit.material.Sign s1 = new org.bukkit.material.Sign();
                        s1.setFacingDirection(f.getOppositeFace());
                        state.setData(s1);
                        state.update(true);
                        sign = (Sign) state;
                        break;
                    }
                }
            }
        }
        new ChestSign(this, prevOwner, NumberUtil.newRandomDouble(100, NumberUtil.getPercent(25, 120)), chest, sign);
    }

    protected void addChestSign(ChestSign sign) {
        if(!chests.contains(sign))
            chests.add(sign);
    }

    protected void removeChestSign(ChestSign sign) {
        if(chests.contains(sign))
            chests.remove(sign);
    }


    public String getGroup() {
        return group;
    }


    public void addRegion(Region region) {
        Configuration cfg1 = sec.getSection("AuctionHouse.regions");
        cfg1.set((regions.size()+1) + ".min", region.getMin());
        cfg1.set((regions.size()+1) + ".max", region.getMax());
    }
}
