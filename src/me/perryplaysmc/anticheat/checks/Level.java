package me.perryplaysmc.anticheat.checks;

import me.perryplaysmc.utils.string.StringUtils;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/12/19-2023
 * Package: me.perryplaysmc.anticheat.checks
 * Path: me.perryplaysmc.anticheat.checks.Level
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public enum Level {
    
    MONITORING, FAILED, PASSED;
    
    public String getName() {
        return StringUtils.getNameFromEnum(this);
    }
    
}
