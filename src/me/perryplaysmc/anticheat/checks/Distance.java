package me.perryplaysmc.anticheat.checks;

import org.bukkit.Location;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/12/19-2023
 * Package: me.perryplaysmc.anticheat.checks
 * Path: me.perryplaysmc.anticheat.checks.Distance
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Distance {
    
    private Location from, to;
    
    private Double xDiff, yDiff, zDiff;
    
    public Distance(PlayerMoveEvent e) {
        this.from = e.getFrom();
        this.to = e.getTo();
        this.xDiff = (from.getX() > to.getX() ? from.getX() : to.getX()) -(from.getX() < to.getX() ? from.getX() : to.getX());
        this.yDiff = (from.getY() > to.getY() ? from.getY() : to.getY()) -(from.getY() < to.getY() ? from.getY() : to.getY());
        this.zDiff = (from.getZ() > to.getZ() ? from.getZ() : to.getZ()) -(from.getZ() < to.getZ() ? from.getZ() : to.getZ());
    }
    
    public Double getDist() {
        return getzDiff() > getxDiff() ? getzDiff() : getxDiff();
    }
    
    public Double getxDiff() {
        return xDiff;
    }
    
    public Double getyDiff() {
        return yDiff;
    }
    
    public Double getzDiff() {
        return zDiff;
    }
    
    
    
}
