package me.perryplaysmc.anticheat.checks.movement;

import me.perryplaysmc.anticheat.checks.*;
import me.perryplaysmc.user.User;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/13/19-2023
 * Package: me.perryplaysmc.anticheat.checks.movement
 * Path: me.perryplaysmc.anticheat.checks.movement.FastUse
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class FastUse {
    
    private static final CheckResult PASS = new CheckResult(Level.PASSED, CheckType.FASTUSE, "Nothing to report");
    
    public static CheckResult runBow(User u) {
        if(u.hasPermission(CheckType.SPEED.getPermission(), "anticheat.bypass.*")) return new CheckResult(Level.PASSED, CheckType.SPEED, "Nothing to report");
        long now = System.currentTimeMillis();
        if(u.getAntiCheatDetector().hasBowStarted() && (now - u.getAntiCheatDetector().getBowStart()) < Settings.MIN_BOW)
            return new CheckResult(Level.FAILED, CheckType.FASTUSE, "tried to shoot too fast").tooltip(
                    "[c]Speed: ([pc]" + (now - u.getAntiCheatDetector().getBowStart()) + "[c])",
                    "[c]Min: ([pc]" + Settings.MIN_BOW + "[c])"
            );
        return PASS;
    }
    public static CheckResult runFood(User u) {
        if(u.hasPermission(CheckType.SPEED.getPermission(), "anticheat.bypass.*")) return new CheckResult(Level.PASSED, CheckType.SPEED, "Nothing to report");
        long now = System.currentTimeMillis();
        if(u.getAntiCheatDetector().hasFoodStarted() && (now - u.getAntiCheatDetector().getFood()) < Settings.MIN_BOW)
            return new CheckResult(Level.FAILED, CheckType.FASTUSE, "tried to eat too fast").tooltip(
                    "[c]Speed: ([pc]" + (now - u.getAntiCheatDetector().getFood()) + "[c])",
                    "[c]Min: ([pc]" + Settings.MIN_BOW + "[c])"
            );
        return PASS;
    }
}
