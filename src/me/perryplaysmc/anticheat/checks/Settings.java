package me.perryplaysmc.anticheat.checks;

import me.perryplaysmc.chat.Message;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.ListsHelper;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.bukkit.Material.*;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/12/19-2023
 * Package: me.perryplaysmc.anticheat.checks
 * Path: me.perryplaysmc.anticheat.checks.Settings
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Settings {
    
    public static final Double MAX_XZ_SPEED = 2.1;//3.07;//1.62;//10.8;//3.06;//1.4023;//0.67;
    public static final Double MAX_XZ_JUMP_SPEED = 3.081;
    public static final Double MAX_XZ_EATING_SPEED = 0.10178;
    public static final Double MAX_XZ_BLOCKING_SPEED = 0.12;
    public static final Double MAX_XZ_SNEAK_SPEED = 4.72;//1.1;//0.08;//0.3566957753563713;//0.074831964424629
    public static final Double MAX_XZ_FLY_SPEED = 10.888;//5.122;//1.0875;//365;//1.0139892647933664;//1.0095700214842848;//0.3566957753563713;//0.074831964424629
    public static final Double MAX_XZ_SNEAK_FLY_SPEED = 5.035;//0.9077949510160579;//0.8885014979905463;//0.8654761606672037;//0.3566957753563713;//0.074831964424629
    public static final Long MIN_BOW = 100l;
    public static final Long MIN_FOOD = 1000l;
    
    public static final List<Material> FOOD;
    
    static {
        ListsHelper<Material> ma = ListsHelper.createList(values());
        ListsHelper<Material> mats = ListsHelper.createList();
        ma.forEach((m) -> {if(m.isEdible())mats.add(m);});
        FOOD = mats.getList();
    }
    
    
    
    
    
    public static void log(CheckResult r, User u) {
        ListsHelper<String> string = ListsHelper.createList("[c]Hack-Test: [pc]" + r.getLevel().getName(),
                "[c]Hack-Type: [pc]" + r.getType().getName());
        string.addAll(r.getTooltip());
        Message msg = new Message("[ac][pc]" + u.getRealName() + "[c]: "
                                  + r.getMessage() + " &7&o(Hover for more details!)").tooltip(string.getList());
        if(!StringUtils.isEmpty(r.getSuggest()))msg.suggest(r.getSuggest());
        for(User user : CoreAPI.getOnlineUsers()) {
            if(user.hasPermission("anticheat.notify"))
                msg.send(user);
        }
        msg = new Message("\n[ac][pc]" + u.getRealName() + "[c]: "
                          + r.getMessage() + "\n -"+string.toString("\n -"));
        msg.send(CoreAPI.getConsole());
    }
    
}
