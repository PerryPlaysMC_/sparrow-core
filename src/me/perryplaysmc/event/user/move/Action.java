package me.perryplaysmc.event.user.move;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2023
 * Package: me.perryplaysmc.event.user
 * Path: me.perryplaysmc.event.user.move.Action
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/
public enum Action {
    MOVE,  MOVE_X,
    MOVE_Y,  MOVE_Z,
    MOVE_X_Z,  MOVE_Z_Y,
    MOVE_X_Y,  JUMP,
    PITCH,  YAW,
    YAW_RIGHT,  YAW_LEFT,
    PITCH_UP,  PITCH_DOWN,
    PITCH_YAW,  FALL,
    PITCH_UP_YAW_LEFT,
    PITCH_UP_YAW_RIGHT,
    PITCH_DOWN_YAW_LEFT,
    PITCH_DOWN_YAW_RIGHT,  NULL;

}