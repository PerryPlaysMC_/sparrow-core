package me.perryplaysmc.event.user;

import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class UserJoinEvent extends Event {
    
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private User u;
    private String message;
    
    public UserJoinEvent(User user, String message) {
        u = user;
        this.message = StringUtils.translate(message);
    }
    
    public String getMessage() {
        return StringUtils.translate(message);
    }
    
    public void setMessage(String message) {
        this.message = StringUtils.translate(message);
    }
    
    public User getUser() {
        return u;
    }
    
    public Player getPlayer() {
        return u.getBase();
    }
    
    public boolean isCancelled() {
        return cancelled;
    }
    
    public void setCancelled(boolean newVal, String message){
        this.cancelled = newVal;
        u.kick(message);
    }
    
    public HandlerList getHandlers()
    {
        return handlers;
    }
    
    
    public static HandlerList getHandlerList()
    {
        return handlers;
    }
}
