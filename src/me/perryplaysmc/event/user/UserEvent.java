package me.perryplaysmc.event.user;

import me.perryplaysmc.user.User;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

public abstract class UserEvent extends Event {

  private User user;

  public UserEvent(User user) {
    super(false);
    this.user = user;
  }

  public UserEvent(User user, boolean isAsync) {
    super(isAsync);
    this.user = user;
  }

  public User getUser()
  {
    return user;
  }

  public Player getPlayer() {
    return user.getBase();
  }
}
