package me.perryplaysmc.event.user;

import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.user.User;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class UserBanEvent extends Event implements Cancellable {

  private static final HandlerList handlers = new HandlerList();
  
  private User user;
  private CommandSource banner;
  private String msg;
  private boolean cancelled = false;
  
  public UserBanEvent(CommandSource banner, User user, String msg) {
    this.msg = msg;
    this.user = user;
    this.banner = banner;
  }
  
  public void setMessage(String newMessage) {
    msg = newMessage;
  }
  
  public String getMessage() {
    return msg;
  }
  
  public CommandSource getBanner() {
    return banner;
  }
  
  public boolean isCancelled()
  {
    return cancelled;
  }
  
  public void setCancelled(boolean cancel)
  {
    cancelled = cancel;
  }
  
  public User getUser() {
    return user;
  }
  
  public Player getPlayer() {
    return user.getBase();
  }
  
  public HandlerList getHandlers()
  {
    return handlers;
  }
  
  public static HandlerList getHandlerList() {
    return handlers;
  }
}
