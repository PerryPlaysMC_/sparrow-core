package me.perryplaysmc.event.user;

import me.perryplaysmc.event.user.block.Action;
import me.perryplaysmc.user.User;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class UserInteractEvent extends UserEvent implements Cancellable {
    
    private static final HandlerList handlers = new HandlerList();
    private Action action;
    private Action actionType;
    private Block clicked_underPlayerBlock; private boolean isCancelled = false;
    
    public UserInteractEvent(User user, Action action, Action actionType, Block clicked_underPlayerBlock) {
        super(user);
        this.action = action;
        this.actionType = actionType;
        this.clicked_underPlayerBlock = clicked_underPlayerBlock;
    }
    
    public Block getBlock() { return clicked_underPlayerBlock; }
    
    public Action getAction()
    {
        return action;
    }
    
    public Action getActionType() { return actionType; }
    
    
    public boolean isCancelled()
    {
        return isCancelled;
    }
    
    public void setCancelled(boolean b)
    {
        isCancelled = b;
    }
    
    public HandlerList getHandlers()
    {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
