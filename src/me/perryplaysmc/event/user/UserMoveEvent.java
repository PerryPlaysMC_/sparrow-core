package me.perryplaysmc.event.user;

import me.perryplaysmc.event.user.move.Action;
import me.perryplaysmc.user.User;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class UserMoveEvent extends UserEvent implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private Action action;
    private Action actionType;
    private boolean isCancelled = false;
    private Location to;
    private Location from;

    public UserMoveEvent(User user, Action action, Action actionType, Location to, Location from) {
        super(user);
        this.action = action;
        this.actionType = actionType;
        this.to = to;
        this.from = from;
    }

    public Action getAction()
    {
        return action;
    }
    
    public Action getActionType() { return actionType; }
    
    
    public boolean isCancelled()
    {
        return isCancelled;
    }
    
    public void setCancelled(boolean b)
    {
        isCancelled = b;
    }
    
    public HandlerList getHandlers()
    {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    public Location getTo() {
        return to;
    }
    
    public void setTo(Location to) {
        this.to = to;
    }
    
    public Location getFrom() {
        return from;
    }
    
    public void setFrom(Location from) {
        this.from = from;
    }
    
}
