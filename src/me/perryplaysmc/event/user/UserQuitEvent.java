package me.perryplaysmc.event.user;

import me.perryplaysmc.user.OfflineUser;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class UserQuitEvent extends Event {

  private static final HandlerList handlers = new HandlerList();
  private OfflineUser u;
  private String message;
  
  public UserQuitEvent(OfflineUser user, String message) {
    u = user;
    this.message = message;
  }
  
  public String getMessage() {
    return message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }
  
  public OfflineUser getUser() {
    return u;
  }
  
  public OfflinePlayer getPlayer() {
    return u.getBase();
  }
  
  public HandlerList getHandlers()
  {
    return handlers;
  }
  

  public static HandlerList getHandlerList()
  {
    return handlers;
  }
}
