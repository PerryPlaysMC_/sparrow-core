package me.perryplaysmc.permissions;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.permissions.group.Group;
import me.perryplaysmc.permissions.group.GroupManager;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/5/19-2023
 * Package: me.perryplaysmc.permissions
 * Path: me.perryplaysmc.permissions.PermissionsImpl
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class PermissionsImpl extends Permission {
    private final String name =  Core.getAPI().getSettings().getString("settings.main-name");
    
    public PermissionsImpl(Plugin plugin) {
        this.plugin = plugin;
        
        // Load Plugin in case it was loaded before
        Plugin perms = plugin.getServer().getPluginManager().getPlugin("Core");
        if (perms != null) {
            if (perms.isEnabled()) {
                try {
                    if (Double.valueOf(perms.getDescription().getVersion()) < 1.16) {
                        log.info(String.format("[%s][Permission] %s below 1.16 is not compatible with Vault! Falling back to SuperPerms only mode. PLEASE UPDATE!", plugin.getDescription().getName(), name));
                    }
                } catch (NumberFormatException e) {
                    // Do nothing
                }
                log.info(String.format("[%s][Permission] %s hooked.", plugin.getDescription().getName(), name));
            }
        }
    }
    
    @Override
    public boolean isEnabled() {
        return true;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public boolean playerInGroup(String worldName, OfflinePlayer op, String groupName) {
        OfflineUser user = getUser(op);
        if (user == null) {
            return false;
        }
        return user.getPermissionData().getGroups().contains(GroupManager.getGroup(groupName));
    }
    
    @Override
    public boolean playerInGroup(String worldName, String playerName, String groupName) {
        return getUser(playerName).getPermissionData().getGroups().contains(GroupManager.getGroup(groupName));
    }
    
    @Override
    public boolean playerAddGroup(String worldName, OfflinePlayer op, String groupName) {
        Group group = GroupManager.getGroup(groupName);
        OfflineUser user = getUser(op);
        if (group == null || user == null) {
            return false;
        } else {
            GroupManager.getGroup(groupName).addUser(user);
            return true;
        }
    }
    
    @Override
    public boolean playerAddGroup(String worldName, String playerName, String groupName) {
        Group group = GroupManager.getGroup(groupName);
        OfflineUser user = getUser(playerName);
        if (group == null || user == null) {
            return false;
        } else {
            GroupManager.getGroup(groupName).addUser(user);
            return true;
        }
    }
    
    @Override
    public boolean playerRemoveGroup(String worldName, OfflinePlayer op, String groupName) {
        OfflineUser user = getUser(op);
        GroupManager.getGroup(groupName).removeUser(user);
        return true;
    }
    
    @Override
    public boolean playerRemoveGroup(String worldName, String playerName, String groupName) {
        GroupManager.getGroup(groupName).removeUser(getUser(playerName));
        return true;
    }
    
    @Override
    public boolean playerAdd(String worldName, OfflinePlayer op, String permission) {
        OfflineUser user = getUser(op);
        if (user == null) {
            return false;
        } else {
            user.getPermissionData().addPermission(permission);
            return true;
        }
    }
    
    @Override
    public boolean playerAdd(String worldName, String playerName, String permission) {
        User user = CoreAPI.getUser(playerName);
        if (user == null) {
            return false;
        } else {
            user.getPermissionData().addPermission(permission);
            return true;
        }
    }
    
    @Override
    public boolean playerRemove(String worldName, OfflinePlayer op, String permission) {
        OfflineUser user = getUser(op);
        if (user == null) {
            return false;
        } else {
            user.getPermissionData().removePermission(permission);
            return true;
        }
    }
    
    @Override
    public boolean playerRemove(String worldName, String playerName, String permission) {
        User user = CoreAPI.getUser(playerName);
        if (user == null) {
            return false;
        } else {
            user.getPermissionData().removePermission(permission);
            return true;
        }
    }
    
    @Override
    public boolean groupAdd(String worldName, String groupName, String permission) {
        Group group = GroupManager.getGroup(groupName);
        if (group == null) {
            return false;
        } else {
            group.addPerm(permission);
            return true;
        }
    }
    
    @Override
    public boolean groupRemove(String worldName, String groupName, String permission) {
        Group group = GroupManager.getGroup(groupName);
        if (group == null) {
            return false;
        } else {
            group.removePerm(permission);
            return true;
        }
    }
    
    @Override
    public boolean groupHas(String worldName, String groupName, String permission) {
        Group group = GroupManager.getGroup(groupName);
        if (group == null) {
            return false;
        } else {
            return group.hasPermission(permission);
        }
    }
    
    private OfflineUser getUser(OfflinePlayer op) {
        return CoreAPI.getAllUser(op.getUniqueId());
    }
    
    private OfflineUser getUser(String playerName) {
        return CoreAPI.getAllUser(playerName);
    }
    
    @Override
    public String[] getPlayerGroups(String world, OfflinePlayer op) {
        OfflineUser user = getUser(op);
        return user == null ? null : user.getPermissionData().getGroups().toArray(new String[0]);
    }
    
    @Override
    public String[] getPlayerGroups(String world, String playerName) {
        User user = CoreAPI.getUser(playerName);
        return user == null ? null : CoreAPI.findNames(user.getPermissionData().getGroups()).toArray(new String[CoreAPI.findNames(user.getPermissionData().getGroups()).size()]);
    }
    
    @Override
    public String getPrimaryGroup(String world, OfflinePlayer op) {
        OfflineUser user = getUser(op);
        if (user == null) {
            return null;
        }else{
            return user.getPermissionData().getGroups().get(0).getName();
        }
    }
    
    @Override
    public String getPrimaryGroup(String world, String playerName) {
        OfflineUser user = getUser(playerName);
        if (user == null) {
            return null;
        }else{
            return user.getPermissionData().getGroups().get(0).getName();
        }
    }
    
    @Override
    public boolean playerHas(String worldName, OfflinePlayer op, String permission) {
        OfflineUser user = getUser(op);
        if (user != null) {
            return user.getPermissionData().hasSinglePermission(permission);
        } else {
            return false;
        }
    }
    
    @Override
    public boolean playerHas(String worldName, String playerName, String permission) {
        User user = CoreAPI.getUser(playerName);
        if (user != null) {
            return user.getPermissionData().hasSinglePermission(permission);
        } else {
            return false;
        }
    }
    
    
    @Override
    public boolean playerAddTransient(String worldName, Player player, String permission) {
        return false;
    }
    
    @Override
    public boolean playerAddTransient(Player player, String permission) {
        return playerAddTransient(null, player, permission);
    }
    
    
    @Override
    public boolean playerRemoveTransient(Player player, String permission) {
        return playerRemoveTransient(null, player, permission);
    }
    
    @Override
    public boolean playerRemoveTransient(String worldName, Player player, String permission) {
        return false;
    }
    
    @Override
    public String[] getGroups() {
        List<Group> groups = GroupManager.getGroups();
        if (groups == null || groups.isEmpty()) {
            return null;
        }
        String[] groupNames = new String[groups.size()];
        for (int i = 0; i < groups.size(); i++) {
            groupNames[i] = groups.get(i).getName();
        }
        return groupNames;
    }
    
    @Override
    public boolean hasSuperPermsCompat() {
        return true;
    }
    
    @Override
    public boolean hasGroupSupport() {
        return true;
    }
}

