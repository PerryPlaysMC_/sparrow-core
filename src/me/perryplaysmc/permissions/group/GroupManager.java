package me.perryplaysmc.permissions.group;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class GroupManager {
    
    static boolean load = false;
    private static List<Group> groups;
    private static List<String> perms = new ArrayList<>();
    
    public static List<Group> getGroups(){
        if(groups == null) {
            groups = new ArrayList<>();
        }
        return groups;
    }
    
    public static void addGroup(Group... groups) {
        for(Group group : groups) {
            if(getGroup(group.getName()) == null && !getGroups().contains(group)) {
                getGroups().add(group);
            }
        }
    }
    
    public static List<Group> getSorted() {
        int highest = 0;
        List<Group> groups = new ArrayList<>();
        List<Group> groups2 = new ArrayList<>();
        groups.addAll(getGroups());
        Iterator<Group> g = groups.iterator();
        while(g.hasNext()) {
            for (int i = 0; i < groups.size(); i++) {
                if(groups.get(i).getRank() < groups.get(highest).getRank()) highest = i;
            }
            groups2.add(g.next());
            g.remove();
        }
        return groups2;
    }
    
    @SuppressWarnings("all")
    public static void  reload() {
        Config perm = Core.getAPI().getPermissions();
        groups = new ArrayList<>();
        perm.reload();
        if(perm.isSet("Permissions.Groups")) {
            for(String group : perm.getSection("Permissions.Groups").getKeys(false)) {
                Group g = new Group(group, perm.getStringList("Permissions.Groups." + group + ".permissions"),
                        perm.getInt("Permissions.Groups." + group + ".rank"));
                g.updateInheritance();
                for(User u : CoreAPI.getOnlineUsers()) {
                    if(u.getPermissionData().getGroups().contains(g)) {
                        for(String per : g.getPermissions()) {
                            u.getPermissionData().addPermission(per);
                        }
                    }
                }
            }
        }
        if(CoreAPI.getAllUsers()!=null&&CoreAPI.getAllUsers().size()>0)
            for(OfflineUser u : CoreAPI.getAllUsers()) {
                if(perm.getSection("Permissions.Groups") != null) {
                    for(String group : perm.getSection("Permissions.Groups").getKeys(false)) {
                        Group g = new Group(group, perm.getStringList("Permissions.Groups." + group + ".permissions"),
                                perm.getInt("Permissions.Groups." + group + ".rank"));
                        g.updateInheritance();
                        if(u.getPermissionData().getGroups().contains(g)) {
                            for(String per : g.getPermissions()) {
                                u.getPermissionData().addPermission(per);
                            }
                        }
                    }
                }
            }
    }
    
    public static Group getGroup(String name) {
        for(Group g : getGroups()) {
            if(g.getName().equalsIgnoreCase(name)) {
                return g;
            }
        }
        return null;
    }
    
    public static Group getGroup(int rank) {
        for(Group g : getGroups()) {
            if(g.getRank() == rank) {
                return g;
            }
        }
        return null;
    }
    
    public static Group getGroup(User u, String a, int index) {
        for(Group g : getGroups()) {
            if(g.getUsers().contains(u.getUniqueId().toString())) {
                return u.getPermissionData().getGroups().get(index);
            }
        }
        return null;
    }
    
    public static Group getGroup(User u, int rank) {
        for(Group g : getGroups()) {
            if(g.getRank() == rank && g.getUsers().contains(u.getName())) {
                return g;
            }
        }
        return null;
    }


//    public static List<String> getPermissions() {
//        List<String> permss = new ArrayList<>();
//        for(Permission p : Bukkit.getPluginManager().getPermissions()) {
//            if(!permss.contains(p.getName()))permss.add(p.getName());
//        }
//
//        return permss;
//    }
//
    
    public static void loadPermissions() {
        String cmdPrefix=CoreAPI.getSettings().getString("settings.permissions-prefix.command");
        String featurePrefix=CoreAPI.getSettings().getString("settings.permissions-prefix.feature");
        List<String> permissions = Arrays.asList(
                cmdPrefix + ".calculate",
                cmdPrefix + ".ban",
                cmdPrefix + ".tempban",
                cmdPrefix + ".chat.clear",
                cmdPrefix + ".chat.clear.all",
                cmdPrefix + ".chat.toggle",
                cmdPrefix + ".chat.clear.self",
                cmdPrefix + ".chat.clear.other",
                cmdPrefix + ".chat.slow.toggle",
                cmdPrefix + ".chat.slow.settime",
                cmdPrefix + ".fly",
                cmdPrefix + ".fly.other",
                cmdPrefix + ".invsee",
                cmdPrefix + ".permission",
                cmdPrefix + ".permission.*",
                cmdPrefix + ".permission.reload",
                cmdPrefix + ".permission.group.create",
                cmdPrefix + ".permission.group.delete",
                cmdPrefix + ".permission.group.info",
                cmdPrefix + ".permission.group.rename",
                cmdPrefix + ".permission.group.set.prefix",
                cmdPrefix + ".permission.group.set.suffix",
                cmdPrefix + ".permission.group.default",
                cmdPrefix + ".permission.group.user.remove",
                cmdPrefix + ".permission.group.user.add",
                cmdPrefix + ".permission.group.inherit.add",
                cmdPrefix + ".permission.group.inherit.remove",
                cmdPrefix + ".permission.group.permission.add",
                cmdPrefix + ".permission.group.permission.remove",
                cmdPrefix + ".permission.user.info",
                cmdPrefix + ".permission.user.set.prefix",
                cmdPrefix + ".permission.user.set.suffix",
                cmdPrefix + ".permission.user.permission.add",
                cmdPrefix + ".permission.user.permission.remove",
                cmdPrefix + ".reload",
                cmdPrefix + ".reload.see",
                cmdPrefix + ".delhome",
                cmdPrefix + ".sethome",
                cmdPrefix + ".home",
                cmdPrefix + ".home.other",
                cmdPrefix + ".enchant",
                cmdPrefix + ".gamerule",
                cmdPrefix + ".kill.all",
                cmdPrefix + ".kill.entity",
                cmdPrefix + ".kill.player",
                cmdPrefix + ".kill.player.all",
                cmdPrefix + ".message",
                cmdPrefix + ".reply",
                cmdPrefix + ".nickname",
                cmdPrefix + ".teleport.self",
                cmdPrefix + ".teleport.other",
                cmdPrefix + ".teleport.other.xyz",
                cmdPrefix + ".teleport.self.xyz",
                cmdPrefix + ".world.create",
                cmdPrefix + ".world.delete",
                cmdPrefix + ".world.list",
                cmdPrefix + ".world.listplayers",
                cmdPrefix + ".world.teleport",
                featurePrefix + ".veinmine",
                featurePrefix + ".mobvoid",
                featurePrefix + ".silkspawner");
        Permission star = new Permission("*");
        for(String p : permissions) {
            Permission perm = new Permission(p);
            if(Bukkit.getPluginManager().getPermission(p)==null)
                Bukkit.getPluginManager().addPermission(perm);
            perm.addParent(star, true);
            
        }
        for(Permission p : Bukkit.getPluginManager().getPermissions()) {
            if(p.getName().equalsIgnoreCase("*"))continue;
            p.addParent(star, true);
        }
        if(Bukkit.getPluginManager().getPermission("*")==null)
            Bukkit.getPluginManager().addPermission(star);
    }
    
    
    public static void removeGroup(Group group) {
        getGroups().remove(group);
    }
}
