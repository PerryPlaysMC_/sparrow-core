package me.perryplaysmc.permissions.group;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.context.ImmutableContextSet;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.ColorUtil;
import org.bukkit.permissions.Permission;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("all")
public class Group {
    
    private Permission perms = null;
    private String name;
    private List<String> permissions, players;
    private Config cfg = CoreAPI.getPermissions();
    private int rank;
    public UUID uuid = UUID.randomUUID();
    
    public Group() {
        if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            if(CoreAPI.getPluginHelper().isEnabled("LuckPerms")) {
                LuckPermsApi api = LuckPerms.getApi();
                me.lucko.luckperms.api.Group g = null;
                if(api.getGroup(name)!=null)
                    g = api.getGroup(name);
                else g = api.getGroupManager().createAndLoadGroup(name).join();
                for(String s : permissions)
                    g.setPermission(api.buildNode(s).build());
                api.getGroupManager().saveGroup(g);
                
            }
            else if(CoreAPI.getPluginHelper().isEnabled("PermissionsEx")) {
                PermissionsEx ex = (PermissionsEx) PermissionsEx.getPlugin();
                PermissionGroup g = ex.getPermissionManager().getGroup(name);
                rank = g.getRank();
                for(PermissionUser u : g.getUsers()) {
                    players.add(u.getName());
                }
            }
        }
    }
    
    
    public Group(String name, List<String> permissions) {
        this.name = name;
        this.permissions = permissions;
        players = new ArrayList<>();
        cfg.set("Permissions.Groups." + name + ".rank", 0);
        GroupManager.addGroup(this);
        for(OfflineUser u : CoreAPI.getAllUsers()) {
            if(u != null && u.getPermissionData().getGroups() != null && u.getPermissionData().getGroups().size() > 0 && u.getPermissionData().getGroups().contains(this)) {
                if(!players.contains(u.getRealName())) {
                    players.add(u.getRealName());
                }
            }
        }
    }
    
    public Group(String name, List<String> permissions, int rank) {
        
        this.name = name;
        this.permissions = permissions;
        this.rank = rank;
        players = new ArrayList<>();
        cfg.set("Permissions.Groups." + name + ".rank", rank);
        GroupManager.addGroup(this);
        for(OfflineUser u : CoreAPI.getAllUsers()) {
            if(u != null && u.getPermissionData().getGroups() != null && u.getPermissionData().getGroups().size() > 0 && u.getPermissionData().getGroups().contains(this)) {
                if(!players.contains(u.getName())) {
                    players.add(u.getName());
                }
            }
        }
    }
    
    public int getRank() {
        if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            if(CoreAPI.getPluginHelper().isEnabled("LuckPerms")) {
                LuckPermsApi api = LuckPerms.getApi();
                me.lucko.luckperms.api.Group g = null;
                if(api.getGroup(name)!=null)
                    g = api.getGroup(name);
                else g = api.getGroupManager().createAndLoadGroup(name).join();
                return g.getWeight().getAsInt();
            }
            else if(CoreAPI.getPluginHelper().isEnabled("PermissionsEx")) {
                PermissionsEx ex = (PermissionsEx) PermissionsEx.getPlugin();
                PermissionGroup g = ex.getPermissionManager().getGroup(name);
                return g.getRank();
            }
            return -1;
        }
        return rank;
    }
    
    public void setRank(int rank) {
        if(CoreAPI.getPluginHelper().isEnabled("LuckPerms")) {
            LuckPermsApi api = LuckPerms.getApi();
            me.lucko.luckperms.api.Group g = api.getGroupManager().getGroup(name);
            ImmutableContextSet contextSet = api.getContextManager().getApplicableContext(g);
            Contexts contexts = api.getContextManager().getApplicableContexts(g);
            return;
        } if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            if(CoreAPI.getPluginHelper().isEnabled("LuckPerms")) {
                LuckPermsApi api = LuckPerms.getApi();
                me.lucko.luckperms.api.Group g = api.getGroup(name);
                ImmutableContextSet contextSet = api.getContextManager().getApplicableContext(g);
                Contexts contexts = api.getContextManager().getApplicableContexts(g);
                return;
            }
            else if(CoreAPI.getPluginHelper().isEnabled("PermissionsEx")) {
                PermissionsEx ex = (PermissionsEx) PermissionsEx.getPlugin();
                PermissionGroup g = ex.getPermissionManager().getGroup(name);
                g.setRank(rank);
                return;
            }
            return;
        }
        cfg.set("Permissions.Groups." + name + ".rank", rank);
        this.rank = rank;
    }
    
    public List<String> getUsers(){
        return players;
    }
    
    public String getPrefix() {
        if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            if(CoreAPI.getPluginHelper().isEnabled("LuckPerms")) {
                LuckPermsApi api = LuckPerms.getApi();
                me.lucko.luckperms.api.Group g = api.getGroup(name);
                ImmutableContextSet contextSet = api.getContextManager().getApplicableContext(g);
                Contexts contexts = api.getContextManager().getApplicableContexts(g);
               
                return g.getCachedData().getMetaData(contexts).getPrefix();
            }
            else if(CoreAPI.getPluginHelper().isEnabled("PermissionsEx")) {
                PermissionsEx ex = (PermissionsEx) PermissionsEx.getPlugin();
                PermissionGroup g = ex.getPermissionManager().getGroup(name);
                return g.getPrefix();
            }
            return "";
        }
        if(!cfg.isSet("Permissions.Groups." + getName() + ".prefix")) return "";
        return ColorUtil.translateChatColor(cfg.getString("Permissions.Groups." + getName() + ".prefix"));
    }
    
    public void setPrefix(String newPrefix) {
        if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            if(CoreAPI.getPluginHelper().isEnabled("LuckPerms")) {
                LuckPermsApi api = LuckPerms.getApi();
                me.lucko.luckperms.api.Group g = api.getGroup(name);
                ImmutableContextSet contextSet = api.getContextManager().getApplicableContext(g);
                Contexts contexts = api.getContextManager().getApplicableContexts(g);
                g.getCachedData().getMetaData(contexts).getMeta().put("prefix", newPrefix);
                return ;
            }
            else if(CoreAPI.getPluginHelper().isEnabled("PermissionsEx")) {
                PermissionsEx ex = (PermissionsEx) PermissionsEx.getPlugin();
                PermissionGroup g = ex.getPermissionManager().getGroup(name);
                g.setPrefix(newPrefix, "");
                return ;
            }
            return;
        }
        if(newPrefix.equalsIgnoreCase(uuid.toString())) {
            cfg.set("Permissions.Groups." + getName() + ".prefix", null);
            return;
        }
        cfg.set("Permissions.Groups." + getName() + ".prefix", ColorUtil.translateChatColor(newPrefix));
    }
    
    public String getSuffix() {
        if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            if(CoreAPI.getPluginHelper().isEnabled("LuckPerms")) {
                LuckPermsApi api = LuckPerms.getApi();
                me.lucko.luckperms.api.Group g = api.getGroup(name);
                ImmutableContextSet contextSet = api.getContextManager().getApplicableContext(g);
                Contexts contexts = api.getContextManager().getApplicableContexts(g);
                return g.getCachedData().getMetaData(contexts).getSuffix();
            }
            else if(CoreAPI.getPluginHelper().isEnabled("PermissionsEx")) {
                PermissionsEx ex = (PermissionsEx) PermissionsEx.getPlugin();
                PermissionGroup g = ex.getPermissionManager().getGroup(name);
                return g.getSuffix();
            }
            return "";
        }
        if(!cfg.isSet("Permissions.Groups." + getName() + ".suffix")) return "";
        return ColorUtil.translateChatColor(cfg.getString("Permissions.Groups." + getName() + ".suffix"));
    }
    
    public void setSuffix(String newSuffix) {
        if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            if(CoreAPI.getPluginHelper().isEnabled("LuckPerms")) {
                LuckPermsApi api = LuckPerms.getApi();
                me.lucko.luckperms.api.Group g = api.getGroup(name);
                ImmutableContextSet contextSet = api.getContextManager().getApplicableContext(g);
                Contexts contexts = api.getContextManager().getApplicableContexts(g);
                g.getCachedData().getMetaData(contexts).getMeta().put("suffix", newSuffix);
                return ;
            }
            else if(CoreAPI.getPluginHelper().isEnabled("PermissionsEx")) {
                PermissionsEx ex = (PermissionsEx) PermissionsEx.getPlugin();
                PermissionGroup g = ex.getPermissionManager().getGroup(name);
                g.setSuffix(newSuffix, "");
                return ;
            }
            return;
        }
        if(newSuffix.equalsIgnoreCase(uuid.toString())) {
            cfg.set("Permissions.Groups." + getName() + ".suffix", null);
            return;
        }
        cfg.set("Permissions.Groups." + getName() + ".suffix", ColorUtil.translateChatColor(newSuffix));
    }
    
    public boolean addUser(OfflineUser u) {
        if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            if(CoreAPI.getPluginHelper().isEnabled("LuckPerms")) {
                LuckPermsApi api = LuckPerms.getApi();
                me.lucko.luckperms.api.Group g = api.getGroup(name);
                ImmutableContextSet contextSet = api.getContextManager().getApplicableContext(g);
                Contexts contexts = api.getContextManager().getApplicableContexts(g);
                me.lucko.luckperms.api.User user = api.getUser(u.getUniqueId());
                if(user.inheritsGroup(g))return false;
                return true;
            }
            else if(CoreAPI.getPluginHelper().isEnabled("PermissionsEx")) {
                PermissionsEx ex = (PermissionsEx) PermissionsEx.getPlugin();
                PermissionGroup g = ex.getPermissionManager().getGroup(name);
                PermissionUser user = ex.getUser(u.getRealName());
                if(g.getUsers().contains(user))return false;
                user.addGroup(g);
                return true;
            }
            return false;
        }
        if(players.contains(u.getName()) && u.getPermissionData().getGroups().contains(this)) {
            return false;
        }
        players.add(u.getName());
        List<String> groups = cfg.getStringList("Permissions.Users." + u.getUniqueId() + ".groups");
        if(!groups.contains(getName())) {
            groups.add(getName());
        }else return false;
        cfg.set("Permissions.Users." + u.getUniqueId().toString() + ".groups", groups);
        u.getPermissionData().getGroups();
        if(u.getBase().isOnline()) {
            User u1 = CoreAPI.getUser(u.getName());
            u1.getPermissionData().getGroups();
        }
        return true;
    }
    
    public boolean removeUser(OfflineUser u) {
        if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            if(CoreAPI.getPluginHelper().isEnabled("LuckPerms")) {
                LuckPermsApi api = LuckPerms.getApi();
                me.lucko.luckperms.api.Group g = api.getGroup(name);
                ImmutableContextSet contextSet = api.getContextManager().getApplicableContext(g);
                Contexts contexts = api.getContextManager().getApplicableContexts(g);
                me.lucko.luckperms.api.User user = api.getUser(u.getUniqueId());
                if(!user.inheritsGroup(g))return false;
                return true;
            }
            else if(CoreAPI.getPluginHelper().isEnabled("PermissionsEx")) {
                PermissionsEx ex = (PermissionsEx) PermissionsEx.getPlugin();
                PermissionGroup g = ex.getPermissionManager().getGroup(name);
                PermissionUser user = ex.getUser(u.getRealName());
                if(!g.getUsers().contains(user))return false;
                user.removeGroup(g);
                return true;
            }
            return false;
        }
        if(!players.contains(u.getName()) && !u.getPermissionData().getGroups().contains(this)) {
            return false;
        }
        players.remove(u.getName());
        List<String> groups = cfg.getStringList("Permissions.Users." + u.getUniqueId() + ".groups");
        if(groups.contains(getName())) {
            groups.remove(getName());
        }else return false;
        cfg.set("Permissions.Users." + u.getUniqueId() + ".groups", groups);
        u.getPermissionData().getGroups();
        if(u.getBase().isOnline()) {
            User u1 = CoreAPI.getUser(u.getName());
            u1.getPermissionData().getGroups();
        }
        return true;
    }
    
    public void updateInheritance() {
        if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            return;
        }
        if(cfg.getStringList("Permissions.Groups." + name + ".inherits") != null){
            for(String groupName : cfg.getStringList("Permissions.Groups." + name + ".inherits")) {
                if(GroupManager.getGroup(groupName) != null) {
                    Group g = GroupManager.getGroup(groupName);
                    for(String perm : g.getPermissions()) {
                        for(User u : CoreAPI.getOnlineUsers()) {
                            if(u != null) {
                                if(u.getPermissionData().getGroups().contains(this)) {
                                    u.getPermissionData().addPermission(perm);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    public List<Group> getInherits() {
        List<Group> groups = new ArrayList<>();
        if(!CoreAPI.getSettings().getBoolean("Features.Permissions.isEnabled")) {
            return groups;
        }
        if(cfg.getStringList("Permissions.Groups." + name + ".inherits") != null){
            for(String groupName : cfg.getStringList("Permissions.Groups." + name + ".inherits")) {
                if(GroupManager.getGroup(groupName) != null) {
                    Group g = GroupManager.getGroup(groupName);
                    if(g != null && !groups.contains(g)) {
                        groups.add(g);
                    }
                }
            }
        }
        return groups;
    }
    
    
    public void addInheritance(Group group) {
        List<String> a = cfg.getStringList("Permissions.Groups." + name + ".inherits");
        if(!a.contains(group.getName())){
            a.add(group.getName());
        }
        cfg.set("Permissions.Groups." + name + ".inherits", a);
        updateInheritance();
    }
    
    public void removeInheritance(Group group) {
        List<String> a = cfg.getStringList("Permissions.Groups." + name + ".inherits");
        if(a.contains(group.getName())){
            a.remove(group.getName());
        }
        cfg.set("Permissions.Groups." + name + ".inherits", a);
        for(String perm : group.getPermissions()) {
            for(User u : CoreAPI.getOnlineUsers()) {
                if(u != null) {
                    if(u.getPermissionData().getGroups().contains(this)) {
                        u.getPermissionData().removePermission(perm);
                    }
                }
            }
        }
        updateInheritance();
    }
    
    public void addPerm(String perm) {
        List<String> perms = cfg.getStringList("Permissions.Groups." + name + ".permissions");
        if(!perms.contains(perm)) {
            perms.add(perm);
        }
        cfg.set("Permissions.Groups." + name + ".permissions", perms);
        this.permissions = perms;
        for(OfflineUser u : CoreAPI.getAllUsers()) {
            if(u != null) {
                if(u.getPermissionData().getGroups().contains(this)) {
                    for(String p : getPermissions()) {
                        u.getPermissionData().addPermission(p);
                    }
                }
            }
        }
    }
    
    public void removePerm(String perm) {
        List<String> perms = cfg.getStringList("Permissions.Groups." + name + ".permissions");
        if(perms.contains(perm)) {
            perms.remove(perm);
        }
        cfg.set("Permissions.Groups." + name + ".permissions", perms);
        for(OfflineUser u : CoreAPI.getAllUsers()) {
            if(u != null) {
                if(u.getPermissionData().getGroups().contains(this)) {
                    for(String p : getPermissions()) {
                        u.getPermissionData().removePermission(p);
                    }
                }
            }
        }
    }
    
    public String toString() {
        return "Group=[{Name=" + getName() + "}, {Prefix=" + getPrefix() + "}]";
    }
    
    public String getName() {
        return name;
    }
    
    public List<String> getPermissions() {
        return cfg.getStringList("Permissions.Groups." + name + ".permissions");
    }
    
    public boolean isDefault() {
        if(!cfg.getConfig().isSet("Permissions.Groups."+name+".default")) return false;
        else return cfg.getBoolean("Permissions.Groups."+name+".default");
    }
    
    public void setDefault(boolean b) {
        cfg.set("Permissions.Groups."+name+".default", b);
    }
    
    public void delete() {
        cfg.set("Permissions.Groups."+name, null);
        GroupManager.removeGroup(this);
    }
    
    public Group rename(String newName) {
        Group g =  new Group(newName, getPermissions());
        g.setPrefix(getPrefix());
        g.setDefault(isDefault());
        g.setRank(getRank());
        g.setSuffix(getSuffix());
    
        List<String> a = cfg.getStringList("Permissions.Groups." + name + ".inherits");
        cfg.set("Permissions.Groups." + newName + ".inherits", a);
        delete();
        return g;
    }
    
    public boolean isPermissionSet(String... permissions) {
        for(String perm : permissions) {
            if(getPermissions().contains(perm.toLowerCase())) return true;
        }
        return false;
    }
    public boolean hasPermission(String... permissions) {
        for(String s : permissions)
            if(getPermissions().contains(s.toLowerCase())) return true;
        
        return false;
    }
}