package me.perryplaysmc.user;

import com.mojang.authlib.GameProfile;
import me.perryplaysmc.user.data.AntiCheatDetector;
import me.perryplaysmc.utils.inventory.CustomInventory;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public interface User extends CommandSource, OfflineUser {


    Player getBase();
    
    void kick(String reason);

    //Inventory Start
    
    void openInventory(Inventory inv);
    
    void openInventory(CustomInventory inv);
    
    void updateInventory();
    
    ItemStack getItemInHand();
    
    //Inventory End
    //GameProfile Start
    
    void setProfile(GameProfile pro);

    GameProfile getProfile();
    
    Class<?> getEntityHumanClass();
    //GameProfile End
    
    void setName(String name);
    
    //WorldEditing Start
    
    void toggleVeinMine();
    
    boolean isVeinMine();
    
    Location getLocation();
    
    World getWorld();
    
    void teleport(Location loc);
    
    void teleport(Entity entity);
    
    //WorldEditing End
    
    AntiCheatDetector getAntiCheatDetector();
    
    UUID getOriginalUniqueId();
    
    String getOriginalName();
    
    Player getOriginalBase();
    
    void kill();
    
    void setHealth(double d);
    
    double getHealth();
    
    void setMaxHealth(double d);
    
    double getMaxHealth();
    
    boolean isCooldown(String key);
    
    void startCooldown(String key, double time);
    
    double getCooldownRemains(String key);
    
    boolean isCaptured();
    
    void setCaptured(boolean bool);

}
