package me.perryplaysmc.user;

import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.instances.CommandSpy;
import me.perryplaysmc.user.instances.NickNameable;
import me.perryplaysmc.user.instances.Vanishable;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.user.instances.Replier;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;

public interface CommandSource extends Vanishable, NickNameable, CommandSpy, Replier {
    
    
    
    CommandSender getBase();
    
    String getName();
    
    String getRealName();
    
    default boolean isPlayer() {
        return true;
    }
    
    boolean hasPermission(String... permissions);
    
    boolean hasPermission(Permission... permissions);
    
    void sendTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut);
    
    void sendTitle(String title);
    
    void sendAction(String message);
    
    void sendMessage(Object... messages);
    
    void sendMessage(Message... messages);
    
    void sendMessageFormat(String location, Object... objects);
    
    void sendMessageFormat(Config cfg, String location, Object... objects);
    
    default User getUser() {
        return (User) this;
    }
    
}
