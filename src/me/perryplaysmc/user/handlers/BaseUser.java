package me.perryplaysmc.user.handlers;

import com.mojang.authlib.GameProfile;
import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.abstraction.AbstractionHandler;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.User;
import me.perryplaysmc.user.data.AntiCheatDetector;
import me.perryplaysmc.utils.string.NumberUtil;
import me.perryplaysmc.utils.string.TimeUtil;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import me.perryplaysmc.chat.Message;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.permissions.PermissionUserData;
import me.perryplaysmc.user.CommandSource;
import me.perryplaysmc.utils.helper.ban.BanData;
import me.perryplaysmc.utils.inventory.CustomInventory;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.permissions.Permission;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.EulerAngle;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.UUID;

public class BaseUser implements User {

    private Config cfg;
    private Player base, originalBase;
    private BankAccount account;
    private PermissionUserData data;
    private RankUtils rankUtils;
    private ShopHandler handler;
    private HomeData homeData;
    private ChatSettings chatSettings;
    private AntiCheatDetector detector;
    private HashMap<String, Double> cooldown;
    private CommandSource replier;

    public BaseUser(Player base) {
        this.base = base;
        this.originalBase = base;
        File f = new File(Config.defaultDirectory, "userData");
        this.cfg = new Config(f, base.getUniqueId().toString() + ".yml");
        account = new BankAccount(this);
        data = new PermissionUserData(this);
        handler = new ShopHandler(this);
        homeData = new HomeData(this);
        chatSettings = new ChatSettings(this);
        detector = new AntiCheatDetector(this);
        rankUtils = new RankUtils(this);
        cfg.set("settings.Name", base.getName());
        cfg.setIfNotSet("settings.Original-Name", base.getName());
        setVanished(isVanished());
        cooldown = new HashMap<>();
        if(!CoreAPI.getBalanceTops().contains(getUniqueId()))
            CoreAPI.getBalanceTops().add(getUniqueId());
        (new BukkitRunnable() {
            @Override
            public void run() {
                BanData b = CoreAPI.getBan(getUniqueId());
                if(b != null) {
                    if(b.getExpire() == -1) {
                        return;
                    }
                    if(TimeUtil.formatDateDiff((b.getExpire())).equalsIgnoreCase("Now")) {
                        CoreAPI.pardonPlayer(getUniqueId());
                    }
                }
            }
        }).runTaskTimer(Core.getAPI(), 0, 20);
        DecimalFormat format = new DecimalFormat("###.##");
        (new BukkitRunnable() {
            @Override
            public void run() {
                Double percent = NumberUtil.getPercent(base.getExp(), 1f).doubleValue();
                sendAction("[c]Exp: %[pc]" + format.format(percent) + " [c]Exp Remains: %[pc]" + format.format((100 + -percent)));
            }
        }).runTaskTimer(Core.getAPI(), 0, 10);
    }

    @Override
    public Player getBase() {
        return base;
    }

    @Override
    public void kick(String reason) {
        base.kickPlayer(StringUtils.translate(reason));
    }

    @Override
    public void openInventory(Inventory inv) {
        base.openInventory(inv);
    }

    @Override
    public void openInventory(CustomInventory inv) {
        openInventory(inv.getInventory());
    }

    @Override
    public void updateInventory() {
        base.updateInventory();
    }

    @Override
    public ItemStack getItemInHand() {
        return base.getInventory().getItemInMainHand();
    }

    @Override
    public void setProfile(GameProfile pro) {
        try {
            cfg.setIfNotSet("settings.holder.previous.name", base.getName());
            cfg.setIfNotSet("settings.holder.previous.uuid", base.getUniqueId().toString());
            Method getHandle = base.getClass().getMethod("getHandle");
            Object entityPlayer = getHandle.invoke(base);
            Class<?> entityHuman = entityPlayer.getClass().getSuperclass();
            Field bH = null;
            for(Field f : entityHuman.getDeclaredFields()) {
                if(f.get(entityPlayer) instanceof GameProfile) {
                    bH = f;
                    break;
                }
            }
            if(bH == null) {
                for(Field f : entityHuman.getFields()) {
                    if(f.get(entityPlayer) instanceof GameProfile) {
                        bH = f;
                        break;
                    }
                }
            }
            if(bH!=null) {
            bH.setAccessible(true);
            bH.set(entityPlayer, pro);
            }
            this.base = Bukkit.getPlayer(pro.getName());
            cfg.set("settings.holder.current.name", pro.getName());
            cfg.set("settings.holder.current.uuid", pro.getId().toString());
        }catch(Exception e) {
        }
    }

    @Override
    public GameProfile getProfile() {
        try {
            Method getHandle = base.getClass().getMethod("getHandle");
            Object entityPlayer = getHandle.invoke(base);
            Class<?> entityHuman = entityPlayer.getClass().getSuperclass();
            Method getProfile =  entityHuman.getMethod("getProfile");
            Object profile = getProfile.invoke(entityPlayer);
            return (GameProfile)profile;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Class<?> getEntityHumanClass() {
        Method getHandle = null;
        Object entityPlayer = null;
        try {
            getHandle = base.getClass().getMethod("getHandle");
            entityPlayer = getHandle.invoke(base);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Class<?> entityHuman = entityPlayer.getClass().getSuperclass();
        return entityHuman;
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public void toggleVeinMine() {
        cfg.set("settings.VeinMine", !isVeinMine());
    }

    @Override
    public boolean isVeinMine() {
        return cfg.getBoolean("settings.VeinMine");
    }

    @Override
    public Location getLocation() {
        return base.getLocation();
    }

    @Override
    public World getWorld() {
        return getLocation().getWorld();
    }

    @Override
    public void teleport(Location loc) {
        base.teleport(loc);
    }

    @Override
    public void teleport(Entity entity) {
        base.teleport(entity);
    }

    @Override
    public AntiCheatDetector getAntiCheatDetector() {
        return detector;
    }

    @Override
    public UUID getOriginalUniqueId() {
        return originalBase.getUniqueId();
    }

    @Override
    public String getOriginalName() {
        return cfg.getString("settings.Original-Name");
    }

    @Override
    public Player getOriginalBase() {
        return originalBase;
    }

    @Override
    public void kill() {
        setHealth(0);
    }

    @Override
    public void setHealth(double d) {
        base.setHealth(d);
    }

    @Override
    public double getHealth() {
        return base.getHealth();
    }

    @Override
    public void setMaxHealth(double d) {
        base.setMaxHealth(d);
    }

    @Override
    public double getMaxHealth() {
        return base.getMaxHealth();
    }

    @Override
    public void startCooldown(String key, double time) {
        cooldown.put(key, time);
        (new BukkitRunnable() {
            @Override
            public void run() {
                if(cooldown.get(key) > 0.1) {
                    cooldown.put(key, cooldown.get(key) +- 0.1);
                }else {
                    cooldown.remove(key);
                    cancel();
                }
            }
        }).runTaskTimer(Core.getAPI(), 0, 2);
    }

    @Override
    public boolean isCooldown(String key) {
        return cooldown.containsKey(key);
    }

    @Override
    public double getCooldownRemains(String key) {
        return isCooldown(key) ? cooldown.get(key) : 0;
    }

    @Override
    public boolean isCaptured() {
        return cfg.getBoolean("settings.isCaptured");
    }

    @Override
    public void setCaptured(boolean bool) {
        cfg.set("settings.isCaptured", bool);
    }

    @Override
    public boolean isVanished() {
        return cfg.getBoolean("settings.isVanished");
    }

    @Override
    public void setVanished(boolean isVanished) {
        cfg.set("settings.isVanished", isVanished);
        if(isVanished) {
            for(Player p : Bukkit.getOnlinePlayers()) {
                if(Version.isVersionHigherThan(true, Versions.v1_13))
                    p.hidePlayer(Core.getAPI(), getOriginalBase());
                else
                    p.hidePlayer(getOriginalBase());
            }
            if(originalBase!=null)
                originalBase.setPlayerListName(ColorUtil.translateChatColor( "&7Vanished &c- " + getRealName()));
        }else {
            for(Player p : Bukkit.getOnlinePlayers()) {
                if(Version.isVersionHigherThan(true, Versions.v1_13))
                    p.showPlayer(Core.getAPI(), getOriginalBase());
                else
                    p.showPlayer(getOriginalBase());
            }
            if(originalBase!=null)
                originalBase.
                        setPlayerListName(
                                getRealName());
        }
    }

    @Override
    public Player getAsPlayer() {
        return base;
    }

    @Override
    public Config getConfig() {
        return cfg;
    }

    @Override
    public ChatSettings getChatSettings() {
        return chatSettings;
    }

    @Override
    public boolean isOnline() {
        return base.isOnline();
    }

    @Override
    public UUID getUniqueId() {
        return base.getUniqueId();
    }

    @Override
    public BankAccount getAccount() {
        return account;
    }

    @Override
    public HomeData getHomeData() {
        return homeData;
    }

    @Override
    public ShopHandler getShopHandler() {
        return handler;
    }

    @Override
    public RankUtils getRankUtils() {
        return rankUtils;
    }

    @Override
    public PermissionUserData getPermissionData() {
        return data;
    }

    @Override
    public PlayerInventory getInventory() {
        return base.getInventory();
    }

    @Override
    public String getName() {
        return !isVanished() ? base.getName() : "Anonymous";
    }

    @Override
    public String getRealName() {
        return base.getName();
    }

    @Override
    public boolean hasPermission(String... permissions) {
        return getPermissionData().hasPermission(permissions);
    }

    @Override
    public boolean hasPermission(Permission... permissions) {
        for(Permission perm : permissions) {
            if(hasPermission(perm.getName()))return true;
        }
        return false;
    }

    @Override
    public void sendTitle(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
        base.sendTitle(StringUtils.translate((title)), StringUtils.translate((subtitle)), fadeIn, stay, fadeOut);
    }

    @Override
    public void sendTitle(String title) {
        sendTitle(title, "", 20, 60, 20);
    }

    @Override
    public void sendAction(String message) {
        base.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(StringUtils.translate(message)));
    }


    @Override
    public void sendMessage(Object... messages) {
        for(Object msg : messages) {
            base.sendMessage(StringUtils.translate(StringUtils.translate(msg.toString())));
        }
    }

    @Override
    public void sendMessage(Message... messages) {
        for(Message msg : messages) {
            msg.send(this);
        }
    }

    @Override
    public void sendMessageFormat(String location, Object... objects) {
        sendMessage(CoreAPI.format(location, objects));
    }

    @Override
    public void sendMessageFormat(Config cfg, String location, Object... objects) {
        sendMessage(CoreAPI.format(cfg, location, objects));
    }

    @Override
    public boolean isNicked() {
        return cfg.isSet("settings.NickName.name") && nicked();
    }

    @Override
    public void setNick(String nick) {
        cfg.set("settings.NickName.name", nick);
    }

    @Override
    public void toggleNick() {
        cfg.set("settings.NickName.isEnabled", !nicked());
    }

    @Override
    public boolean nicked() {
        return cfg.getBoolean("settings.NickName.isEnabled");
    }

    @Override
    public String getNickName() {
        return isNicked() ? cfg.getString("settings.NickName.name") : getName();
    }

    @Override
    public boolean isCommandSpyEnabled() {
        return cfg.getBoolean("settings.CommandSpy");
    }

    @Override
    public void toggleCommandSpy() {
        setCommandSpy(!isCommandSpyEnabled());
    }

    @Override
    public void setCommandSpy(boolean enabled) {
        cfg.set("settings.CommandSpy", enabled);
    }

    @Override
    public CommandSource getReplier() {
        return replier;
    }

    @Override
    public void setReplier(CommandSource s) {
        replier = s;
    }

    @Override
    public boolean isOp() {
        return base.isOp();
    }

    @Override
    public void setOp(boolean b) {
        base.setOp(b);
    }
}
