package me.perryplaysmc.user.handlers;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.prison.ranks.RankData;
import me.perryplaysmc.prison.ranks.RankDataHandler;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.string.NumberUtil;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/8/19-2023
 * Package: me.perryplaysmc.user.handlers
 * Path: me.perryplaysmc.user.handlers.RankUtils
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class RankUtils {
    
    
    private Config cfg;
    
    public RankUtils(OfflineUser u) {
        cfg = u.getConfig();
        User u1 = CoreAPI.getUser(u.getUniqueId());
        if(u.isOnline()&&u1!=null&&u1.getConfig()!=null)
            cfg = u1.getConfig();
        if(RankDataHandler.getFirstRank()!=null)
            cfg.setIfNotSet("Rank", RankDataHandler.getFirstRank().getRank());
        cfg.setIfNotSet("Prestiege", 0);
    }
    
    public RankUtils addPrestiege() {
        cfg.set("Rank", (cfg.getInt("Prestiege")+1));
        return this;
    }
    
    public String getPrestiege() {
        return NumberUtil.convertToRoman(cfg.getInt("Prestiege"));
    }
    
    public RankUtils setRank(RankData rank) {
        cfg.set("Rank", RankDataHandler.getFirstRank().getRank());
        return this;
    }
    
    public RankData getRank() {
        return RankDataHandler.getRank(cfg.getString("Rank"));
    }
    
    
}
