package me.perryplaysmc.user.handlers.shops;

import org.bukkit.Location;

public class Shop
{
  private ShopType type;
  private Location loc;
  
  public Shop(ShopType type, Location loc)
  {
    this.type = type;
    this.loc = loc;
  }
  
  public Location getLoc()
  {
    return loc;
  }
  
  public ShopType getType() {
    return type;
  }
}
