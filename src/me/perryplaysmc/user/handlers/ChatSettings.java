package me.perryplaysmc.user.handlers;

import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.OfflineUser;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 9/10/19-2023
 * Package: me.perryplaysmc.user.handlers
 * Path: me.perryplaysmc.user.handlers.ChatSettings
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class ChatSettings {
    
    Config cfg;
    
    public ChatSettings(OfflineUser u) {
        cfg = u.getConfig();
        cfg.setIfNotSet("settings.Chat.isEnabled", true);
        cfg.setIfNotSet("settings.Chat.allowLinks", true);
        cfg.setIfNotSet("settings.Chat.format", "default");
    }
    
    public boolean isEnabled() {
        return cfg.getBoolean("settings.Chat.isEnabled");
    }
    
    public boolean allowLinks() {
        return cfg.getBoolean("settings.Chat.allowLinks");
    }
    
    public void toggle() {
        cfg.set("settings.Chat.isEnabled", !isEnabled());
    }
    
    public void toggleLinks() {
        cfg.set("settings.Chat.allowLinks", !allowLinks());
    }
    
    public String getChatFormat() {
        return cfg.getString("settings.Chat.format");
    }
    
    public void setChatFormat(String format) {
        cfg.set("settings.Chat.format", format);
    }
    
    
}
