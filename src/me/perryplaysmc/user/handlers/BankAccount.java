package me.perryplaysmc.user.handlers;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.user.OfflineUser;
import me.perryplaysmc.utils.string.NumberUtil;

@SuppressWarnings("all")
public class BankAccount {
    
    private Config cfg;
    
    public BankAccount(OfflineUser u) {
        cfg = u.getConfig();
        cfg.setIfNotSet("Account.balance", 0);
    }
    
    
    public String getBalance() {
        return NumberUtil.format(getBalanceRaw());
    }
    
    public boolean create() {
        if(cfg.isSet("Account.balace"))
            return false;
        return true;
    }
    
    
    public Double getBalanceRaw() {
        return NumberUtil.getMoney(String.valueOf(cfg.get("Account.balance")).replaceAll(",", ""));
    }
    
    public boolean hasEnough(double amt) {
        return getBalanceRaw() >= amt || (!exceedsMin(false, amt));
    }
    
    public void removeBalance(double amt) {
        if(!exceedsMin(false, amt))
            setBalance(getBalanceRaw()+-amt);
    }
    
    public boolean exceedsMax(boolean isSet, double amt) {
        String maxBalance = CoreAPI.getSettings().getString("EconomySettings.maxBalance");
        if(maxBalance.equalsIgnoreCase("off") || maxBalance.equalsIgnoreCase("false")) return false;
        if(NumberUtil.isDouble(maxBalance)) {
            double d = NumberUtil.getMoney(maxBalance);
            if(isSet)
                if(amt>d)
                    return true;
                else
                    return false;
            if((getBalanceRaw()+amt)>d) {
                return true;
            }else return false;
        }
        CoreAPI.error("Settings.yml >> 'EconomySettings.maxBalance' is not a valid number or is not [off, false] " + maxBalance);
        return false;
    }
    public boolean exceedsMin(boolean isSet, double amt) {
        String minBalance = CoreAPI.getSettings().getString("EconomySettings.minBalance");
        if(minBalance.equalsIgnoreCase("off") || minBalance.equalsIgnoreCase("false")) return false;
        if(NumberUtil.isDouble(minBalance)) {
            double d = NumberUtil.getMoney(minBalance);
            if(isSet)
                if(amt<d)
                    return true;
                else
                    return false;
            if((getBalanceRaw()-amt)<d) {
                return true;
            }else return false;
        }
        CoreAPI.error("Settings.yml >> 'EconomySettings.minBalance' is not a valid number or is not [off, false] " + minBalance);
        return false;
    }
    
    public void addBalance(double amt) {
        if(!exceedsMax(false, amt))
            setBalance(getBalanceRaw()+amt);
    }
    
    public void setBalance(double amt) {
        cfg.set("Account.balance", NumberUtil.getMoney(""+amt));
    }
    
    
}
