package me.perryplaysmc.user.instances;

public interface CommandSpy {
    
    
    boolean isCommandSpyEnabled();
    
    void toggleCommandSpy();
    
    void setCommandSpy(boolean enabled);
    
    
}
