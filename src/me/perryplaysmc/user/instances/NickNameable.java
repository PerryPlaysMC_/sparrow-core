package me.perryplaysmc.user.instances;

public interface NickNameable {
    
    boolean isNicked();
    
    void setNick(String nick);
    
    void toggleNick();
    
    boolean nicked();
    
    String getNickName();
    
}
