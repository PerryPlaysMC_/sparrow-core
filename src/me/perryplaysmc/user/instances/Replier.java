package me.perryplaysmc.user.instances;

import me.perryplaysmc.user.CommandSource;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/1/19-2023
 **/
public interface Replier {
    
    CommandSource getReplier();
    
    void setReplier(CommandSource s);
    
}
