package me.perryplaysmc.user.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import org.bukkit.craftbukkit.libs.org.apache.commons.codec.DecoderException;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.libs.org.apache.commons.codec.binary.Hex;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;



@SuppressWarnings("all")
public class PlayerData {
    
    public static UUID getUUID(Player player) throws Exception {
        
        OfflinePlayer op = Bukkit.getOfflinePlayer(player.getName());
        if (op.hasPlayedBefore()) {
            return op.getUniqueId();
        } else {
            return getUUIDFromString(player.getName());
        }
    }
    
    public static UUID getUUID(String playerName) throws Exception {
        OfflinePlayer op = Bukkit.getOfflinePlayer(playerName);
        if (op.hasPlayedBefore() || op.isOnline()) {
            return op.getUniqueId();
        }else {
            return getUUIDFromString(playerName);
        }
    }
    
    public static UUID getUUIDFromString(String playerName) throws Exception {
        byte[] data = null;
        int length = getWebData("https://api.mojang.com/users/profiles/minecraft/" + playerName, "id").length();
        data = Hex.decodeHex(getWebData("https://api.mojang.com/users/profiles/minecraft/" + playerName, "id")
                .substring(1, length +-1).toCharArray());
        return new UUID(ByteBuffer.wrap(data, 0, 8).getLong(), ByteBuffer.wrap(data, 8, 8).getLong());
    }
    
    public static List<String> getLocation(Player player) throws Exception {
        return getLocation(player.getAddress());
    }
    
    public static List<String> getLocation(InetSocketAddress inetSocketAddress) throws Exception {
        List<String> localIPs = Arrays.asList("192.168.1.1", "127.0.0.1");
        List<String> dataToGet = Arrays.asList("country", "regionName", "city", "zip");
        List<String> data;
        if(localIPs.contains(inetSocketAddress.getAddress().toString().replace("/", ""))) {
            data = getWebData("http://ip-api.com/json/" + Bukkit.getServer().getIp().replace("/", ""), dataToGet);
        }
        else {
            data = getWebData("http://ip-api.com/json/" + inetSocketAddress.getAddress().toString().replace("/", ""), dataToGet);
        }
        data.set(0, "Country: " + data.get(0));
        data.set(1, "State: " + data.get(1));
        data.set(2, "City: " + data.get(2));
        data.set(3, "Zip Code: " + data.get(3));
        return data;
    }
    
    
    public static String getName(Player player) throws Exception {
        if(player.isOnline()) {
            return getName(player.getUniqueId());
        }else {
            return getName(getUUID(player));
        }
    }
    public static UUID stringToUUID(String uuid) throws Exception{
        byte[] data;
        if(Version.isVersionHigherThan(true, Versions.v1_13)) {
            data = Hex.decodeHex(uuid.replace("-", "").toCharArray());
        }
        else data = org.apache.commons.codec.binary.Hex.decodeHex(uuid.replace("-", "").toCharArray());
        return new UUID(ByteBuffer.wrap(data, 0, 8).getLong(), ByteBuffer.wrap(data, 8, 8).getLong());
    }
    public static String getName(UUID uuid) throws Exception {
        return getWebData("https://sessionserver.mojang.com/session/minecraft/profile/"
                          + stringToUUID(uuid.toString()).toString().replace("-", ""), "name").replace("\"", "");
    }
    public static String getSkin(UUID uuid) throws Exception {
        return getWebData("https://sessionserver.mojang.com/session/minecraft/profile/" + stringToUUID(uuid.toString()).toString().replace("-", ""), "textures");
    }
    
    
    
    public static String getWebData(String URL, String JSONDataToGet) throws Exception{
        java.net.URL url = null;
        JsonObject rootobj = null;
        url = new URL(URL);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();
        
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        rootobj = root.getAsJsonObject();
        
        return rootobj.get(JSONDataToGet).toString();//.getAsString();
    }
    
    static List<String> getWebData(String URL, List<String> JSONDataToGet) throws Exception {
        URL url = new URL(URL);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();
        
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject rootobj = root.getAsJsonObject();
        
        List<String> DataToReturn = new ArrayList<>();
        
        for(String JSONData : JSONDataToGet){
            DataToReturn.add(rootobj.get(JSONData).getAsString());
        }
        return DataToReturn;
    }
    
}
