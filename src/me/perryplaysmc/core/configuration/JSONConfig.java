package me.perryplaysmc.core.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.perryplaysmc.core.Core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/8/19-2023
 * Package: me.perryplaysmc.core.configuration
 * Path: me.perryplaysmc.core.configuration.JSONConfig
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class JSONConfig {
    
    
    private File f, dir;
    private String name;
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private Map<String, Object> map = new HashMap<>();
    
    public JSONConfig(String dir, String name) {
        this.name = name.endsWith(".json") ? name : name+".json";
        f = new File(dir, name);
        this.dir = f.getParentFile();
        
        try {
            if (!f.exists()) Core.getAPI().saveResource(f.getName(), false);
        }catch (Exception e) {
            reload();
        }
        
    }
    
    public String getString(String path) {
        return (String) map.get(path);
    }
    
    
    public JSONConfig set(String path, Object value) {
        map.put(path, value);
        if(value == null) {
            map.remove(path);
        }
        save();
        return this;
    }
    
    
    public JSONConfig reload() {
        try {
            map = gson.fromJson(new FileReader(f), new HashMap<String, Object>().getClass());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return this;
    }
    
    
    public JSONConfig save() {
        final String json = gson.toJson(map); // Remember pretty printing? This is needed here.
        f.delete(); // won't throw an exception, don't worry.
        try {
            Files.write(f.toPath(), json.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }
    
}
