package me.perryplaysmc.core.configuration;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.core.logging.LogType;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConfigManager {
    
    private static Set<Config> cfgs;
    private static Set<Config> all;
    
    
    
    
    public static Set<Config> getConfigs() {
        if (cfgs == null) {
            cfgs = new HashSet<>();
        }
        return cfgs;
    }
    
    public static List<File> getFiles(File dir) {
        List<File> files = new ArrayList<>();
        File[] fls = dir.listFiles();
        if(fls == null) return files;
        for(File f : fls) {
            if(f.isDirectory()) {
                files.addAll(getFiles(f));
                continue;
            }
            if(!files.contains(f))
                files.add(f);
        }
        return files;
    }
    public static void deleteFiles(File dir) {
        if(dir.exists()) {
            List<File> files = getFiles(dir);
            for (File f : files) {
                if(f.exists()) {
                    if(f.isDirectory()) {
                        deleteFiles(f);
                        continue;
                    }
                    f.delete();
                }
            }
            dir.delete();
        }
    }
    
    
    
    public static void loadFile(String filePath, String fileName) {
        loadFile(filePath, "", fileName);
    }
    
    public static void loadFile(String filePath, String copy, String fileName) {
        File f = new File(filePath, fileName);
        if(!f.exists()) {
            try {
                InputStream io = Core.getAPI().getResource(copy);
                if(io != null) {
                    copyToFile(io, f);
                } else
                    f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static File getDirectory(File dir, String fileName) {
        if(!dir.exists()||!dir.isDirectory()||dir.listFiles()==null||dir.listFiles().length==0)return null;
        for(File f : dir.listFiles()) {
            if(f.getName().equalsIgnoreCase(fileName) && f.isDirectory()) {
                return f;
            }
        }
        return null;
    }
    
    public static boolean containsFile(File dir, String fileName) {
        if(!dir.exists()||!dir.isDirectory()||dir.listFiles()==null||dir.listFiles().length==0)return false;
        for(File f : dir.listFiles()) {
            if(f.getName().equalsIgnoreCase(fileName)) {
                return true;
            }
        }
        return false;
    }
    
    public static void addConfig(Config cfg) {
        if ((cfg != null) && (!cfg.getFile().getName().contains("null")) && !getConfigs().contains(cfg)) {
            getConfigs().add(cfg);
        }
    }
    
    public static void reloadAll() {
        for (Config cfg : getConfigs()) {
            cfg.reload();
        }
    }
    
    public static void deleteAll() {
        for (Config cfg : getConfigs()) {
            cfg.deleteConfig();
        }
    }
    
    public static void removeConfig(Config cfg) {
        getConfigs().remove(cfg);
    }
    
    public static Config getConfig(String name) {
        for (Config cfg : getConfigs()) {
            if ((cfg.getName().equalsIgnoreCase(name)) || (cfg.getLocalName().equalsIgnoreCase(name))) {
                return cfg;
            }
        }
        return null;
    }
    
    public static Config getConfigMain(String name) {
        for (Config cfg : Core.getAPI().getConfigs()) {
            if ((cfg.getName().equalsIgnoreCase(name)) || (cfg.getLocalName().equalsIgnoreCase(name))) {
                return cfg;
            }
        }
        return null;
    }
    
    public static boolean contentEquals(File f1, File f2) {
        
        boolean x;
        try {
            if(Version.isVersionHigherThan(true, Versions.v1_13)) {
                x=org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils.contentEquals(f1, f2);
            }
            else
                x=org.apache.commons.io.FileUtils.contentEquals(f1, f2);
        } catch (IOException e) {
            x = false;
        }
        return x;
    }
    
    
    public static boolean resetConfig(InputStream f1, File f2) {
        File dir = new File(Config.defaultDirectory, "Holder");
        dir.mkdirs();
        File ff1 = new File(dir, f2.getName());
        copyToFile(f1, ff1);
        boolean x = ff1.length() > f2.length();
        deleteFiles(dir);
        return x;
    }
    
    public static void copyToFile(InputStream io, File f) {
        try {
            if(Version.isVersionHigherThan(true, Versions.v1_13))
                org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils.copyToFile(io, f);
            else
                org.apache.commons.io.FileUtils.copyToFile(io, f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void copyInputStreamToFile(InputStream io, File f) {
        try {
            if(Version.isVersionHigherThan(true, Versions.v1_13))
                org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils.copyInputStreamToFile(io, f);
            else
                org.apache.commons.io.FileUtils.copyInputStreamToFile(io, f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void copyToFile(File f1, File f2) {
        try {
            if(Version.isVersionHigherThan(true, Versions.v1_13))
                org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils.copyFile(f1, f2);
            else
                org.apache.commons.io.FileUtils.copyFile(f1, f2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static boolean copyFolderFiles(String worldName, String dest) { return copyFolderFiles(true, worldName, dest); }
    
    
    public static boolean copyFolder(String worldName, String dest) {
        if (new File("." + File.separator + dest).exists()) {
            CoreAPI.warning( dest + " already exists, Deleting!");
            new File("." + File.separator + dest).delete();
        }
        try {
            if(Version.isVersionHigherThan(true, Versions.v1_13))
                org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils.copyDirectory(new File("." + File.separator + worldName), new File("." + File.separator + dest));
            else
                org.apache.commons.io.FileUtils.copyDirectory(new File("." + File.separator + worldName), new File("." + File.separator + dest));
            CoreAPI.info( "File " + worldName + " has been copied!");
            return true;
        } catch (java.io.IOException e) {
            CoreAPI.warning( "Error duplicating world!"); }
        return false;
    }
    
    public static boolean copyFolderFiles(boolean print, String worldName, String dest) {
        try
        {
            File folder1 = new File("." + File.separator + worldName);
            File folder2 = new File("." + File.separator + dest);
            if (!folder1.exists()) folder1.mkdir();
            if (!folder2.exists()) folder2.mkdir();
            File[] fs1 = folder1.listFiles();
            File[] fs2 = folder2.listFiles();
            if ((fs2 == null) || (!folder2.exists()) || (!folder2.isDirectory()) || (folder2.length() == 0) || (fs2.length == 0)) {
                copyFolder(worldName, dest);
                if (print)
                    CoreAPI.info( "Loading files");
            }
            if ((fs1 == null) || (folder2.listFiles() == null) || (fs1.length != fs2.length)) {
                copyFolder(worldName, dest);
                if (print)
                    CoreAPI.error( "Folder files " + (
                            (folder1.listFiles() == null) || (folder2.listFiles() == null) ? 1 : false)
                                   + "/" + (fs1.length != fs2.length ? 1 : false));
            }
            if(fs1!=null&&fs2!=null&&fs1.length == fs2.length)
                for (int i = 0; i < fs1.length; i++) {
                    File f1 = fs1[i];
                    File f2 = fs2[i];
                    if (f1.length() == 0L) {
                        copyToFile(f2, f1);
                        if (print)
                            CoreAPI.info( "Copied file (" + f2
                                    .getParentFile().getName() + ")" + f2.getName() + " To (" + f1.getParentFile().getName() + ")" + f1.getName());
                    }
                    if(!contentEquals(f2, f1)) {
                        copyToFile(f1, f2);
                        if (print)
                            CoreAPI.info( "Files are not equal, Copied file (" + f1
                                    .getParentFile().getName() + ")" + f1.getName() + " To (" + f2.getParentFile().getName() + ")" + f2.getName());
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
            if (print)
                CoreAPI.error( "Error");
            return false;
        }
        return true;
    }
}
