package me.perryplaysmc.core.configuration;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.abstraction.versionUtil.Version;
import me.perryplaysmc.core.abstraction.versionUtil.Versions;
import me.perryplaysmc.utils.string.StringUtils;
import me.perryplaysmc.world.LocationUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.io.InputStream;
import java.util.*;

@SuppressWarnings("all")
public class Config implements Configuration {
    
    
    public final static File defaultDirectory = new File("plugins", "Core");
    private File f, dir;
    private String name, copyOf, local;
    private boolean isCopyOf = true;
    private YamlConfiguration cfg;
    private Core core;
    
    
    public Config(Core core, File dir, String copyOf, String name) {
        this.core = core;
        this.name = name;
        this.copyOf = copyOf;
        if(copyOf == "") isCopyOf = false;
        if(dir == null) {
            dir = defaultDirectory;
        }
        if(!dir.isDirectory()) dir.mkdirs();
        
        f = new File(dir, name);
        loadFile();
        ConfigManager.addConfig(this);
        if(f.getName().endsWith(".yml")) {
            reload();
            setLocal(f.getName().replace(".yml",""));
        }
    }
    
    public Config(String dir, String name) {
        this(Core.getAPI(), new File(dir), "", name);
    }
    
    public Config(File dir, String fileToCopy, String name) {
        this(Core.getAPI(), dir, fileToCopy, name);
    }
    
    public Config(File dir, String name) {
        this(Core.getAPI(), dir, "", name);
    }
    
    public Config(String name) {
        this(Core.getAPI(), defaultDirectory, "", name);
    }
    
    public String getString(String path) {
        return cfg.getString(path);
    }
    
    public String getString(String path, String def) {
        return cfg.isSet(path) ? cfg.getString(path) : def;
    }
    
    @Override
    public String getCurrentPath() {
        return "";
    }
    
    public boolean getBoolean(String path)
    {
        return cfg.getBoolean(path);
    }
    
    public Object get(String path) {
        if (cfg.get(path) instanceof String) {
            if(getLocation(path)!=null) {
                return getLocation(getString(path));
            }
            return getString(path);
        }
        return cfg.get(path);
    }
    
    public Configuration createSection(String path) {
        cfg.createSection(path);
        return new ConfigurationImpl(this, cfg.getConfigurationSection(path));
    }
    
    public int getInt(String path)
    {
        return cfg.getInt(path);
    }
    
    public double getDouble(String path)
    {
        return cfg.getDouble(path);
    }
    
    @Override
    public List<String> getKeys(boolean keys) {
        try {
            return new ArrayList<>(cfg.getKeys(keys));
        }catch (Exception e) {
            return new ArrayList<>();
        }
    }
    
    @Override
    public float getFloat(String path) {
        return Float.valueOf(String.valueOf(cfg.get(path)));
    }
    
    public List<String> getStringList(String path) {
        if(cfg.get(path)==null)return new ArrayList<>();
        if(cfg.get(path) instanceof List)
            return cfg.isSet(path) ? cfg.getStringList(path) : new ArrayList();
        else
            return Arrays.asList(cfg.getString(path));
    }
    
    public Location getLocation(String path) {
        return (isSet(path) && getString(path).contains("/") && getString(path).split("/").length == 6 ? LocationUtil.stringToLocation(getString(path)) : null);
    }
    
    public String[] getStringArray(String path) {
        List<String> s = getStringList(path);
        return s.toArray(new String[s.size()]);
    }
    
    public List<Object> getObjectList(String path) {
        return (List<Object>) cfg.getList(path);
    }
    
    public Config set(String path, Object var) {
        if ((var instanceof Location)) {
            cfg.set(path, LocationUtil.locationToString((Location)var));
            save();
            return this;
        }
        if(var instanceof ItemStack) {
            setItemStack(path, (ItemStack) var);
            save();
            return this;
        }
        if(var instanceof Enum) {
            set(path, ((Enum) var).name());
            save();
            return this;
        }
        cfg.set(path, var);
        save();
        return this;
    }
    
    public Config setIfNotSet(String path, Object var) {
        if(isSet(path))return this;
        if ((var instanceof Location)) {
            cfg.set(path, LocationUtil.locationToString((Location)var));
            save();
            return this;
        }
        if(var instanceof ItemStack) {
            setItemStack(path, (ItemStack) var);
            save();
            return this;
        }
        cfg.set(path, var);
        save();
        return this;
    }
    
    public List<?> getList(String path) {
        return cfg.getList(path);
    }
    
    private void saveItem(Configuration section, ItemStack item) {
        if (item == null) return;
        section.set("type", item.getType().name());
        section.set("amount", item.getAmount());
        if (item.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)item.getItemMeta();
            if (im.getOwner() != null)
                section.set("Owner", im.getOwner());
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
            return;
        }
        if (item.hasItemMeta()) {
            ItemMeta im = item.getItemMeta();
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
        }
        section.set("data", item.getDurability());
    }
    
    private ItemStack loadItem(Configuration section) {
        if(section == null || !isSet(section.getCurrentPath())) {
            return new ItemStack(Material.AIR);
        }
        if(Material.valueOf(section.getString("type")).name().contains("AIR")) {
            cfg.set(section.getCurrentPath(), null);
            return new ItemStack(Material.AIR);
        }
        ItemStack i = new ItemStack(Material.valueOf(section.getString("type")), section.getInt("amount"));
        if (i.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)i.getItemMeta();
            if (section.getString("Owner") != null)
                im.setOwner(section.getString("Owner"));
            if (section.getStringList("enchants") != null) {
                for (String a : section.getStringList("enchants"))
                    im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
            }
            if (section.getStringList("lore") != null) {
                im.setLore(section.getStringList("lore"));
            }
            im.setUnbreakable(section.getBoolean("unbreakable"));
            i.setDurability((short)section.getInt("data"));
            i.setItemMeta(im);
            return i;
        }
        ItemMeta im = i.getItemMeta();
        if (section.getStringList("enchants") != null) {
            for (String a : section.getStringList("enchants"))
                im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
        }
        if (section.getStringList("lore") != null) {
            im.setLore(section.getStringList("lore"));
        }
        im.setUnbreakable(section.getBoolean("unbreakable"));
        i.setDurability((short)section.getInt("data"));
        i.setItemMeta(im);
        return i;
    }
    
    public Config setItemStack(String path, ItemStack item) {
        saveItem(createSection(path), item);
        save();
        return this;
    }
    
    @Override
    public boolean isSet(String path) {
        return cfg.isSet(path);
    }
    
    public ItemStack getItemStack(String path) {
        return loadItem(getSection(path));
    }
    
    @Override
    public long getLong(String path) {
        return cfg.getLong(path);
    }
    
    @Override
    public byte getByte(String path) {
        return (Byte) cfg.get(path);
    }
    
    
    @Override
    public boolean contains(String object) {
        return cfg.contains(object);
    }
    
    @Override
    public Configuration getSection(String path) {
        if(!cfg.isSet(path))
            cfg.createSection(path);
        return new ConfigurationImpl(this, cfg.getConfigurationSection(path));
    }

    @Override
    public void remove() {
        cfg.set(cfg.getCurrentPath(), null);
    }

    public void reload() {
        cfg = YamlConfiguration.loadConfiguration(f);
    }
    
    public void save() {
        try {
            cfg.save(f);
            if (dir == new File(defaultDirectory, "userData")) {
                ConfigManager.copyFolderFiles(false, defaultDirectory.getPath()+"/userData", defaultDirectory.getPath()+"/userDataBackup");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void deleteConfig() {
        String name = getLocalName();
        ConfigManager.removeConfig(this);
        getFile().delete();
        this.name = "";
        core.debug(name + " Has been deleted");
    }
    
    
    private void loadFile() {
        if(!f.exists()) {
            try {
                if(!isCopyOf) {
                    if(core != null) {
                        if(core.getResource(f.getName()) != null)
                            ConfigManager.copyInputStreamToFile(core.getResource(f.getName()), f);
                        else
                            f.createNewFile();
                    } else {
                        f.createNewFile();
                    }
                } else {
                    if(core != null) {
                        if(core.getResource(copyOf) != null)
                            ConfigManager.copyInputStreamToFile(core.getResource(copyOf), f);
                        else
                            f.createNewFile();
                        
                    } else {
                        f.createNewFile();
                    }
                }
                CoreAPI.debug("Created '" + name + "'");
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
//        else {
//            InputStream i = core.getResource(f.getName());
//            if(i!=null&&ConfigManager.resetConfig(i, f)) {
//                resetConfig();
//                CoreAPI.debug("Reset config " + f.getName());
//            }
//        }
    }
    
    public Config setHeader(String... header) {
        int max  = " ________________________________________________________________________".toCharArray().length;
        String h = " ________________________________________________________________________\n" +
                   "/                                                                        \\" + "\n" +
                   "|                                                                        |"  + "\n" +
                   "|                                                                        |";
        for(String a : header) {
            h+= "\n" + "|" + StringUtils.centerText(max, a, "|");
        }
        h+="\n"+
           "|                                                                        |\n" +
           "|                                                                        |\n" +
           "\\________________________________________________________________________/";
        cfg.options().header(h);
        cfg.options().copyDefaults(true);
        save();
        return this;
    }
    
    public Core getCore() {
        return core;
    }
    
    public Config resetConfig() {
        try {
            f.delete();
            loadFile();
            reload();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }
    
    public String getName() {
        return name;
    }
    
    public String getLocalName() {
        if(local == null || local.isEmpty()) local = name.replace(".yml", "");
        return local;
    }
    
    public void setLocal(String newLocal) {
        this.local = newLocal;
    }
    
    public File getDirectory()
    {
        return dir;
    }
    
    public File getFile()
    {
        return f;
    }
    
    public YamlConfiguration getConfig()
    {
        return cfg;
    }
    
    
}
