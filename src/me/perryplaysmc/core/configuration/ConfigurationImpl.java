package me.perryplaysmc.core.configuration;

import me.perryplaysmc.world.LocationUtil;
import org.apache.commons.lang.Validate;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/8/19-2023
 * Package: me.perryplaysmc.core.configuration
 * Path: me.perryplaysmc.core.configuration.ConfigurationImpl
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class ConfigurationImpl implements Configuration {
    
    private ConfigurationSection cfg;
    private Config config;
    private String prefix;
    
    public ConfigurationImpl(Config config, ConfigurationSection cfg) {
        this.cfg = cfg;
        this.config = config;
    }
    
    @Override
    public String getCurrentPath() {
        return cfg.getCurrentPath();
    }
    
    @Override
    public boolean getBoolean(String path) {
        return cfg.getBoolean(path);
    }
    
    @Override
    public String getString(String path) {
        return cfg.getString(path);
    }
    
    @Override
    public double getDouble(String path) {
        return cfg.getDouble(path);
    }
    
    @Override
    public float getFloat(String path) {
        return (Float) cfg.get(path);
    }
    
    @Override
    public int getInt(String path) {
        return cfg.getInt(path);
    }
    
    @Override
    public long getLong(String path) {
        return cfg.getLong(path);
    }
    
    @Override
    public byte getByte(String path) {
        return Byte.valueOf(cfg.getString(path));
    }
    
    @Override
    public Object get(String path) {
        return cfg.get(path);
    }
    
    @Override
    public Location getLocation(String path) {
        return (isSet(path) && getString(path).contains("/") && getString(path).split("/").length == 6 ? LocationUtil.stringToLocation(getString(path)) : null);
    }
    
    @Override
    public ItemStack getItemStack(String path) {
        return loadItem(new ConfigurationImpl(config, cfg.getConfigurationSection(path)));
    }
    
    @Override
    public String[] getStringArray(String path) {
        List<String> s = getStringList(path);
        return s.toArray(new String[s.size()]);
    }
    
    @Override
    public List<String> getStringList(String path) {
        return cfg.getStringList(path);
    }
    
    @Override
    public List<?> getList(String path) {
        return cfg.getList(path);
    }
    
    @Override
    public boolean isSet(String path) {
        return cfg.isSet(path);
    }
    
    @Override
    public boolean contains(String object) {
        return cfg.contains(object);
    }
    
    @Override
    public List<String> getKeys(boolean keys) {
        try {
            return new ArrayList<>(cfg.getKeys(keys));
        }catch (java.lang.StackOverflowError | Exception e) {
            return new ArrayList<>();
        }
    }
    
    public Configuration set(String path, Object var) {
        if ((var instanceof Location)) {
            cfg.set(path, LocationUtil.locationToString((Location)var));
            save();
            return this;
        }
        if(var instanceof ItemStack) {
            setItemStack(path, (ItemStack) var);
            save();
            return this;
        }
        if(var instanceof Enum) {
            set(path, ((Enum) var).name());
            save();
            return this;
        }
        cfg.set(path, var);
        save();
        return this;
    }
    
    @Override
    public Configuration getSection(String path) {
        return new ConfigurationImpl(config, cfg.getConfigurationSection(path));
    }

    @Override
    public void remove() {
        config.set(getCurrentPath(), "");
    }

    public Configuration createSection(String path) {
        cfg.createSection(path);
        return new ConfigurationImpl(config, cfg.getConfigurationSection(path));
    }
    
    public Configuration setItemStack(String path, ItemStack item) {
        saveItem(createSection(path), item);
        save();
        return this;
    }
    
    
    private ItemStack loadItem(Configuration section) {
        if(section == null || !isSet(section.getCurrentPath())) {
            return new ItemStack(Material.AIR);
        }
        if(Material.valueOf(section.getString("type")).name().contains("AIR")) {
            cfg.set(section.getCurrentPath(), null);
            return new ItemStack(Material.AIR);
        }
        ItemStack i = new ItemStack(Material.valueOf(section.getString("type")), section.getInt("amount"));
        if (i.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)i.getItemMeta();
            if (section.getString("Owner") != null)
                im.setOwner(section.getString("Owner"));
            if (section.getStringList("enchants") != null) {
                for (String a : section.getStringList("enchants"))
                    im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
            }
            if (section.getStringList("lore") != null) {
                im.setLore(section.getStringList("lore"));
            }
            im.setUnbreakable(section.getBoolean("unbreakable"));
            i.setDurability((short)section.getInt("data"));
            i.setItemMeta(im);
            return i;
        }
        ItemMeta im = i.getItemMeta();
        if (section.getStringList("enchants") != null) {
            for (String a : section.getStringList("enchants"))
                im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
        }
        if (section.getStringList("lore") != null) {
            im.setLore(section.getStringList("lore"));
        }
        im.setUnbreakable(section.getBoolean("unbreakable"));
        i.setDurability((short)section.getInt("data"));
        i.setItemMeta(im);
        return i;
    }
    
    
    private void saveItem(Configuration section, ItemStack item) {
        if (item == null) return;
        section.set("type", item.getType().name());
        section.set("amount", item.getAmount());
        if (item.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)item.getItemMeta();
            if (im.getOwner() != null)
                section.set("Owner", im.getOwner());
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
            return;
        }
        if (item.hasItemMeta()) {
            ItemMeta im = item.getItemMeta();
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
        }
        section.set("data", item.getDurability());
    }
    
    public void save() {
        config.save();
    }
    
}
