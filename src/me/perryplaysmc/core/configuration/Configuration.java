package me.perryplaysmc.core.configuration;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/8/19-2023
 * Package: me.perryplaysmc.core.configuration
 * Path: me.perryplaysmc.core.configuration.Configuration
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public interface Configuration {
    
    
    String getCurrentPath();
    
    boolean getBoolean(String path);
    
    String getString(String path);
    
    double getDouble(String path);
    
    float getFloat(String path);
    
    int getInt(String path);
    
    long getLong(String path);
    
    byte getByte(String path);
    
    Object get(String path);
    
    Location getLocation(String path);
    
    ItemStack getItemStack(String path);
    
    String[] getStringArray(String path);
    
    List<String> getStringList(String path);
    
    List<?> getList(String path);
    
    boolean isSet(String path);
    
    boolean contains(String object);
    
    List<String> getKeys(boolean keys);
    
    Configuration set(String path, Object value);
    
    Configuration getSection(String path);

    void remove();
}
