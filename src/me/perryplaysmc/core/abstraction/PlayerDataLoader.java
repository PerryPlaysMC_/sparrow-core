package me.perryplaysmc.core.abstraction;

import com.sun.istack.internal.NotNull;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public interface PlayerDataLoader {


    public Player loadPlayer(@NotNull final OfflinePlayer offline);

}
