package me.perryplaysmc.core.abstraction.versionUtil;

public enum Versions {
    v1_8(180), v1_8_R1(181), v1_8_R2(182), v1_8_R3(183),
    v1_9(190), v1_9_R1(191), v1_9_R2(192),
    v1_10(1100), v1_10_R1(1101),
    v1_11(1110), v1_11_R1(1111),
    v1_12(1120), v1_12_R1(1121),
    v1_13(1130), v1_13_R1(1131), v1_13_R2(1132),
    v1_14(1140), v1_14_R1(1141), NULL(-1);
    
    private int ver;
    Versions(int ver) {
        this.ver = ver;
    }
    
    public int getVersion() {
        return ver;
    }
    
}