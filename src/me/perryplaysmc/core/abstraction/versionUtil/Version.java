package me.perryplaysmc.core.abstraction.versionUtil;

import org.bukkit.Bukkit;

import java.util.Arrays;

public class Version {
    
    public static Versions getVersionExact() {
        String pack =  Bukkit.getServer().getClass().getPackage().getName();
        String version = pack.substring(pack.lastIndexOf('.')+1).replaceFirst("v", "");
        Versions ret = Versions.NULL;
        switch(version) {
            case "1_8_R1": {
                ret = Versions.v1_8_R1;
                break;
            }
            case "1_8_R2": {
                ret = Versions.v1_8_R2;
                break;
            }
            case "1_8_R3": {
                ret = Versions.v1_8_R3;
                break;
            }
            case "1_9_R1": {
                ret = Versions.v1_9_R1;
                break;
            }
            case "1_9_R2": {
                ret = Versions.v1_9_R2;
                break;
            }
            case "1_10_R1": {
                ret = Versions.v1_10_R1;
                break;
            }
            case "1_11_R1": {
                ret = Versions.v1_11_R1;
                break;
            }
            case "1_12_R1": {
                ret = Versions.v1_12_R1;
                break;
            }
            case "1_13_R1": {
                ret = Versions.v1_13_R1;
                break;
            }
            case "1_13_R2": {
                ret = Versions.v1_13_R2;
                break;
            }
            case "1_14_R1": {
                ret = Versions.v1_14_R1;
                break;
            }
        }
        return ret;
    }
    public static Versions getVersion() {
        String pack =  Bukkit.getServer().getClass().getPackage().getName();
        String version = pack.substring(pack.lastIndexOf('.')+1).replaceFirst("v", "");
        Versions ret = Versions.NULL;
        switch(version) {
            case "1_8_R1": {
                ret = Versions.v1_8;
                break;
            }
            case "1_8_R2": {
                ret = Versions.v1_8;
                break;
            }
            case "1_8_R3": {
                ret = Versions.v1_8;
                break;
            }
            case "1_9_R1": {
                ret = Versions.v1_9;
                break;
            }
            case "1_9_R2": {
                ret = Versions.v1_9;
                break;
            }
            case "1_10_R1": {
                ret = Versions.v1_10;
                break;
            }
            case "1_11_R1": {
                ret = Versions.v1_11;
                break;
            }
            case "1_12_R1": {
                ret = Versions.v1_12;
                break;
            }
            case "1_13_R1": {
                ret = Versions.v1_13;
                break;
            }
            case "1_13_R2": {
                ret = Versions.v1_13;
                break;
            }
            case "1_14_R1": {
                ret = Versions.v1_14;
                break;
            }
        }
        return ret;
    }
    
    public static boolean isVersion(Versions... versions) {
        for(Versions v : versions)
            if(getVersion() == v || getVersionExact() == v)
                return true;
        return false;
    }
    
    public static boolean isVersionHigherThan(boolean useExact, Versions ver) {
        if(useExact) {
            if(getVersionExact().getVersion() == ver.getVersion())return true;
            int max = ver.getVersion();
            Versions[] vers = Arrays.copyOfRange(Versions.values(), ver.ordinal(), Versions.values().length);
            if(isVersion(vers))return true;
            return getVersionExact() == ver;
        }else {
            if(getVersion().getVersion()==ver.getVersion())return true;
            int max = ver.getVersion();
            for (Versions v : Versions.values()) {
                if(v.getVersion()>=max) {
                    return true;
                }
            }
            return getVersion() == ver;
        }
    }
    
    
    
}
