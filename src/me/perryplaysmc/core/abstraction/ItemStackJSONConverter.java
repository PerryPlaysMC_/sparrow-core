package me.perryplaysmc.core.abstraction;

import org.bukkit.inventory.ItemStack;

public interface ItemStackJSONConverter {


    String convertItemStack(ItemStack item);


}
