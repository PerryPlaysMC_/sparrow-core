package me.perryplaysmc.core.abstraction.handlers.packet.PacketUtil.v1_13;

import me.perryplaysmc.core.abstraction.PacketUtil;
import net.minecraft.server.v1_13_R1.EntityPlayer;
import net.minecraft.server.v1_13_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_13_R1.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import org.bukkit.craftbukkit.v1_13_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.List;

@SuppressWarnings("all")
public class PacketUtil_v1_13_R1 implements PacketUtil {
    
    
    @Override
    public void PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo_Action action, List<Player> sendTo, Player... playersToUpdate) {
        EnumPlayerInfoAction a = EnumPlayerInfoAction.valueOf(action.name());
    
        EntityPlayer[] players = new EntityPlayer[playersToUpdate.length];
        
        for(int i = 0; i < players.length; i++) {
            CraftPlayer p = (CraftPlayer)playersToUpdate[i];
            players[i] =  p.getHandle();
        }
        
        PacketPlayOutPlayerInfo info = new PacketPlayOutPlayerInfo(a, players);
        
        for(Player op : sendTo) {
            CraftPlayer p = (CraftPlayer)op;
            p.getHandle().playerConnection.sendPacket(info);
        }
    }
}
