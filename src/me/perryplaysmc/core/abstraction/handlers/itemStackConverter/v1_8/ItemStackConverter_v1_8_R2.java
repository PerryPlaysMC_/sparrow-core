package me.perryplaysmc.core.abstraction.handlers.itemStackConverter.v1_8;

import me.perryplaysmc.core.abstraction.ItemStackJSONConverter;
import net.minecraft.server.v1_8_R2.NBTTagCompound;
import org.bukkit.craftbukkit.v1_8_R2.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

public class ItemStackConverter_v1_8_R2 implements ItemStackJSONConverter {

    @Override
    public String convertItemStack(ItemStack item) {
        net.minecraft.server.v1_8_R2.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = new NBTTagCompound();
        compound = nmsItemStack.save(compound);
        return compound.toString();
    }
}
