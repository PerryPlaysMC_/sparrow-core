package me.perryplaysmc.core.abstraction.handlers.itemStackConverter.v1_12;

import me.perryplaysmc.core.abstraction.ItemStackJSONConverter;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

public class ItemStackConverter_v1_12_R1 implements ItemStackJSONConverter {

    @Override
    public String convertItemStack(ItemStack item) {
        net.minecraft.server.v1_12_R1.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = new NBTTagCompound();
        compound = nmsItemStack.save(compound);
        return compound.toString();
    }
}
