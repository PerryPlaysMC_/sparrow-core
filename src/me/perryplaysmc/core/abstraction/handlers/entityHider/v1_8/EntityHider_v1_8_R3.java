package me.perryplaysmc.core.abstraction.handlers.entityHider.v1_8;

import me.perryplaysmc.core.abstraction.EntityHider;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntity;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/5/19-2023
 * Package: me.perryplaysmc.core.abstraction.handlers.entityHider.v1_8
 * Path: me.perryplaysmc.core.abstraction.handlers.entityHider.v1_8.EntityHider_v1_8_R1
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class EntityHider_v1_8_R3 implements EntityHider {
    
    @Override
    public void hideEntity(Player p, Entity entity) {
        PacketPlayOutEntityDestroy ppoed = new PacketPlayOutEntityDestroy(entity.getEntityId());
        ((CraftPlayer)p).getHandle().playerConnection.sendPacket(ppoed);
    }
    
    @Override
    public void showEntity(Player p, Entity entity) {
        PacketPlayOutEntity ppoed = new PacketPlayOutEntity(entity.getEntityId());
        ((CraftPlayer)p).getHandle().playerConnection.sendPacket(ppoed);
    }
}
