package me.perryplaysmc.core.abstraction.handlers.entityHider.v1_11;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.abstraction.EntityHider;
import net.minecraft.server.v1_11_R1.*;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/5/19-2023
 * Package: me.perryplaysmc.core.abstraction.handlers.entityHider.v1_8
 * Path: me.perryplaysmc.core.abstraction.handlers.entityHider.v1_8.EntityHider_v1_8_R1
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class EntityHider_v1_11_R1 implements EntityHider {
    
    @Override
    public void hideEntity(Player p, Entity entity) {
        (new BukkitRunnable() {
            @Override
            public void run() {
                PacketPlayOutEntityDestroy ppoed = new PacketPlayOutEntityDestroy(entity.getEntityId());
                ((CraftPlayer)p).getHandle().playerConnection.sendPacket(ppoed);
            }
        }).runTaskLater(Core.getAPI(), 50);
    }
    
    @Override
    public void showEntity(Player p, Entity entity) {
        (new BukkitRunnable() {
            @Override
            public void run() {
                PacketPlayOutEntity ppoed = new PacketPlayOutEntity(entity.getEntityId());
                ((CraftPlayer)p).getHandle().playerConnection.sendPacket(ppoed);
            }
        }).runTaskLater(Core.getAPI(), 50);
    }
}
