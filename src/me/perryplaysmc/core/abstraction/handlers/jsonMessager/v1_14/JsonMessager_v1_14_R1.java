package me.perryplaysmc.core.abstraction.handlers.jsonMessager.v1_14;

import net.minecraft.server.v1_14_R1.ChatMessageType;
import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import net.minecraft.server.v1_14_R1.PacketPlayOutChat;
import me.perryplaysmc.core.abstraction.JsonMessager;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JsonMessager_v1_14_R1 implements JsonMessager {

    @Override
    public void sendMessage(Player player, String json) {
        IChatBaseComponent c = IChatBaseComponent.ChatSerializer.a(json);
        PacketPlayOutChat p = new PacketPlayOutChat(c, ChatMessageType.CHAT);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
    }
}
