package me.perryplaysmc.core.abstraction.handlers.inventoryCreator.v1_8;

import me.perryplaysmc.core.abstraction.InventoryCreate;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftInventoryPlayer;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.PlayerInventory;

public class Inventory_v1_8_R3 implements InventoryCreate {
    @Override
    public PlayerInventory createInventory(HumanEntity base, String name) {
        return new CraftInventoryPlayer(new net.minecraft.server.v1_8_R3.PlayerInventory(null));
    }
}
