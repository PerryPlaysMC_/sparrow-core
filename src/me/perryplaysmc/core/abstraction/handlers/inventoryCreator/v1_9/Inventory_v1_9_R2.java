package me.perryplaysmc.core.abstraction.handlers.inventoryCreator.v1_9;

import me.perryplaysmc.core.abstraction.InventoryCreate;
import org.bukkit.craftbukkit.v1_9_R2.inventory.CraftInventoryPlayer;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.PlayerInventory;

public class Inventory_v1_9_R2 implements InventoryCreate {
    @Override
    public PlayerInventory createInventory(HumanEntity base, String name) {
        return new CraftInventoryPlayer(new net.minecraft.server.v1_9_R2.PlayerInventory(null));
    }
}
