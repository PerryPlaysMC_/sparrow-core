package me.perryplaysmc.core.abstraction.handlers.inventoryConverter.v1_13;

import me.perryplaysmc.core.abstraction.InventoryConverter;
import me.perryplaysmc.utils.inventory.CustomInventory;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftInventory;
import org.bukkit.inventory.Inventory;

public class InventoryConverter_v1_13_R2 implements InventoryConverter {
    @Override
    public CustomInventory convertInventory(Inventory inv) {
        CraftInventory inv2 = (CraftInventory) inv;
        return new CustomInventory(inv2.getHolder(), inv2.getSize(), inv2.getName());
    }
}
