package me.perryplaysmc.utils.inventory.shops;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.core.configuration.Config;
import me.perryplaysmc.core.configuration.Configuration;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import me.perryplaysmc.utils.string.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GUIShop
{
    private Inventory inv;
    private Config cfg;
    private ItemStack display;
    private String displayName;
    private String name;
    private GUIShop next;

    public GUIShop(String name)
    {
        cfg = new Config(new File(Config.defaultDirectory, "shops"), name + ".yml");
        System.out.println("Created");
        if (!cfg.isSet("Settings.Size")) {
            CoreAPI.error("Settings.Size must be set in config");
            return;
        }
        if (!cfg.isSet("Settings.Display")) {
            CoreAPI.error("Settings.Display must be set in config");
            return;
        }
        if (((!cfg.isSet("Settings.isNext")) || (!cfg.getBoolean("Settings.isNext"))) &&
                (!cfg.isSet("Settings.Item"))) {
            CoreAPI.error("Settings.Size must be set in config");
            return;
        }

        if ((cfg.isSet("NextPage.Display")) && (cfg.isSet("NextPage.Name"))) {
            Config cfg2 = new Config("plugins/Miniverse/Inventories", cfg.getString("NextPage.Name") + ".yml");
            if (!cfg2.isSet("Settings.Size")) cfg2.set("Settings.Size", cfg.getInt("Settings.Size"));
            if (!cfg2.isSet("Settings.Display")) cfg2.set("Settings.Display", cfg.getString("NextPage.Display"));
            cfg2.set("Settings.isNext", true);
            cfg2.set("Settings.Previous", getName());
            next = new GUIShop(cfg.getString("NextPage.Name"));
        }
        this.name = name;
        inv = Bukkit.createInventory(null, cfg.getInt("Settings.Size"), StringUtils.translate(cfg.getString("Settings.Display")));
        displayName = StringUtils.translate(cfg.getString("Settings.Display"));
        if (getInventory().getSize() >= 9) {
            Material[] glass = { Material.PURPLE_STAINED_GLASS_PANE, Material.LIME_STAINED_GLASS_PANE };
            for (int i = 1; i < 9; i++) {
                setItem(getInventory().getSize() + -i, ItemBuilder
                        .createItem(new ItemStack(glass[new java.util.Random().nextInt(glass.length)])).setName(" ").buildItem());
            }
            String end = "previous page";
            setItem(getInventory().getSize() + -9, ItemBuilder.createWool(ItemBuilder.ColorType.RED)
                    .setName("[c]&lBack").setAmount(1).setLore("[c]Return to " + end).buildItem());
            if (!isNext()) {
                setItem(getInventory().getSize() + -9, ItemBuilder.createWool(ItemBuilder.ColorType.RED)
                        .setName("[c]&lBack").setAmount(1).setLore("[c]Return to Main page").buildItem());
            }
        }
        if (hasNext())
            setItem(getInventory().getSize() + -1, ItemBuilder.createWool(ItemBuilder.ColorType.LIME)
                    .setName("[c]&lBack").setAmount(1).setLore("[c]Return to Main page").buildItem());
            setItem(getInventory().getSize() + -1, ItemBuilder.createWool(ItemBuilder.ColorType.LIME)
                    .setName("[c]&lNext").setAmount(1).setLore("[c]Go to the next page").buildItem());
        try {
            if (cfg.isSet("Settings.Item")) {
                display = ItemBuilder.createItem(cfg.getString("Settings.Item"))
                .setName(getDisplayName()).buildItem();
                if (cfg.isSet("Settings.ItemLore")) {
                    display = ItemBuilder.createItem(cfg.getString("Settings.Item"))
                            .setName(getDisplayName()).setLore(cfg.getStringArray("Settings.ItemLore")).buildItem();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        GUIShopHandler.addShop(this);
    }

    public boolean hasNext() {
        return next != null;
    }

    void createNext() {
        cfg.set("NextPage.Name", name.replace(".yml", "") + "_2");
        cfg.set("NextPage.Display", getDisplayName() + "-2");
        Config cfg2 = new Config("plugins/Miniverse/Inventories", cfg.getString("NextPage.Name") + ".yml");
        if (!cfg2.isSet("Settings.Size")) cfg2.set("Settings.Size", cfg.getInt("Settings.Size"));
        if (!cfg2.isSet("Settings.Display")) cfg2.set("Settings.Display", cfg.getString("NextPage.Display"));
        cfg2.set("Settings.Previous", getName());
        cfg2.set("Settings.isNext", true);
        next = new GUIShop(cfg.getString("NextPage.Name"));
    }

    public boolean isNext() {
        return cfg.isSet("Settings.isNext") && cfg.getBoolean("Settings.isNext");
    }

    public Configuration getItemSection() {
        return cfg.getSection("Items");
    }

    public void remove(ItemStack item) {
        for (String a : getItemSection().getKeys(false)) {
            ItemStack stack = ItemBuilder.createItem(a).buildItem();
            if (stack == null) {
                CoreAPI.warning("MiniShop- Invalid item type, " + a + " exists in bukkit? " + (Material.getMaterial(a) != null ? 1 : false) + " exists in minecraft? " + (
                        Material.matchMaterial(a) != null ? 1 : false));

            }
            else if (stack.getType() == item.getType()) {
                cfg.set("Items." + a, null);
                break;
            }
        }
    }

    public String getPath(ItemStack i) {
        String r = "";
        for (String a : getItemSection().getKeys(false)) {
            ItemStack stack = ItemBuilder.createItem(a).buildItem();
            if (stack == null) {
                CoreAPI.warning("MiniShop- Invalid item type, " + a + " exists in bukkit? " + (Material.getMaterial(a) != null ? 1 : false) + " exists in minecraft? " + (
                        Material.matchMaterial(a) != null ? 1 : false));

            }
            else if (stack.getType() == i.getType()) {
                r = "Items." + a;
                break;
            }
        }
        return r;
    }

    public List<ItemValues> getPrices() {
        List<ItemValues> f = new ArrayList<>();
        if (getItemSection() == null) return null;
        for (String a : getItemSection().getKeys(false)) {
            try {
                ItemStack stack = ItemBuilder.createItem(a).buildItem();
                if (stack == null) {
                    CoreAPI.warning("MiniShop- Invalid item type, " + a + " exists in bukkit? " + (Material.getMaterial(a) != null ? 1 : false) + " exists in minecraft? " + (
                            Material.matchMaterial(a) != null ? 1 : false));
                }
                else {
                    double buy = getItemSection().getDouble(a + ".buyCost");
                    double sell = getItemSection().getDouble(a + ".sellCost");
                    f.add(new ItemValues(stack, buy, sell));
                }
            } catch (Exception e) { CoreAPI.warning("MiniShop- Invalid item type, " + a + " exists in bukkit? " + (Material.getMaterial(a) != null ? 1 : false) + " exists in minecraft? " + (
                    Material.matchMaterial(a) != null ? 1 : false));
            }
        }
        return f;
    }

    public ItemValues getValue(ItemStack i) { if ((getPrices() == null) || (getPrices().size() == 0)) return null;
        for (ItemValues val : getPrices()) {
            if ((val != null) && (val.getStack() != null) && (val.getStack().getType() == i.getType())) {
                return val;
            }
        }
        return null;
    }

    public ItemValues getValue(Material i) { return getValue(new ItemStack(i)); }

    public double getSellCost(ItemStack i)
    {
        for (ItemValues val : getPrices()) {
            if (val.getStack().isSimilar(i)) {
                return val.getSell();
            }
        }
        return -1.0D;
    }

    public double getSellCost(Material i) { return getSellCost(new ItemStack(i)); }

    public double getBuyCost(ItemStack i)
    {
        for (ItemValues val : getPrices()) {
            if (val.getStack().isSimilar(i)) {
                return val.getBuy();
            }
        }
        return -1.0D;
    }

    public double getBuyCost(Material i) { return getBuyCost(new ItemStack(i)); }

    public Inventory getInventory()
    {
        return inv;
    }

    public Config getConfig() {
        return cfg;
    }

    public ItemStack getDisplay() {
        return display;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getName() {
        return name;
    }

    public GUIShop getNext() {
        return next;
    }

    public HashMap<Integer, ItemStack> addItems(ItemStack... items) {
        return inv.addItem(items);
    }

    public ItemStack getItem(int slot) {
        return inv.getItem(slot);
    }


    public ItemStack[] getItems()
    {
        return inv.getContents();
    }

    public void setItem(int slot, ItemStack im)
    {
        inv.setItem(slot, im);
    }

    public void setItems(ItemStack... items)
    {
        inv.setContents(items);
    }
}
