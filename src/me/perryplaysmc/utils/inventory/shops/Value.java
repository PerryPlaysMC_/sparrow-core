package me.perryplaysmc.utils.inventory.shops;

import org.bukkit.inventory.ItemStack;

public class Value {
  private ItemStack stack;
  private String name;
  private String path;
  private String color;
  private boolean isBuy;
  private boolean isSell;

  public Value(ItemStack stack, String name, String path, String color, boolean isBuy, boolean isSell) {
    this.stack = stack;
    this.name = name;
    this.path = path;
    this.color = color;
    this.isBuy = isBuy;
    this.isSell = isSell;
  }

  public ItemStack getStack()
  {
    return stack;
  }

  public String getColor() {
    return color;
  }

  public boolean isBuy() {
    return isBuy;
  }

  public boolean isSell() {
    return isSell;
  }

  public String getPath() {
    return path;
  }

  public String getName() {
    return name;
  }
}
