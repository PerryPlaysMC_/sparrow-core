package me.perryplaysmc.utils.inventory;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.utils.string.ColorUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GuiHandler implements Listener {

    private static List<GuiUtil> guis = new ArrayList<>();

    public static void add(GuiUtil gui) {
        guis.add(gui);
    }

    public static void remove(GuiUtil gui) {
        guis.remove(gui);
    }

    List<Player> click = new ArrayList<>();

    @EventHandler
    void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if(click.contains(p)) return;
        else click.add(p);
        (new BukkitRunnable(){public void run() { click.remove(p); }}).runTaskLater(Core.getAPI(), 3);
        try {
            for(GuiUtil gui : guis) {
                if(ColorUtil.removeColor(e.getView().getTitle()).equalsIgnoreCase(ColorUtil.removeColor(gui.getName()))) {
                    if(!(e.getClickedInventory()!=null&&e.getClickedInventory().getType() == gui.getResult().getType())) return;
                    if(gui.getClick()!=null)
                        gui.getClick().event(e);
                }
            }
        }catch (java.util.ConcurrentModificationException ignored){}
    }

    List<Player> close = new ArrayList<>();

    @EventHandler
    void onClick(InventoryCloseEvent e) {
        Player p = (Player) e.getPlayer();
        if(close.contains(p)) return;
        else close.add(p);
        (new BukkitRunnable(){public void run() { close.remove(p); }}).runTaskLater(Core.getAPI(), 3);
        try {
            for (GuiUtil gui : guis) {
                if(ColorUtil.removeColor(e.getView().getTitle()).equalsIgnoreCase(ColorUtil.removeColor(gui.getName()))) {
                    if(!(e.getInventory().getType() == gui.getResult().getType())) return;
                    if (gui.getClose() != null)
                        gui.getClose().event(e);
                }
            }
        }catch (java.util.ConcurrentModificationException ignored){}
    }

    List<Player> open = new ArrayList<>();

    @EventHandler
    void onClick(InventoryOpenEvent e) {
        Player p = (Player) e.getPlayer();
        if(open.contains(p)) return;
        else open.add(p);
        (new BukkitRunnable(){public void run() { open.remove(p); }}).runTaskLater(Core.getAPI(), 3);
        try{
            for(GuiUtil gui : guis) {
                if(ColorUtil.removeColor(e.getView().getTitle()).equalsIgnoreCase(ColorUtil.removeColor(gui.getName()))) {
                    if(!(e.getInventory().getType() == gui.getResult().getType())) return;
                    if(gui.getOpen()!=null)
                        gui.getOpen().event(e);
                }
            }
        }catch (java.util.ConcurrentModificationException ignored){}
    }

    List<Player> drag = new ArrayList<>();

    @EventHandler
    void onClick(InventoryDragEvent e) {
        Player p = (Player) e.getWhoClicked();
        if(drag.contains(p)) return;
        else drag.add(p);
        (new BukkitRunnable(){public void run() { drag.remove(p); }}).runTaskLater(Core.getAPI(), 3);
        try{
            for(GuiUtil gui : guis) {
                if(ColorUtil.removeColor(e.getView().getTitle()).equalsIgnoreCase(ColorUtil.removeColor(gui.getName()))) {
                    if(!(e.getInventory().getType() == gui.getResult().getType())) return;
                    if(gui.getDrag()!=null)
                        gui.getDrag().event(e);
                }
            }
        }catch (java.util.ConcurrentModificationException ignored){}
    }

}
