package me.perryplaysmc.utils.inventory.menus;

import me.perryplaysmc.prison.cells.PrisonCell;
import me.perryplaysmc.prison.cells.PrisonCellManager;
import me.perryplaysmc.user.User;
import me.perryplaysmc.utils.inventory.GuiUtil;
import me.perryplaysmc.utils.inventory.ItemBuilder;
import me.perryplaysmc.utils.string.ColorUtil;
import me.perryplaysmc.utils.string.NumberUtil;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

@SuppressWarnings("all")
public class ListGui {

    private HashMap<Integer, ItemStack> items = new HashMap<>();
    private HashMap<Integer, HashMap<Integer, ItemStack>> pages = new HashMap<>();
    private String name;
    private GuiUtil main;
    private GuiUtil.InventoryClick click;
    private GuiUtil.InventoryClose close;
    private int size, currentPage = 1;

    private ListGui(GuiUtil main, String name) {
        this.main = main;
        this.name = name;
    }

    public void onClick(GuiUtil.InventoryClick click) {
        this.click = click;
    }
    public void onClose(GuiUtil.InventoryClose close) {
        this.close = close;
    }

    public static ListGui createGui(GuiUtil main, String name) {
        return new ListGui(main, name);
    }

    public ListGui addItem(int slot, ItemStack item) {
        this.items.put(slot, item);
        return this;
    }

    public ListGui addItem(ItemStack... items) {
        for(int i = 0; i < items.length; i++) {
            this.items.put(this.items.keySet().size()+(i), items[i]);
        }
        return this;
    }

    public void remove() {

    }

    public void send(User user, int guiPage) {
        int size = items.size();
        int rSize = NumberUtil.getInventorySizeList(size);
        this.size = rSize;
        GuiUtil ng = new GuiUtil(name, rSize);
        int islot = 0;
        HashMap<Integer, HashMap<Integer, ItemStack>> stacks = new HashMap<>();
        HashMap<Integer, ItemStack> page = new HashMap<>();
        int index1 = 1;
        this.currentPage = guiPage;
        for(Map.Entry<Integer, ItemStack> stack : items.entrySet()) {
            if(islot == (ng.size()-9)) {
                stacks.put(index1, page);
                page = new HashMap<>();
                index1++;
                islot = 0;
            }
            if(index1==guiPage)
                ng.setItem((stack.getKey() % (rSize-9)), stack.getValue());
            page.put((stack.getKey() % (rSize-9)), stack.getValue());
            islot++;
        }
        stacks.put(index1, page);
        for(int i = 0; i < (ng.size() < 9 ? ng.size()%9 : 9); i++) {
            ng.setItem(ng.size()-(i+1), ItemBuilder.createGlass(ItemBuilder.ColorType.random(
                    ItemBuilder.ColorType.LIGHT_BLUE,
                    ItemBuilder.ColorType.LIME,
                    ItemBuilder.ColorType.PURPLE), true)
                    .setName(""));
        }
        if(stacks.containsKey(currentPage+1))
            ng.setItem(ng.size()-1, ItemBuilder.createWool(ItemBuilder.ColorType.LIME)
                    .setName("&aNext")
                    .setLore("&aPage: &4" + (currentPage+1)).setInt("page", (currentPage+1)));

        ng.setItem(ng.size()-(ng.size()>9 ? 9 : 4), ItemBuilder.createWool(ItemBuilder.ColorType.RED)
                .setName("&cBack")
                .setLore("&aPage: &4Main")
                .setInt("page", -1));
        ng.onClick((e1) -> {
            if(e1.getSlot() >= (ng.size()-9)) {
                ItemStack cur = e1.getCurrentItem();
                e1.setCancelled(true);
                if (cur == null) return;
                if (!cur.hasItemMeta()) return;
                ItemMeta i = cur.getItemMeta();
                if (i.getDisplayName().startsWith("§aNext")) {
                    ItemBuilder item = ItemBuilder.createItem(cur);
                    ng.clear();
                    currentPage = item.getInt("page");
                    for (Map.Entry<Integer, ItemStack> items : stacks.get(currentPage).entrySet()) {
                        ng.setItem(items.getKey(), items.getValue());
                    }
                    for (int i1 = 0; i1 < (ng.size() < 9 ? ng.size() % 9 : 9); i1++) {
                        ng.setItem(ng.size() - (i1 + 1), ItemBuilder.createGlass(ItemBuilder.ColorType.random(
                                ItemBuilder.ColorType.LIGHT_BLUE,
                                ItemBuilder.ColorType.LIME,
                                ItemBuilder.ColorType.PURPLE), true)
                                .setName(""));
                    }
                    if (stacks.containsKey(currentPage + 1)) {
                        ng.setItem(ng.size() - 1, ItemBuilder.createWool(ItemBuilder.ColorType.LIME)
                                .setName("&aNext")
                                .setLore("&aPage: &4" + (currentPage + 1))
                                .setInt("page", (currentPage+1)));
                    } else {
                        ng.setItem(ng.size() - (1), ItemBuilder.createGlass(ItemBuilder.ColorType.random(
                                ItemBuilder.ColorType.LIGHT_BLUE,
                                ItemBuilder.ColorType.LIME,
                                ItemBuilder.ColorType.PURPLE), true)
                                .setName("")
                                .setInt("page", currentPage));
                    }

                    if (stacks.containsKey(currentPage - 1))
                        ng.setItem(ng.size() - 9, ItemBuilder.createWool(ItemBuilder.ColorType.RED)
                                .setName("&cBack")
                                .setLore("&aPage: &4" + (currentPage - 1))
                                .setInt("page", (currentPage-1)));
                    return;
                }
                if (i.getDisplayName().equalsIgnoreCase("§cBack")) {
                    ItemBuilder item = ItemBuilder.createItem(cur);
                    ng.clear();
                    currentPage = item.getInt("page");
                    if (currentPage == -1) {
                        user.openInventory(main.getResult());
                        return;
                    }
                    for (Map.Entry<Integer, ItemStack> items : stacks.get(currentPage).entrySet()) {
                        ng.setItem(items.getKey(), items.getValue());
                    }
                    for (int i1 = 0; i1 < (ng.size() < 9 ? ng.size() % 9 : 9); i1++) {
                        ng.setItem(ng.size() - (i1 + 1), ItemBuilder.createGlass(ItemBuilder.ColorType.random(
                                ItemBuilder.ColorType.LIGHT_BLUE,
                                ItemBuilder.ColorType.LIME,
                                ItemBuilder.ColorType.PURPLE), true)
                                .setName(""));
                    }
                    if (currentPage > 1)
                        if (stacks.containsKey(currentPage - 1)) {
                            ng.setItem(ng.size() - 9, ItemBuilder.createWool(ItemBuilder.ColorType.RED)
                                    .setName("&cBack")
                                    .setLore("&aPage: &4" + (currentPage - 1))
                                    .setInt("page", (currentPage-1)));
                        } else ng.setItem(ng.size() - 9, ItemBuilder.createWool(ItemBuilder.ColorType.RED)
                                .setName("&cBack")
                                .setLore("&aPage: &4Main")
                                .setInt("page", -1));
                    else ng.setItem(ng.size() - 9, ItemBuilder.createWool(ItemBuilder.ColorType.RED)
                            .setName("&cBack")
                            .setLore("&aPage: &4Main")
                            .setInt("page", -1));

                    ng.setItem(ng.size() - 1, ItemBuilder.createWool(ItemBuilder.ColorType.LIME)
                            .setName("&aNext")
                            .setLore("&aPage: &4" + (currentPage + 1))
                            .setInt("page", (currentPage + 1)));
                    return;
                }
                return;
            }
            if(click != null) {
                e1.setCancelled(false);
                click.event(e1);
            }
        });

        ng.onClose((e1) -> {
            if(close!=null)close.event(e1);
            ng.remove();
        });
        user.openInventory(ng.getResult());
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int size() {
        return size;
    }


}
