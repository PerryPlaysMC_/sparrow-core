package me.perryplaysmc.utils.helper.ban;

import me.perryplaysmc.core.CoreAPI;
import me.perryplaysmc.user.OfflineUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/3/19-2023
 **/
public class BanData {
    
    private String reason, by;
    private UUID user;
    private long expire;
    private BanType type;
    private SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    
    public BanData(BanType type, String by, UUID user, String reason, long expire) {
        this.type = type;
        this.reason = reason;
        this.by = by;
        this.user = user;
        this.expire = expire;
        if(type == BanType.IP) {
            CoreAPI.getBanFile()
                    .set("Banned."+user+".BannedBy", by)
                    .set("Banned."+user+".BannedOn", new Date().getTime())
                    .set("Banned."+user+".Expires", format.format(new Date(expire)))
                    .set("Banned."+user+".Reason", reason);
            return;
        }
        CoreAPI.getBanFile()
                .set("Banned."+user+".BanType", type.name())
                .set("Banned."+user+".BannedBy", by)
                .set("Banned."+user+".BannedOn", new Date().getTime())
                .set("Banned."+user+".Expires", format.format(new Date(expire)))
                .set("Banned."+user+".Reason", reason);
    }
    public BanData(BanType type, String by, UUID user, String reason) {
        this.type = type;
        this.reason = reason;
        this.by = by;
        this.user = user;
        this.expire = -1;
        if(type == BanType.IP) {
            CoreAPI.getBanFile()
                    .set("Banned."+user+".BannedBy", by)
                    .set("Banned."+user+".BannedOn", new Date().getTime())
                    .set("Banned."+user+".Expires", "Forever")
                    .set("Banned."+user+".Reason", reason);
            return;
        }
        CoreAPI.getBanFile()
                .set("Banned."+user+".BanType", type.name())
                .set("Banned."+user+".BannedBy", by)
                .set("Banned."+user+".BannedOn", new Date().getTime())
                .set("Banned."+user+".Expires", "Forever")
                .set("Banned."+user+".Reason", reason);
    }
    
    
    public long getExpire() {
        return expire;
    }
    
    public void setExpire(long expire) {
        this.expire = expire;
        CoreAPI.getBanFile()
                .set("Banned."+user+".Expires", format.format(new Date(expire)));
    }
    
    public String getReason() {
        return reason;
    }
    
    public void setReason(String reason) {
        this.reason = reason;
        CoreAPI.getBanFile()
                .set("Banned."+user+".Reason", reason);
    }
    
    public String getBy() {
        return by;
    }
    
    public void setBy(String by) {
        this.by = by;
        CoreAPI.getBanFile()
                .set("Banned." + user + ".BannedBy", by);
    }
    
    public OfflineUser getBanned() {
        return CoreAPI.getAllUser(user);
    }
    
    public UUID getUser() {
        return user;
    }
    
    public String toString() {
        return "BanData=(user=" + CoreAPI.getOfflineUser(user).getName() + ",reason='" + reason + "',expire=" + expire + ",by="+by+")";
    }
    public enum BanType {
        IP,  NAME,  REGULAR;
    }
}
