package me.perryplaysmc.utils.helper;

import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import java.util.Set;

public interface PluginHelper {


    Set<Plugin> getPlugins();

    Plugin getPlugin(String name);

    PluginManager getPluginManager();

    PluginHelper registerListener(Listener... listeners);

    boolean isEnabled(Plugin pl);

    boolean isDisabled(Plugin pl);
    
    boolean isEnabled(String pl);
    
    boolean isDisabled(String pl);

    String loadPlugin(String plugin);

    String loadPlugin(String folder, String plugin);

    String disablePlugin(String plugin);

    String disablePlugin(Plugin plugin);
}
