package me.perryplaysmc.utils.string;

import me.perryplaysmc.Main_Core;
import me.perryplaysmc.user.OfflineUser;

import java.util.Calendar;
import java.util.Date;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 7/28/19-2023
 **/
public class PlaceHolderUtil {
    
    
    
    
    public static String translate(OfflineUser u, String str) {
        return str.replace("%balance%", u.getAccount().getBalance())
                .replace("%player%", u.getRealName())
                .replace("%name%", u.getRealName())
                .replace("%display%", Main_Core.getInstance().loadPlayer(u.getBase()).getDisplayName())
                .replace("%displayname%", Main_Core.getInstance().loadPlayer(u.getBase()).getDisplayName())
                .replace("%balance%", u.getAccount().getBalance())
                
                .replace("%prefix%", u.getPermissionData().getPrefix())
                .replace("% prefix%", !u.getPermissionData().getPrefix().isEmpty() ? " " + u.getPermissionData().getPrefix() : "")
                .replace("%prefix %", !u.getPermissionData().getPrefix().isEmpty() ? u.getPermissionData().getPrefix() + " " : "")
                .replace("% prefix %", !u.getPermissionData().getPrefix().isEmpty() ? " " + u.getPermissionData().getPrefix() + " " : "")
                
                .replace("%suffix%", u.getPermissionData().getSuffix())
                .replace("% suffix%", !u.getPermissionData().getSuffix().isEmpty() ? " " + u.getPermissionData().getSuffix() : "")
                .replace("%suffix %", !u.getPermissionData().getSuffix().isEmpty() ? u.getPermissionData().getSuffix() + " " : "")
                .replace("% suffix %", !u.getPermissionData().getSuffix().isEmpty() ? " " + u.getPermissionData().getSuffix() + " " : "");
    }
    
    public static String translateTime(String msg) {
    
        String day = "";
        Date d = new Date(System.currentTimeMillis());
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        switch(dayOfWeek) {
            case 1:
                day = "Sunday";
                break;
            case 2:
                day = "Monday";
                break;
            case 3:
                day = "Tuesday";
                break;
            case 4:
                day = "Wednesday";
                break;
            case 5:
                day = "Thursday";
                break;
            case 6:
                day = "Friday";
                break;
            case 7:
                day = "Saturday";
                break;
        }
        int hour = d.getHours(), min = d.getMinutes(), sec = d.getSeconds();
        return msg.replace("%day%", day+"")
                .replace("%hour%", hour+"")
                .replace("%minute%", min+"")
                .replace("%min%", min+"")
                .replace("%second%", sec+"")
                .replace("%sec%", sec+"");
    }
    
    
    
    
}
