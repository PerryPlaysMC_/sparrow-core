package me.perryplaysmc.utils.string;

import me.perryplaysmc.prison.cells.PrisonCellManager;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;

@SuppressWarnings("all")
public class NumberUtil {
    final static char symbol[] = {'M', 'D', 'C', 'L', 'X', 'V', 'I'};
    final static int value[] = {1000, 500, 100, 50, 10, 5, 1};
    final static List<Character> operators = ListsHelper.createList('+', '-', '/', '*', '%', '^').getList();
    final static char digits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    public static boolean isNumber(char c) {
        for(char x : digits)
            if(c == x) return true;
        return false;
    }

    private static Double calc2(String s) {
        double d = 0;
        int start = 0;
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(operators.contains(c)) {
                start = i;
                break;
            }
        }
        String a = "";
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(operators.contains(c)) break;
            a += c;
        }
        d = getDouble(a);
        for (int i = start; i < chars.length; i++) {
            char pr = ' ';
            char nxt = ' ';
            if(i + 1 < chars.length) nxt = chars[i + 1];
            if(i + -1 > start) pr = chars[i + -1];
            if((operators.contains(pr)) && nxt == '-') continue;
            char c = chars[i];
            if(c == '+') {
                String x = "";
                boolean isNegative = false;
                A:
                for (int j = i + 1; j < chars.length; j++) {
                    if(j == i + 1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '%' || chars[j] == '/') {
                        break A;
                    }
                    x += chars[j];
                }
                d += isNegative ? -getDouble(x) : getDouble(x);
            } else if(c == '-') {
                String x = "";
                boolean isNegative = false;
                A:
                for (int j = i + 1; j < chars.length; j++) {
                    if(j == i + 1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '%' || chars[j] == '/') {
                        break A;
                    }
                    x += chars[j];
                }
                d -= isNegative ? -getDouble(x) : getDouble(x);
            } else if(c == '/') {
                String x = "";
                boolean isNegative = false;
                A:
                for (int j = i + 1; j < chars.length; j++) {
                    if(j == i + 1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '%' || chars[j] == '/') {
                        break A;
                    }
                    x += chars[j];
                }
                d /= isNegative ? -getDouble(x) : getDouble(x);
            } else if(c == '*') {
                String x = "";
                boolean isNegative = false;
                A:
                for (int j = i + 1; j < chars.length; j++) {
                    if(j == i + 1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '%' || chars[j] == '/') {
                        break A;
                    }
                    x += chars[j];
                }
                d *= isNegative ? -getDouble(x) : getDouble(x);
            }else if(c == '%') {
                String x = "";
                boolean isNegative = false;
                A:
                for (int j = i + 1; j < chars.length; j++) {
                    if(j == i + 1 && chars[j] == '-') {
                        isNegative = true;
                        continue A;
                    }
                    if(chars[j] == '+' || chars[j] == '-'
                            || chars[j] == '*' || chars[j] == '%' || chars[j] == '/') {
                        break A;
                    }
                    x += chars[j];
                }
                d %= isNegative ? -getDouble(x) : getDouble(x);
            }

        }

        return d;
    }

    static Double getCalcDouble(char[] chars, int i) {
        String x = "";
        boolean isNegative = false;
        A:for (int j = i + 1; j < chars.length; j++) {
            if(j == i + 1 && chars[j] == '-') {
                isNegative = true;
                continue A;
            }
            if(operators.contains(chars[j]) || chars[j] == ')') {
                break A;
            }
            x += chars[j];
        }
        return isNegative ? -getDouble(x) : getDouble(x);
    }

    public static double calc(String s) {
        char[] chars = s.toCharArray();
        Double d = 0d;
        int start = 0;
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(c == '(' && i == 0) continue;
            else if(i > 0 && c == '(') break;
            if(operators.contains(c) || c == ')') {
                if(c == ')') {
                    start = i + 1;
                } else start = i;

                break;
            }
        }
        String a = "";
        boolean calcStr = false;
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(c == '(' && i == 0) continue;
            else if(i > 0 && c == '(') break;
            if(c == '(') {
                calcStr = true;
                continue;
            }
            if(c == ')') break;
            if(!calcStr) {
                if(operators.contains(c)) break;
            }
            a += c;
        }
        if(calcStr) {
            d = calc2(a);
        } else d = getDouble(a);
        if(start == (calcStr ? chars.length + -2 : chars.length) || start == (calcStr ? chars.length + -2 : chars.length) + -1)
            return d;
        boolean m = false, ad = false, su = false, div = false, p = false, po = false;
        String f = "";
        for (int i = start; i < chars.length; i++) {
            char c = chars[i];
            char pr = ' ';
            char nxt = ' ';
            if(i + 1 <= chars.length + -1) nxt = chars[i + 1];
            if(i + -1 > start) pr = chars[i + -1];
            if((operators.contains(pr)) && nxt == '-') continue;
            if(c == '(' && (i+-1)>0) {
                char v = chars[i + -1];
                if(v == '+')
                    ad = true;
                else if(v == '-')
                    su = true;
                else if(v == '/')
                    div = true;
                else if(v == '*')
                    m = true;
                else if(v == '%')
                    p = true;
                else if(v == '^')
                    po = true;
                else m = true;
                continue;
            }
            if(m || ad || su || div || p) {
                if(c == ')') {
                    if(!f.isEmpty()) {
                        if(m)
                            d *= calc2(f);
                        if(ad)
                            d += calc2(f);
                        if(su)
                            d -= calc2(f);
                        if(div)
                            d /= calc2(f);
                        if(p)
                            d %= calc2(f);
                        if(po)
                            d = Double.valueOf((d.intValue()) ^ (calc2(f).intValue()));
                    }
                    m = false;
                    ad = false;
                    su = false;
                    div = false;
                    p = false;
                    po = false;
                    f = "";
                    continue;
                }
                if(c != '(' && c != ')')
                    f += c;
                continue;
            }
            if(i < chars.length + -2 && ("" + chars[i + 1]) != null && chars[i + 1] == '(') continue;
            if(c == '+') {
                d += getCalcDouble(chars, i);
            } else if(c == '-') {
                d -= getCalcDouble(chars, i);
            } else if(c == '/') {
                d /= getCalcDouble(chars, i);
            } else if(c == '*') {
                d *= getCalcDouble(chars, i);
            }else if(c == '%') {
                d %= getCalcDouble(chars, i);
            }else if(c == '^') {
                d = Double.valueOf((d.intValue()) ^ (getCalcDouble(chars, i).intValue()));
            }

        }

        return d;
    }

    public static float lerp(float point1, float point2, float alpha) {
        return point1 + alpha * (point2 - point1);
    }

    public static int intToRoman(String roman) {
        roman = roman.toUpperCase();
        if(roman.length() == 0) {
            return -1;
        }
        for (int i = 0; i < symbol.length; i++) {
            int pos = roman.indexOf(symbol[i]);
            if(pos >= 0) {
                return value[i] - intToRoman(roman.substring(0, pos)) + intToRoman(roman.substring(pos + 1));
            }
        }
        throw new IllegalArgumentException("Invalid Roman Symbol.");
    }

    public static int intToRoman2(String roman) {
        int r;
        try{
            r = intToRoman(roman);
        }catch(IllegalArgumentException e) {
            r = -1;
        }
        return r;
    }
    private static int[] numbers = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

    private static String[] letters = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

    public static Double getPercent(Double current, Double max) {
        if((max == null || max == 0) || (current == null || current == 0)) {
            return 0d;
        }
        Double bottom = max;
        Double top = current;
        Double percent = (top * 100 / bottom);
        String p = (percent + "").replace("-", "");
        return percent;
    }
    public static Double getPercent(Integer current, Integer max) {
        if((max == null || max == 0) || (current == null || current == 0)) {
            return 0d;
        }
        double bottom = max;
        double top = current;
        Double percent = (top * 100 / bottom);
        String p = (percent + "").replace("-", "");
        return percent;
    }
    public static Double getPercent(Integer current, Double max) {
        if((max == null || max == 0) || (current == null || current == 0)) {
            return 0d;
        }
        Double bottom = max;
        Integer top = current;
        Double percent = (top * 100 / bottom);
        String p = (percent + "").replace("-", "");
        return percent;
    }

    public static Float getPercent(Float current, Float max) {
        if((max == null || max == 0) || (current == null || current == 0)) {
            return 0f;
        }
        Float bottom = max;
        Float top = current;
        Float percent = (top * 100 / bottom);
        String p = (percent + "").replace("-", "");
        return percent;
    }


    public static int newRandomInt(int range) {
        return new Random().nextInt(range);
    }
    public static int newRandomInt(int min, int max) {
        Random r = new Random();
        int rv = min + (max-min) * r.nextInt();
        return rv;
    }

    public static double newRandomDouble(double max) {
        return newRandomDouble(0, max);
    }

    public static double newRandomDouble(double min, double max) {
        Random r = new Random();
        double rv = min + (max - min) * r.nextDouble();
        return rv;
    }

    public static int getNumDrops(ItemStack item) {
        if(!item.containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) return 1;
        int i = item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
        if(i <= 0) {
            return 1;
        }
        int j = 2;
        int k = 35 - i * 5;
        if(i > 100) {
            j = (int) (j + Math.round(i - 40.0D));
        }
        if(i > 50) {
            k = 1;
        } else if(i > 25) {
            k = 2;
        } else if(i > 15) {
            k = 3;
        } else if(i > 8) {
            k = 4;
        }
        if(k < 5) {
            i = 100;
        }
        int l = k;
        int i1 = new Random().nextInt(100) + 1;
        while (l <= k * (i + 1)) {
            if(i1 <= l) {
                return j;
            }
            l += k;
            j++;
        }
        return 1;
    }

    public static boolean isEven(int i) {
        if((i % 2) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isInt(String is) {
        try {
            Integer.parseInt(is);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static long getLong(String is) {
        try {
            return Long.parseLong(is);
        } catch (Exception e) {
            return -1;
        }
    }

    public static String format(double d) {
        DecimalFormat df = new DecimalFormat("###,##0.00");
        String x = df.format(d);
        try {
            if(d < 10) {
                if(String.valueOf(d).contains(".")) {
                    return df.format(d);
                }else {
                    return d + ".00";
                }
            }
            return x.charAt(x.length() + -1) == '0' && x.charAt(x.length() + -2) == '0' ? x.substring(0, x.length() + -3) : x;
        } catch (Exception e) {
            return x;
        }
    }

    public static boolean isDouble(String is) {
        try {
            Double.parseDouble(is);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String convertToRoman(int N) {
        String roman = "";
        for (int i = 0; i < numbers.length; i++) {
            while (N >= numbers[i]) {
                roman += letters[i];
                N -= numbers[i];
            }
        }
        return roman;
    }
    public static Double getMoney(double toConvert) {
        return getMoney(""+toConvert);
    }

    public static Double getMoney(String toConvert) {
        double ret = 0;
        boolean isNegative = toConvert.startsWith("-");
        toConvert = toConvert.replace("-", "");
        if(toConvert.contains(","))
            toConvert = toConvert.replace(",", "");
        try {
            ret += Double.parseDouble((toConvert.contains(".") ? toConvert.split("\\.")[0] : toConvert).replace(",", ""));
            if((toConvert).contains(".")) {
                String split1 = toConvert.split("\\.")[1];
                if(getDouble(split1) > Double.MAX_VALUE) return Double.MAX_VALUE;
                int split1Int = getInt(split1);
                if(split1Int > 100) {
                    double amount = 0;
                    if(split1Int >= 100) {
                        while (split1Int >= 100) {
                            amount++;
                            split1Int = split1Int + -100;
                        }
                    }
                    if(split1Int < 100) {
                        if(split1Int < 10) {
                            String total = amount + "";
                            if(("" + amount).contains(".")) {
                                total = ("" + amount).replace(".", ",").split(",")[0];
                            }
                            amount = Double.parseDouble(total + ".0" + split1Int);
                        } else {
                            String total = amount + "";
                            if(("" + amount).contains(".")) {
                                total = ("" + amount).replace(".", ",").split(",")[0];
                            }
                            if((split1Int + "").charAt(1) == '0')
                                amount = Double.parseDouble(total + "." + split1Int + "0");
                            else
                                amount = Double.parseDouble(total + "." + split1Int);
                        }
                    }
                    ret += amount;
                } else return isNegative ? -Double.parseDouble(toConvert) : Double.parseDouble(toConvert);
            }
        } catch (Exception e) {
            ret = 0;
            e.printStackTrace();
        }
        return isNegative ? -ret : ret;
    }

    public static Double getDouble(String intToGet) {
        double ret = 0;
        try {
            ret = Double.parseDouble(intToGet);
        } catch (Exception e) {
            ret = 0;
        }
        return ret;
    }
    public static Double getDoubleRaw(String intToGet) {
        return Double.parseDouble(intToGet);
    }

    public static Integer getInt(String intToGet) {
        int ret = 0;
        try {
            ret = Integer.parseInt(intToGet);
        } catch (Exception e) {
        }
        return ret;
    }

    public static boolean isLong(String intToGet) {
        try {
            Long.parseLong(intToGet);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean containsEquation(String s) {
        try {
            char[] chars = s.toCharArray();
            Double d = 0d;
            int start = 0;
            for (int i = 0; i < chars.length; i++) {
                char c = chars[i];
                if(c!='.'&&(Character.isLetter(c) || !isNumber(c) || c == '=')&&!operators.contains(c))return false;
                if(c == '(' && i == 0) continue;
                else if(i > 0 && c == '(') break;
                if(operators.contains(c) || c == ')') {
                    if(c == ')') {
                        start = i + 1;
                    } else start = i;

                    break;
                }
            }
            String a = "";
            boolean calcStr = false;
            for (int i = 0; i < chars.length; i++) {
                char c = chars[i];
                if(c!='.'&&(Character.isLetter(c) || !isNumber(c) || c == '=')&&!operators.contains(c))return false;
                if(c == '(' && i == 0) continue;
                else if(i > 0 && c == '(') break;
                if(c == '(') {
                    calcStr = true;
                    continue;
                }
                if(c == ')') break;
                if(!calcStr) {
                    if(operators.contains(c)) break;
                }
                a += c;
            }
            if(calcStr) {
                d = calc2(a);
            } else d = getDouble(a);
            if(start == (calcStr ? chars.length + -2 : chars.length) || start == (calcStr ? chars.length + -2 : chars.length) + -1)
                return true;
            boolean m = false, ad = false, su = false, div = false, p = false, po = false;
            String f = "";
            for (int i = start; i < chars.length; i++) {
                char c = chars[i];
                if(c!='.'&&(Character.isLetter(c) || !isNumber(c) || c == '=')&&!operators.contains(c))return false;
                char pr = ' ';
                char nxt = ' ';
                if(i + 1 <= chars.length + -1) nxt = chars[i + 1];
                if(i + -1 > start) pr = chars[i + -1];
                if((operators.contains(pr)) && nxt == '-') continue;
                if(c == '(' && (i+-1)>0) {
                    char v = chars[i + -1];
                    if(v == '+')
                        ad = true;
                    else if(v == '-')
                        su = true;
                    else if(v == '/')
                        div = true;
                    else if(v == '*')
                        m = true;
                    else if(v == '%')
                        p = true;
                    else if(v == '^')
                        po = true;
                    else m = true;
                    continue;
                }
                if(m || ad || su || div || p) {
                    if(c == ')') {
                        if(!f.isEmpty()) {
                            if(m)
                                d *= calc2(f);
                            if(ad)
                                d += calc2(f);
                            if(su)
                                d -= calc2(f);
                            if(div)
                                d /= calc2(f);
                            if(p)
                                d %= calc2(f);
                            if(po)
                                d = Double.valueOf((d.intValue()) ^ (calc2(f).intValue()));
                        }
                        m = false;
                        ad = false;
                        su = false;
                        div = false;
                        p = false;
                        po = false;
                        f = "";
                        continue;
                    }
                    if(c != '(' && c != ')')
                        f += c;
                    continue;
                }
                if(i < chars.length + -2 && ("" + chars[i + 1]) != null && chars[i + 1] == '(') continue;
                if(c!='.'&&(Character.isLetter(c) || !isNumber(c) || c == '=')&&!operators.contains(c))return false;
                if(c == '+') {
                    d += getCalcDouble(chars, i);
                } else if(c == '-') {
                    d -= getCalcDouble(chars, i);
                } else if(c == '/') {
                    d /= getCalcDouble(chars, i);
                } else if(c == '*') {
                    d *= getCalcDouble(chars, i);
                }else if(c == '%') {
                    d %= getCalcDouble(chars, i);
                }else if(c == '^') {
                    d = Double.valueOf((d.intValue()) ^ (getCalcDouble(chars, i).intValue()));
                }

            }
            return true;
        }catch (Exception o) {
            return false;
        }
    }

    public static double findNumber(String str) {
        String trueStr = ColorUtil.removeColor(str);
        String result = "";
        for(char c : trueStr.toCharArray()) {
            if(Character.isDigit(c) || (!result.isEmpty() && c == '.')) {
                result+=c;
            }
        }
        return result.isEmpty() ? -1 : (result.contains(".") ? getDouble(result) : getInt(result));
    }

    public static int getInventorySize(int size) {
        return size <= 5 ? 5
                : size <= 9 ? 9
                : size <= 18 ? 18
                : size <= 27 ? 27
                : size <= 36 ? 36
                : size <= 45 ? 45
                : 54;
    }

    public static int getInventorySizeList(int size) {
        return  size <= 18 ? 18
                : size <= 27 ? 27
                : size <= 36 ? 36
                : size <= 45 ? 45
                : 54;
    }
}