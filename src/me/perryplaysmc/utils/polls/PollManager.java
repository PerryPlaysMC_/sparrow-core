package me.perryplaysmc.utils.polls;

import me.perryplaysmc.core.Core;
import me.perryplaysmc.core.CoreAPI;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class PollManager {
    
    private static List<Poll> polls = new ArrayList<>();
    
    public static void runPolls() {
        (new BukkitRunnable(){
            @Override
            public void run() {
                if(polls != null && polls.size() >0)
                    for(Poll poll : polls) {
                        if(poll != null) {
                            if(poll.getTime() == 300) {
                                CoreAPI.broadcast("[c]Poll: '[pc]"+poll.getName()+"[c]' is ending in 5 minutes");
                            }
                            if(poll.getTime() == 60) {
                                CoreAPI.broadcast("[c]Poll: '[pc]"+poll.getName()+"[c]' is ending in 1 minute");
                            }
                            if(poll.getTime() == 5) {
                                CoreAPI.broadcast("[c]Poll: '[pc]"+poll.getName()+"[c]' is ending in 5 seconds");
                            }
                            if(poll.getTime() == 4) {
                                CoreAPI.broadcast("[c]Poll: '[pc]"+poll.getName()+"[c]' is ending in 4 seconds");
                            }
                            if(poll.getTime() == 3) {
                                CoreAPI.broadcast("[c]Poll: '[pc]"+poll.getName()+"[c]' is ending in 3 seconds");
                            }
                            if(poll.getTime() == 2) {
                                CoreAPI.broadcast("[c]Poll: '[pc]"+poll.getName()+"[c]' is ending in 2 seconds");
                            }
                            if(poll.getTime() == 1) {
                                CoreAPI.broadcast("[c]Poll: '[pc]"+poll.getName()+"[c]' is ending in 1 second");
                            }
                            
                            if(poll.getTime() <= 0) {
                                CoreAPI.broadcast("[c]Poll: [pc]" + poll.getName() + "[c] Has ended, the results are in!",
                                        "&b--------------------------------",
                                        poll.getResults());
                                if(polls.size() == 1) {
                                    polls = new ArrayList<>();
                                    continue;
                                }
                                polls.remove(poll);
                                continue;
                            }
                            poll.setTime(poll.getTime() + -1);
                        }
                    }
            }
        }).runTaskTimer(Core.getAPI(), 0, 20);
    }
    
    public static List<Poll> getActivePolls(){
        return polls;
    }
    
    public static void addPoll(Poll poll) {
        if(!getActivePolls().contains(poll)) {
            getActivePolls().add(poll);
        }
    }
    
    
    public static Poll getPoll(String name) {
        for(Poll poll : getActivePolls()) {
            if(poll.getName().equalsIgnoreCase(name)) return poll;
        }
        return null;
    }
    
    
    
}

