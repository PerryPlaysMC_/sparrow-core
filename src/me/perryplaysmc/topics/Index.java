package me.perryplaysmc.topics;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.help.HelpTopic;
import org.bukkit.help.IndexHelpTopic;

import java.util.Collection;

public class Index  extends IndexHelpTopic {

    public Index(String name, String shortText, String permission, Collection<HelpTopic> topics) {
        super(name, shortText, permission, topics);
    }

    @Override
    public boolean canSee(CommandSender sender) {
        if(sender instanceof ConsoleCommandSender) {
            return true;
        }
        return sender.hasPermission(permission);
    }

    public Collection<HelpTopic> getTopics() {
        return allTopics;
    }

    public void clear() {
        allTopics.clear();
    }

    public void setTopics(Collection<HelpTopic> topics){
        this.allTopics = topics;
    }

}